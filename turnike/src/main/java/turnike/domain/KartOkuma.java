package turnike.domain;


import java.io.Serializable;
import java.util.Objects;

/**
 * A KartOkuma.
 */

public class KartOkuma implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;
    private byte[] resim;
    private String adiSoyadi;
    private String tarih;
    private int saat;
    private String hareket;
    private int dakika;
    private int kapi;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getResim() {
        return resim;
    }

    public void setResim(byte[] resim) {
        this.resim = resim;
    }

    public String getAdiSoyadi() {
        return adiSoyadi;
    }

    public void setAdiSoyadi(String adiSoyadi) {
        this.adiSoyadi = adiSoyadi;
    }

    public String getTarih() {
        return tarih;
    }

    public void setTarih(String tarih) {
        this.tarih = tarih;
    }

    public int getSaat() {
        return saat;
    }

    public void setSaat(int saat) {
        this.saat = saat;
    }

    public String getHareket() {
        return hareket;
    }

    public void setHareket(String hareket) {
        this.hareket = hareket;
    }

    public int getDakika() {
        return dakika;
    }

    public void setDakika(int dakika) {
        this.dakika = dakika;
    }

    public int getKapi() {
        return kapi;
    }

    public void setKapi(int kapi) {
        this.kapi = kapi;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KartOkuma kartOkuma = (KartOkuma) o;
        if (kartOkuma.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), kartOkuma.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Kart Okuma{" +
            "id=" + getId() +
            ", Adı Soyadı='" + getAdiSoyadi() + "'" +
            ", Giriş Tarihi='" + getTarih() + "'" +
            "}";
    }
}
