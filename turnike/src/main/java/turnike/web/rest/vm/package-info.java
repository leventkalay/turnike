/**
 * View Models used by Spring MVC REST controllers.
 */
package turnike.web.rest.vm;
