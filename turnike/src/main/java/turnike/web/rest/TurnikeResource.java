package turnike.web.rest;

import com.codahale.metrics.annotation.Timed;
import turnike.domain.KartOkuma;
import turnike.domain.Turnike;
import turnike.service.TurnikeService;
import turnike.web.rest.errors.BadRequestAlertException;
import turnike.web.rest.util.HeaderUtil;
import turnike.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Turnike.
 */
@RestController
@RequestMapping("/api")
public class TurnikeResource {

    private final Logger log = LoggerFactory.getLogger(TurnikeResource.class);

    private static final String ENTITY_NAME = "turnike";

    private final TurnikeService turnikeService;

    public TurnikeResource(TurnikeService turnikeService) {
        this.turnikeService = turnikeService;
    }

    /**
     * POST  /turnikes : Create a new turnike.
     *
     * @param turnike the turnike to create
     * @return the ResponseEntity with status 201 (Created) and with body the new turnike, or with status 400 (Bad Request) if the turnike has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/turnikes")
    @Timed
    public ResponseEntity<Turnike> createTurnike(@RequestBody Turnike turnike) throws URISyntaxException {
        log.debug("REST request to save Turnike : {}", turnike);
        if (turnike.getId() != null) {
            throw new BadRequestAlertException("A new turnike cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Turnike result = turnikeService.save(turnike);
        return ResponseEntity.created(new URI("/api/turnikes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /turnikes : Updates an existing turnike.
     *
     * @param turnike the turnike to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated turnike,
     * or with status 400 (Bad Request) if the turnike is not valid,
     * or with status 500 (Internal Server Error) if the turnike couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/turnikes")
    @Timed
    public ResponseEntity<Turnike> updateTurnike(@RequestBody Turnike turnike) throws URISyntaxException {
        log.debug("REST request to update Turnike : {}", turnike);
        if (turnike.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Turnike result = turnikeService.save(turnike);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, turnike.getId().toString()))
            .body(result);
    }

    /**
     * GET  /turnikes : get all the turnikes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of turnikes in body
     */
    @GetMapping("/turnikes")
    @Timed
    public ResponseEntity<List<Turnike>> getAllTurnikes(Pageable pageable) {
        log.debug("REST request to get a page of Turnikes");
        Page<Turnike> page = turnikeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/turnikes");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /turnikes/:id : get the "id" turnike.
     *
     * @param id the id of the turnike to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the turnike, or with status 404 (Not Found)
     */
    @GetMapping("/turnikes/{id}")
    @Timed
    public ResponseEntity<Turnike> getTurnike(@PathVariable Long id) {
        log.debug("REST request to get Turnike : {}", id);
        Optional<Turnike> turnike = turnikeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(turnike);
    }

    /**
     * DELETE  /turnikes/:id : delete the "id" turnike.
     *
     * @param id the id of the turnike to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/turnikes/{id}")
    @Timed
    public ResponseEntity<Void> deleteTurnike(@PathVariable Long id) {
        log.debug("REST request to delete Turnike : {}", id);
        turnikeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/turnikes/girisler")
    public ResponseEntity<List<KartOkuma>> getKartOkuma(@Valid @RequestBody long id) {
        String url = "jdbc:sqlserver://10.10.2.57:1443;databaseName=PRO01_2020";
        String url2 = "jdbc:sqlserver://10.10.2.57:1443;databaseName=PRO01_SBT";
        List<KartOkuma> kartOkumaList = new ArrayList<>();
        long lastId =0;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection conn = null;
        Connection conn2 = null;
        try {
            conn = DriverManager.getConnection(url, "PersonelGirisUser" , "P3rs0n!GsL2v");

            Statement statement = conn.createStatement();


            String queryString = "select MAX(KO_ID) KO_ID from KARTOKUTMA where KAPI in (01,02,03,04,05)";
            ResultSet rs = statement.executeQuery(queryString);

            while (rs.next()) {
                lastId=rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(lastId!=id) {
            try {
                conn = DriverManager.getConnection(url, "PersonelGirisUser", "P3rs0n!GsL2v");
                conn2 = DriverManager.getConnection(url2, "PersonelGirisUser" , "P3rs0n!GsL2v");
                Statement statement = conn.createStatement();
                statement = conn2.createStatement();
                String queryString = "select TOP 12 [KO_ID]\n" +
                    "     ,kt.[SICILNO]\n" +
                    "  ,sr.[RESIM]\n" +
                    "      ,[KAPI]\n" +
                    "      ,[ADISOYADI]\n" +
                    "      ,[TARIH]\n" +
                    "      ,[HAREKET]\n" +
                    "      ,[SAAT]  FROM [PRO01_2020].[dbo].[KARTOKUTMA] as kt left JOIN [PRO01_SBT].[dbo].[SICIL_RESIM] as sr\n" +
                    "    ON (sr.[SICILNO] = kt.[SICILNO]) where kt.KAPI in (01,02,03,04,05) order by KO_ID DESC";
                ResultSet rs = statement.executeQuery(queryString);
                log.info("*************************geldim geldim geldm****** "+id);
                while (rs.next()) {
                    KartOkuma kartOkuma = new KartOkuma();
                    kartOkuma.setId(rs.getLong(1));
                    kartOkuma.setResim(rs.getBytes(3)); //Resim
                    kartOkuma.setAdiSoyadi(rs.getString(5));
                    kartOkuma.setKapi(rs.getInt(4));
                    kartOkuma.setHareket(rs.getString(7));
                    int toplamSaat = rs.getInt(8);
                    kartOkuma.setSaat(toplamSaat / 60);
                    kartOkuma.setDakika(toplamSaat % 60);
                    kartOkumaList.add(kartOkuma);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.ok(kartOkumaList);
    }
}
