package turnike.repository;

import turnike.domain.Turnike;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Turnike entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TurnikeRepository extends JpaRepository<Turnike, Long> {

}
