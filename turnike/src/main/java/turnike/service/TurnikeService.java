package turnike.service;

import turnike.domain.Turnike;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Turnike.
 */
public interface TurnikeService {

    /**
     * Save a turnike.
     *
     * @param turnike the entity to save
     * @return the persisted entity
     */
    Turnike save(Turnike turnike);

    /**
     * Get all the turnikes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Turnike> findAll(Pageable pageable);


    /**
     * Get the "id" turnike.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Turnike> findOne(Long id);

    /**
     * Delete the "id" turnike.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
