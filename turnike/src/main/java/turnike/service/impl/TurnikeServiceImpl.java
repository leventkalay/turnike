package turnike.service.impl;

import turnike.service.TurnikeService;
import turnike.domain.Turnike;
import turnike.repository.TurnikeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Turnike.
 */
@Service
@Transactional
public class TurnikeServiceImpl implements TurnikeService {

    private final Logger log = LoggerFactory.getLogger(TurnikeServiceImpl.class);

    private final TurnikeRepository turnikeRepository;

    public TurnikeServiceImpl(TurnikeRepository turnikeRepository) {
        this.turnikeRepository = turnikeRepository;
    }

    /**
     * Save a turnike.
     *
     * @param turnike the entity to save
     * @return the persisted entity
     */
    @Override
    public Turnike save(Turnike turnike) {
        log.debug("Request to save Turnike : {}", turnike);
        return turnikeRepository.save(turnike);
    }

    /**
     * Get all the turnikes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Turnike> findAll(Pageable pageable) {
        log.debug("Request to get all Turnikes");
        return turnikeRepository.findAll(pageable);
    }


    /**
     * Get one turnike by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Turnike> findOne(Long id) {
        log.debug("Request to get Turnike : {}", id);
        return turnikeRepository.findById(id);
    }

    /**
     * Delete the turnike by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Turnike : {}", id);
        turnikeRepository.deleteById(id);
    }
}
