import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITurnike } from 'app/shared/model/turnike.model';

@Component({
    selector: 'jhi-turnike-detail',
    templateUrl: './turnike-detail.component.html'
})
export class TurnikeDetailComponent implements OnInit {
    turnike: ITurnike;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ turnike }) => {
            this.turnike = turnike;
        });
    }

    previousState() {
        window.history.back();
    }
}
