import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Turnike } from 'app/shared/model/turnike.model';
import { TurnikeService } from './turnike.service';
import { TurnikeComponent } from './turnike.component';
import { TurnikeDetailComponent } from './turnike-detail.component';
import { TurnikeUpdateComponent } from './turnike-update.component';
import { TurnikeDeletePopupComponent } from './turnike-delete-dialog.component';
import { ITurnike } from 'app/shared/model/turnike.model';

@Injectable({ providedIn: 'root' })
export class TurnikeResolve implements Resolve<ITurnike> {
    constructor(private service: TurnikeService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Turnike> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Turnike>) => response.ok),
                map((turnike: HttpResponse<Turnike>) => turnike.body)
            );
        }
        return of(new Turnike());
    }
}

export const turnikeRoute: Routes = [
    {
        path: 'turnike',
        component: TurnikeComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'turnikeApp.turnike.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'turnike/:id/view',
        component: TurnikeDetailComponent,
        resolve: {
            turnike: TurnikeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'turnikeApp.turnike.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'turnike/new',
        component: TurnikeUpdateComponent,
        resolve: {
            turnike: TurnikeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'turnikeApp.turnike.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'turnike/:id/edit',
        component: TurnikeUpdateComponent,
        resolve: {
            turnike: TurnikeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'turnikeApp.turnike.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const turnikePopupRoute: Routes = [
    {
        path: 'turnike/:id/delete',
        component: TurnikeDeletePopupComponent,
        resolve: {
            turnike: TurnikeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'turnikeApp.turnike.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
