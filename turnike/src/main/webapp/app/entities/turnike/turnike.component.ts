import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ITurnike } from 'app/shared/model/turnike.model';
import {AccountService, Principal} from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { TurnikeService } from './turnike.service';
import {IKartOkuma} from "app/shared/model/kart-okuma";

@Component({
    selector: 'jhi-turnike',
    templateUrl: './turnike.component.html',
    styles: [`
        .cikis {
            color: red !important;
        }
        .cardBottom {

        }
        .giris {
            color: green  !important;
        }
        .girisBorder {
            background-color: green;
        }
        .cikisBorder {
            background-color: red;
        }
    `
    ]
})
export class TurnikeComponent implements OnInit, OnDestroy {
    currentAccount: any;
    turnikes: ITurnike[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    lastId: number;
    id: any;
    id2: any;
    kartOkumaList: IKartOkuma[];

    constructor(
        private turnikeService: TurnikeService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.lastId=0;
    }

    loadFirst() {
        this.turnikeService
            .getKartOkuma(this.lastId)
            .subscribe(
                (res: HttpResponse<IKartOkuma[]>) => this.paginateTurnikes(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadAll() {
        if (this.kartOkumaList != null)
            if (this.kartOkumaList.length != 0) {
                this.lastId = this.kartOkumaList[0].id;
            }
        this.turnikeService
            .getKartOkuma(this.lastId)
            .subscribe(
                (res: HttpResponse<IKartOkuma[]>) => this.paginateTurnikes(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/turnike'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/turnike',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    ngOnInit() {
        let date = new Date();
        this.loadFirst();
        if (date.getDay() == 0 || date.getDay() == 6) {
            this.id = setInterval(() => {
                this.loadAll();
            }, 5000);
        } else {
            if (date.getHours() > 19 || date.getHours() < 8) {
                setInterval(() => {
                    this.loadAll();
                }, 5000);
            } else {
                this.id2 = setInterval(() => {
                    this.loadAll();
                }, 1500);
            }
        }
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInTurnikes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ITurnike) {
        return item.id;
    }

    registerChangeInTurnikes() {
        this.eventSubscriber = this.eventManager.subscribe('turnikeListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateTurnikes(data: ITurnike[], headers: HttpHeaders) {
        if (data.length != 0){
            this.kartOkumaList = data;
        }
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
