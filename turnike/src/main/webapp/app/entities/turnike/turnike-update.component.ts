import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ITurnike } from 'app/shared/model/turnike.model';
import { TurnikeService } from './turnike.service';

@Component({
    selector: 'jhi-turnike-update',
    templateUrl: './turnike-update.component.html'
})
export class TurnikeUpdateComponent implements OnInit {
    turnike: ITurnike;
    isSaving: boolean;

    constructor(private turnikeService: TurnikeService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ turnike }) => {
            this.turnike = turnike;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.turnike.id !== undefined) {
            this.subscribeToSaveResponse(this.turnikeService.update(this.turnike));
        } else {
            this.subscribeToSaveResponse(this.turnikeService.create(this.turnike));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ITurnike>>) {
        result.subscribe((res: HttpResponse<ITurnike>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
