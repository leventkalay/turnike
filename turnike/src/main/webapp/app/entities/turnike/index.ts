export * from './turnike.service';
export * from './turnike-update.component';
export * from './turnike-delete-dialog.component';
export * from './turnike-detail.component';
export * from './turnike.component';
export * from './turnike.route';
