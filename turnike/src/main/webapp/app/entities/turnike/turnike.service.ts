import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITurnike } from 'app/shared/model/turnike.model';
import {IKartOkuma} from "app/shared/model/kart-okuma";

type EntityResponseType = HttpResponse<ITurnike>;
type EntityArrayResponseType = HttpResponse<ITurnike[]>;
type EntityArrayKartOkumaResponseType = HttpResponse<IKartOkuma[]>;

@Injectable({ providedIn: 'root' })
export class TurnikeService {
    public resourceUrl = SERVER_API_URL + 'api/turnikes';
    public resourceUrlKartOkumaList = SERVER_API_URL + 'api/turnikes/girisler';

    constructor(private http: HttpClient) {}

    create(turnike: ITurnike): Observable<EntityResponseType> {
        return this.http.post<ITurnike>(this.resourceUrl, turnike, { observe: 'response' });
    }

    update(turnike: ITurnike): Observable<EntityResponseType> {
        return this.http.put<ITurnike>(this.resourceUrl, turnike, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ITurnike>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ITurnike[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getKartOkuma(id: number): Observable<EntityArrayKartOkumaResponseType> {
        return this.http.post<IKartOkuma[]>(this.resourceUrlKartOkumaList, id, { observe: 'response' });
    }
}
