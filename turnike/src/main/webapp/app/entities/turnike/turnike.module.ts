import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TurnikeSharedModule } from 'app/shared';
import {
    TurnikeComponent,
    TurnikeDetailComponent,
    TurnikeUpdateComponent,
    TurnikeDeletePopupComponent,
    TurnikeDeleteDialogComponent,
    turnikeRoute,
    turnikePopupRoute
} from './';

const ENTITY_STATES = [...turnikeRoute, ...turnikePopupRoute];

@NgModule({
    imports: [TurnikeSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        TurnikeComponent,
        TurnikeDetailComponent,
        TurnikeUpdateComponent,
        TurnikeDeleteDialogComponent,
        TurnikeDeletePopupComponent
    ],
    entryComponents: [TurnikeComponent, TurnikeUpdateComponent, TurnikeDeleteDialogComponent, TurnikeDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TurnikeTurnikeModule {}
