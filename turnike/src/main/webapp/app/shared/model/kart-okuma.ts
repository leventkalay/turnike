export interface IKartOkuma {
    id?: number;
    resim?: any;
    adiSoyadi?: string;
    tarih?: string;
    saat?: number;
    hareket?: string;
    dakika?: number;
    kapi?: number;
}

export class KartOkuma implements IKartOkuma {
    constructor(public id?: number, public resim?: any, public adi?: string, public tarih?: string, public saat?: number, public hareket?: string, dakika?: number, kapi?: number) {
    }
}
