import { NgModule } from '@angular/core';

import { TurnikeSharedLibsModule, FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent } from './';
import {CardModule} from 'primeng/card';

@NgModule({
    imports: [TurnikeSharedLibsModule, CardModule],
    declarations: [FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent],
    exports: [TurnikeSharedLibsModule, FindLanguageFromKeyPipe, JhiAlertComponent, JhiAlertErrorComponent, CardModule]
})
export class TurnikeSharedCommonModule {}
