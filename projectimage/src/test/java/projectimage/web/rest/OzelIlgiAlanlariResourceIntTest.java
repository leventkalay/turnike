package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.OzelIlgiAlanlari;
import projectimage.repository.OzelIlgiAlanlariRepository;
import projectimage.service.OzelIlgiAlanlariService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OzelIlgiAlanlariResource REST controller.
 *
 * @see OzelIlgiAlanlariResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class OzelIlgiAlanlariResourceIntTest {

    private static final String DEFAULT_OZEL_ILGI_ALANLARI = "AAAAAAAAAA";
    private static final String UPDATED_OZEL_ILGI_ALANLARI = "BBBBBBBBBB";

    @Autowired
    private OzelIlgiAlanlariRepository ozelIlgiAlanlariRepository;

    @Autowired
    private OzelIlgiAlanlariService ozelIlgiAlanlariService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOzelIlgiAlanlariMockMvc;

    private OzelIlgiAlanlari ozelIlgiAlanlari;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OzelIlgiAlanlariResource ozelIlgiAlanlariResource = new OzelIlgiAlanlariResource(ozelIlgiAlanlariService);
        this.restOzelIlgiAlanlariMockMvc = MockMvcBuilders.standaloneSetup(ozelIlgiAlanlariResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OzelIlgiAlanlari createEntity(EntityManager em) {
        OzelIlgiAlanlari ozelIlgiAlanlari = new OzelIlgiAlanlari()
            .ozelIlgiAlanlari(DEFAULT_OZEL_ILGI_ALANLARI);
        return ozelIlgiAlanlari;
    }

    @Before
    public void initTest() {
        ozelIlgiAlanlari = createEntity(em);
    }

    @Test
    @Transactional
    public void createOzelIlgiAlanlari() throws Exception {
        int databaseSizeBeforeCreate = ozelIlgiAlanlariRepository.findAll().size();

        // Create the OzelIlgiAlanlari
        restOzelIlgiAlanlariMockMvc.perform(post("/api/ozel-ilgi-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ozelIlgiAlanlari)))
            .andExpect(status().isCreated());

        // Validate the OzelIlgiAlanlari in the database
        List<OzelIlgiAlanlari> ozelIlgiAlanlariList = ozelIlgiAlanlariRepository.findAll();
        assertThat(ozelIlgiAlanlariList).hasSize(databaseSizeBeforeCreate + 1);
        OzelIlgiAlanlari testOzelIlgiAlanlari = ozelIlgiAlanlariList.get(ozelIlgiAlanlariList.size() - 1);
        assertThat(testOzelIlgiAlanlari.getOzelIlgiAlanlari()).isEqualTo(DEFAULT_OZEL_ILGI_ALANLARI);
    }

    @Test
    @Transactional
    public void createOzelIlgiAlanlariWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ozelIlgiAlanlariRepository.findAll().size();

        // Create the OzelIlgiAlanlari with an existing ID
        ozelIlgiAlanlari.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOzelIlgiAlanlariMockMvc.perform(post("/api/ozel-ilgi-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ozelIlgiAlanlari)))
            .andExpect(status().isBadRequest());

        // Validate the OzelIlgiAlanlari in the database
        List<OzelIlgiAlanlari> ozelIlgiAlanlariList = ozelIlgiAlanlariRepository.findAll();
        assertThat(ozelIlgiAlanlariList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllOzelIlgiAlanlaris() throws Exception {
        // Initialize the database
        ozelIlgiAlanlariRepository.saveAndFlush(ozelIlgiAlanlari);

        // Get all the ozelIlgiAlanlariList
        restOzelIlgiAlanlariMockMvc.perform(get("/api/ozel-ilgi-alanlaris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ozelIlgiAlanlari.getId().intValue())))
            .andExpect(jsonPath("$.[*].ozelIlgiAlanlari").value(hasItem(DEFAULT_OZEL_ILGI_ALANLARI.toString())));
    }
    
    @Test
    @Transactional
    public void getOzelIlgiAlanlari() throws Exception {
        // Initialize the database
        ozelIlgiAlanlariRepository.saveAndFlush(ozelIlgiAlanlari);

        // Get the ozelIlgiAlanlari
        restOzelIlgiAlanlariMockMvc.perform(get("/api/ozel-ilgi-alanlaris/{id}", ozelIlgiAlanlari.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ozelIlgiAlanlari.getId().intValue()))
            .andExpect(jsonPath("$.ozelIlgiAlanlari").value(DEFAULT_OZEL_ILGI_ALANLARI.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOzelIlgiAlanlari() throws Exception {
        // Get the ozelIlgiAlanlari
        restOzelIlgiAlanlariMockMvc.perform(get("/api/ozel-ilgi-alanlaris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOzelIlgiAlanlari() throws Exception {
        // Initialize the database
        ozelIlgiAlanlariService.save(ozelIlgiAlanlari);

        int databaseSizeBeforeUpdate = ozelIlgiAlanlariRepository.findAll().size();

        // Update the ozelIlgiAlanlari
        OzelIlgiAlanlari updatedOzelIlgiAlanlari = ozelIlgiAlanlariRepository.findById(ozelIlgiAlanlari.getId()).get();
        // Disconnect from session so that the updates on updatedOzelIlgiAlanlari are not directly saved in db
        em.detach(updatedOzelIlgiAlanlari);
        updatedOzelIlgiAlanlari
            .ozelIlgiAlanlari(UPDATED_OZEL_ILGI_ALANLARI);

        restOzelIlgiAlanlariMockMvc.perform(put("/api/ozel-ilgi-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOzelIlgiAlanlari)))
            .andExpect(status().isOk());

        // Validate the OzelIlgiAlanlari in the database
        List<OzelIlgiAlanlari> ozelIlgiAlanlariList = ozelIlgiAlanlariRepository.findAll();
        assertThat(ozelIlgiAlanlariList).hasSize(databaseSizeBeforeUpdate);
        OzelIlgiAlanlari testOzelIlgiAlanlari = ozelIlgiAlanlariList.get(ozelIlgiAlanlariList.size() - 1);
        assertThat(testOzelIlgiAlanlari.getOzelIlgiAlanlari()).isEqualTo(UPDATED_OZEL_ILGI_ALANLARI);
    }

    @Test
    @Transactional
    public void updateNonExistingOzelIlgiAlanlari() throws Exception {
        int databaseSizeBeforeUpdate = ozelIlgiAlanlariRepository.findAll().size();

        // Create the OzelIlgiAlanlari

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOzelIlgiAlanlariMockMvc.perform(put("/api/ozel-ilgi-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ozelIlgiAlanlari)))
            .andExpect(status().isBadRequest());

        // Validate the OzelIlgiAlanlari in the database
        List<OzelIlgiAlanlari> ozelIlgiAlanlariList = ozelIlgiAlanlariRepository.findAll();
        assertThat(ozelIlgiAlanlariList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOzelIlgiAlanlari() throws Exception {
        // Initialize the database
        ozelIlgiAlanlariService.save(ozelIlgiAlanlari);

        int databaseSizeBeforeDelete = ozelIlgiAlanlariRepository.findAll().size();

        // Get the ozelIlgiAlanlari
        restOzelIlgiAlanlariMockMvc.perform(delete("/api/ozel-ilgi-alanlaris/{id}", ozelIlgiAlanlari.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OzelIlgiAlanlari> ozelIlgiAlanlariList = ozelIlgiAlanlariRepository.findAll();
        assertThat(ozelIlgiAlanlariList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OzelIlgiAlanlari.class);
        OzelIlgiAlanlari ozelIlgiAlanlari1 = new OzelIlgiAlanlari();
        ozelIlgiAlanlari1.setId(1L);
        OzelIlgiAlanlari ozelIlgiAlanlari2 = new OzelIlgiAlanlari();
        ozelIlgiAlanlari2.setId(ozelIlgiAlanlari1.getId());
        assertThat(ozelIlgiAlanlari1).isEqualTo(ozelIlgiAlanlari2);
        ozelIlgiAlanlari2.setId(2L);
        assertThat(ozelIlgiAlanlari1).isNotEqualTo(ozelIlgiAlanlari2);
        ozelIlgiAlanlari1.setId(null);
        assertThat(ozelIlgiAlanlari1).isNotEqualTo(ozelIlgiAlanlari2);
    }
}
