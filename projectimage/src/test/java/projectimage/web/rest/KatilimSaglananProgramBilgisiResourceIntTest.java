package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.KatilimSaglananProgramBilgisi;
import projectimage.repository.KatilimSaglananProgramBilgisiRepository;
import projectimage.service.KatilimSaglananProgramBilgisiService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.ProgramTuru;
import projectimage.domain.enumeration.ProgramGorevi;
import projectimage.domain.enumeration.Ulke;
import projectimage.domain.enumeration.Sehir;
import projectimage.domain.enumeration.BelgeTuru;
/**
 * Test class for the KatilimSaglananProgramBilgisiResource REST controller.
 *
 * @see KatilimSaglananProgramBilgisiResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class KatilimSaglananProgramBilgisiResourceIntTest {

    private static final ProgramTuru DEFAULT_PROGRAM_TURU = ProgramTuru.SEMINER;
    private static final ProgramTuru UPDATED_PROGRAM_TURU = ProgramTuru.KURS;

    private static final ProgramGorevi DEFAULT_PROGRAM_GOREVI = ProgramGorevi.KATILIMCI;
    private static final ProgramGorevi UPDATED_PROGRAM_GOREVI = ProgramGorevi.KURSIYER;

    private static final String DEFAULT_PROGRAM_ADI = "AAAAAAAAAA";
    private static final String UPDATED_PROGRAM_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_PROGRAM_KONUSU = "AAAAAAAAAA";
    private static final String UPDATED_PROGRAM_KONUSU = "BBBBBBBBBB";

    private static final Integer DEFAULT_BASLANGIC_YILI = 1;
    private static final Integer UPDATED_BASLANGIC_YILI = 2;

    private static final Integer DEFAULT_BITIS_YILI = 1;
    private static final Integer UPDATED_BITIS_YILI = 2;

    private static final String DEFAULT_DUZENLEYEN_KURUM = "AAAAAAAAAA";
    private static final String UPDATED_DUZENLEYEN_KURUM = "BBBBBBBBBB";

    private static final Ulke DEFAULT_ULKE = Ulke.TURKIYE;
    private static final Ulke UPDATED_ULKE = Ulke.INGILTERE;

    private static final Sehir DEFAULT_SEHIR = Sehir.ADANA;
    private static final Sehir UPDATED_SEHIR = Sehir.ADIYAMAN;

    private static final BelgeTuru DEFAULT_BELGE_TURU = BelgeTuru.CAE;
    private static final BelgeTuru UPDATED_BELGE_TURU = BelgeTuru.CPE;

    private static final String DEFAULT_ANAHTAR_KELIMELER = "AAAAAAAAAA";
    private static final String UPDATED_ANAHTAR_KELIMELER = "BBBBBBBBBB";

    @Autowired
    private KatilimSaglananProgramBilgisiRepository katilimSaglananProgramBilgisiRepository;

    @Autowired
    private KatilimSaglananProgramBilgisiService katilimSaglananProgramBilgisiService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restKatilimSaglananProgramBilgisiMockMvc;

    private KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KatilimSaglananProgramBilgisiResource katilimSaglananProgramBilgisiResource = new KatilimSaglananProgramBilgisiResource(katilimSaglananProgramBilgisiService);
        this.restKatilimSaglananProgramBilgisiMockMvc = MockMvcBuilders.standaloneSetup(katilimSaglananProgramBilgisiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KatilimSaglananProgramBilgisi createEntity(EntityManager em) {
        KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi = new KatilimSaglananProgramBilgisi()
            .programTuru(DEFAULT_PROGRAM_TURU)
            .programGorevi(DEFAULT_PROGRAM_GOREVI)
            .programAdi(DEFAULT_PROGRAM_ADI)
            .programKonusu(DEFAULT_PROGRAM_KONUSU)
            .baslangicYili(DEFAULT_BASLANGIC_YILI)
            .bitisYili(DEFAULT_BITIS_YILI)
            .duzenleyenKurum(DEFAULT_DUZENLEYEN_KURUM)
            .ulke(DEFAULT_ULKE)
            .sehir(DEFAULT_SEHIR)
            .belgeTuru(DEFAULT_BELGE_TURU)
            .anahtarKelimeler(DEFAULT_ANAHTAR_KELIMELER);
        return katilimSaglananProgramBilgisi;
    }

    @Before
    public void initTest() {
        katilimSaglananProgramBilgisi = createEntity(em);
    }

    @Test
    @Transactional
    public void createKatilimSaglananProgramBilgisi() throws Exception {
        int databaseSizeBeforeCreate = katilimSaglananProgramBilgisiRepository.findAll().size();

        // Create the KatilimSaglananProgramBilgisi
        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isCreated());

        // Validate the KatilimSaglananProgramBilgisi in the database
        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeCreate + 1);
        KatilimSaglananProgramBilgisi testKatilimSaglananProgramBilgisi = katilimSaglananProgramBilgisiList.get(katilimSaglananProgramBilgisiList.size() - 1);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramTuru()).isEqualTo(DEFAULT_PROGRAM_TURU);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramGorevi()).isEqualTo(DEFAULT_PROGRAM_GOREVI);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramAdi()).isEqualTo(DEFAULT_PROGRAM_ADI);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramKonusu()).isEqualTo(DEFAULT_PROGRAM_KONUSU);
        assertThat(testKatilimSaglananProgramBilgisi.getBaslangicYili()).isEqualTo(DEFAULT_BASLANGIC_YILI);
        assertThat(testKatilimSaglananProgramBilgisi.getBitisYili()).isEqualTo(DEFAULT_BITIS_YILI);
        assertThat(testKatilimSaglananProgramBilgisi.getDuzenleyenKurum()).isEqualTo(DEFAULT_DUZENLEYEN_KURUM);
        assertThat(testKatilimSaglananProgramBilgisi.getUlke()).isEqualTo(DEFAULT_ULKE);
        assertThat(testKatilimSaglananProgramBilgisi.getSehir()).isEqualTo(DEFAULT_SEHIR);
        assertThat(testKatilimSaglananProgramBilgisi.getBelgeTuru()).isEqualTo(DEFAULT_BELGE_TURU);
        assertThat(testKatilimSaglananProgramBilgisi.getAnahtarKelimeler()).isEqualTo(DEFAULT_ANAHTAR_KELIMELER);
    }

    @Test
    @Transactional
    public void createKatilimSaglananProgramBilgisiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = katilimSaglananProgramBilgisiRepository.findAll().size();

        // Create the KatilimSaglananProgramBilgisi with an existing ID
        katilimSaglananProgramBilgisi.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        // Validate the KatilimSaglananProgramBilgisi in the database
        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkProgramTuruIsRequired() throws Exception {
        int databaseSizeBeforeTest = katilimSaglananProgramBilgisiRepository.findAll().size();
        // set the field null
        katilimSaglananProgramBilgisi.setProgramTuru(null);

        // Create the KatilimSaglananProgramBilgisi, which fails.

        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramGoreviIsRequired() throws Exception {
        int databaseSizeBeforeTest = katilimSaglananProgramBilgisiRepository.findAll().size();
        // set the field null
        katilimSaglananProgramBilgisi.setProgramGorevi(null);

        // Create the KatilimSaglananProgramBilgisi, which fails.

        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = katilimSaglananProgramBilgisiRepository.findAll().size();
        // set the field null
        katilimSaglananProgramBilgisi.setProgramAdi(null);

        // Create the KatilimSaglananProgramBilgisi, which fails.

        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDuzenleyenKurumIsRequired() throws Exception {
        int databaseSizeBeforeTest = katilimSaglananProgramBilgisiRepository.findAll().size();
        // set the field null
        katilimSaglananProgramBilgisi.setDuzenleyenKurum(null);

        // Create the KatilimSaglananProgramBilgisi, which fails.

        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUlkeIsRequired() throws Exception {
        int databaseSizeBeforeTest = katilimSaglananProgramBilgisiRepository.findAll().size();
        // set the field null
        katilimSaglananProgramBilgisi.setUlke(null);

        // Create the KatilimSaglananProgramBilgisi, which fails.

        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSehirIsRequired() throws Exception {
        int databaseSizeBeforeTest = katilimSaglananProgramBilgisiRepository.findAll().size();
        // set the field null
        katilimSaglananProgramBilgisi.setSehir(null);

        // Create the KatilimSaglananProgramBilgisi, which fails.

        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBelgeTuruIsRequired() throws Exception {
        int databaseSizeBeforeTest = katilimSaglananProgramBilgisiRepository.findAll().size();
        // set the field null
        katilimSaglananProgramBilgisi.setBelgeTuru(null);

        // Create the KatilimSaglananProgramBilgisi, which fails.

        restKatilimSaglananProgramBilgisiMockMvc.perform(post("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllKatilimSaglananProgramBilgisis() throws Exception {
        // Initialize the database
        katilimSaglananProgramBilgisiRepository.saveAndFlush(katilimSaglananProgramBilgisi);

        // Get all the katilimSaglananProgramBilgisiList
        restKatilimSaglananProgramBilgisiMockMvc.perform(get("/api/katilim-saglanan-program-bilgisis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(katilimSaglananProgramBilgisi.getId().intValue())))
            .andExpect(jsonPath("$.[*].programTuru").value(hasItem(DEFAULT_PROGRAM_TURU.toString())))
            .andExpect(jsonPath("$.[*].programGorevi").value(hasItem(DEFAULT_PROGRAM_GOREVI.toString())))
            .andExpect(jsonPath("$.[*].programAdi").value(hasItem(DEFAULT_PROGRAM_ADI.toString())))
            .andExpect(jsonPath("$.[*].programKonusu").value(hasItem(DEFAULT_PROGRAM_KONUSU.toString())))
            .andExpect(jsonPath("$.[*].baslangicYili").value(hasItem(DEFAULT_BASLANGIC_YILI)))
            .andExpect(jsonPath("$.[*].bitisYili").value(hasItem(DEFAULT_BITIS_YILI)))
            .andExpect(jsonPath("$.[*].duzenleyenKurum").value(hasItem(DEFAULT_DUZENLEYEN_KURUM.toString())))
            .andExpect(jsonPath("$.[*].ulke").value(hasItem(DEFAULT_ULKE.toString())))
            .andExpect(jsonPath("$.[*].sehir").value(hasItem(DEFAULT_SEHIR.toString())))
            .andExpect(jsonPath("$.[*].belgeTuru").value(hasItem(DEFAULT_BELGE_TURU.toString())))
            .andExpect(jsonPath("$.[*].anahtarKelimeler").value(hasItem(DEFAULT_ANAHTAR_KELIMELER.toString())));
    }
    
    @Test
    @Transactional
    public void getKatilimSaglananProgramBilgisi() throws Exception {
        // Initialize the database
        katilimSaglananProgramBilgisiRepository.saveAndFlush(katilimSaglananProgramBilgisi);

        // Get the katilimSaglananProgramBilgisi
        restKatilimSaglananProgramBilgisiMockMvc.perform(get("/api/katilim-saglanan-program-bilgisis/{id}", katilimSaglananProgramBilgisi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(katilimSaglananProgramBilgisi.getId().intValue()))
            .andExpect(jsonPath("$.programTuru").value(DEFAULT_PROGRAM_TURU.toString()))
            .andExpect(jsonPath("$.programGorevi").value(DEFAULT_PROGRAM_GOREVI.toString()))
            .andExpect(jsonPath("$.programAdi").value(DEFAULT_PROGRAM_ADI.toString()))
            .andExpect(jsonPath("$.programKonusu").value(DEFAULT_PROGRAM_KONUSU.toString()))
            .andExpect(jsonPath("$.baslangicYili").value(DEFAULT_BASLANGIC_YILI))
            .andExpect(jsonPath("$.bitisYili").value(DEFAULT_BITIS_YILI))
            .andExpect(jsonPath("$.duzenleyenKurum").value(DEFAULT_DUZENLEYEN_KURUM.toString()))
            .andExpect(jsonPath("$.ulke").value(DEFAULT_ULKE.toString()))
            .andExpect(jsonPath("$.sehir").value(DEFAULT_SEHIR.toString()))
            .andExpect(jsonPath("$.belgeTuru").value(DEFAULT_BELGE_TURU.toString()))
            .andExpect(jsonPath("$.anahtarKelimeler").value(DEFAULT_ANAHTAR_KELIMELER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingKatilimSaglananProgramBilgisi() throws Exception {
        // Get the katilimSaglananProgramBilgisi
        restKatilimSaglananProgramBilgisiMockMvc.perform(get("/api/katilim-saglanan-program-bilgisis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKatilimSaglananProgramBilgisi() throws Exception {
        // Initialize the database
        katilimSaglananProgramBilgisiService.save(katilimSaglananProgramBilgisi);

        int databaseSizeBeforeUpdate = katilimSaglananProgramBilgisiRepository.findAll().size();

        // Update the katilimSaglananProgramBilgisi
        KatilimSaglananProgramBilgisi updatedKatilimSaglananProgramBilgisi = katilimSaglananProgramBilgisiRepository.findById(katilimSaglananProgramBilgisi.getId()).get();
        // Disconnect from session so that the updates on updatedKatilimSaglananProgramBilgisi are not directly saved in db
        em.detach(updatedKatilimSaglananProgramBilgisi);
        updatedKatilimSaglananProgramBilgisi
            .programTuru(UPDATED_PROGRAM_TURU)
            .programGorevi(UPDATED_PROGRAM_GOREVI)
            .programAdi(UPDATED_PROGRAM_ADI)
            .programKonusu(UPDATED_PROGRAM_KONUSU)
            .baslangicYili(UPDATED_BASLANGIC_YILI)
            .bitisYili(UPDATED_BITIS_YILI)
            .duzenleyenKurum(UPDATED_DUZENLEYEN_KURUM)
            .ulke(UPDATED_ULKE)
            .sehir(UPDATED_SEHIR)
            .belgeTuru(UPDATED_BELGE_TURU)
            .anahtarKelimeler(UPDATED_ANAHTAR_KELIMELER);

        restKatilimSaglananProgramBilgisiMockMvc.perform(put("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedKatilimSaglananProgramBilgisi)))
            .andExpect(status().isOk());

        // Validate the KatilimSaglananProgramBilgisi in the database
        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeUpdate);
        KatilimSaglananProgramBilgisi testKatilimSaglananProgramBilgisi = katilimSaglananProgramBilgisiList.get(katilimSaglananProgramBilgisiList.size() - 1);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramTuru()).isEqualTo(UPDATED_PROGRAM_TURU);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramGorevi()).isEqualTo(UPDATED_PROGRAM_GOREVI);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramAdi()).isEqualTo(UPDATED_PROGRAM_ADI);
        assertThat(testKatilimSaglananProgramBilgisi.getProgramKonusu()).isEqualTo(UPDATED_PROGRAM_KONUSU);
        assertThat(testKatilimSaglananProgramBilgisi.getBaslangicYili()).isEqualTo(UPDATED_BASLANGIC_YILI);
        assertThat(testKatilimSaglananProgramBilgisi.getBitisYili()).isEqualTo(UPDATED_BITIS_YILI);
        assertThat(testKatilimSaglananProgramBilgisi.getDuzenleyenKurum()).isEqualTo(UPDATED_DUZENLEYEN_KURUM);
        assertThat(testKatilimSaglananProgramBilgisi.getUlke()).isEqualTo(UPDATED_ULKE);
        assertThat(testKatilimSaglananProgramBilgisi.getSehir()).isEqualTo(UPDATED_SEHIR);
        assertThat(testKatilimSaglananProgramBilgisi.getBelgeTuru()).isEqualTo(UPDATED_BELGE_TURU);
        assertThat(testKatilimSaglananProgramBilgisi.getAnahtarKelimeler()).isEqualTo(UPDATED_ANAHTAR_KELIMELER);
    }

    @Test
    @Transactional
    public void updateNonExistingKatilimSaglananProgramBilgisi() throws Exception {
        int databaseSizeBeforeUpdate = katilimSaglananProgramBilgisiRepository.findAll().size();

        // Create the KatilimSaglananProgramBilgisi

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKatilimSaglananProgramBilgisiMockMvc.perform(put("/api/katilim-saglanan-program-bilgisis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(katilimSaglananProgramBilgisi)))
            .andExpect(status().isBadRequest());

        // Validate the KatilimSaglananProgramBilgisi in the database
        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKatilimSaglananProgramBilgisi() throws Exception {
        // Initialize the database
        katilimSaglananProgramBilgisiService.save(katilimSaglananProgramBilgisi);

        int databaseSizeBeforeDelete = katilimSaglananProgramBilgisiRepository.findAll().size();

        // Get the katilimSaglananProgramBilgisi
        restKatilimSaglananProgramBilgisiMockMvc.perform(delete("/api/katilim-saglanan-program-bilgisis/{id}", katilimSaglananProgramBilgisi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisiList = katilimSaglananProgramBilgisiRepository.findAll();
        assertThat(katilimSaglananProgramBilgisiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KatilimSaglananProgramBilgisi.class);
        KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi1 = new KatilimSaglananProgramBilgisi();
        katilimSaglananProgramBilgisi1.setId(1L);
        KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi2 = new KatilimSaglananProgramBilgisi();
        katilimSaglananProgramBilgisi2.setId(katilimSaglananProgramBilgisi1.getId());
        assertThat(katilimSaglananProgramBilgisi1).isEqualTo(katilimSaglananProgramBilgisi2);
        katilimSaglananProgramBilgisi2.setId(2L);
        assertThat(katilimSaglananProgramBilgisi1).isNotEqualTo(katilimSaglananProgramBilgisi2);
        katilimSaglananProgramBilgisi1.setId(null);
        assertThat(katilimSaglananProgramBilgisi1).isNotEqualTo(katilimSaglananProgramBilgisi2);
    }
}
