package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.Istectrubeleri;
import projectimage.repository.IstectrubeleriRepository;
import projectimage.service.IstectrubeleriService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.CalismaAlani;
import projectimage.domain.enumeration.CalismaSekli;
import projectimage.domain.enumeration.Aylar;
import projectimage.domain.enumeration.Aylar;
import projectimage.domain.enumeration.Ulke;
import projectimage.domain.enumeration.Sehir;
/**
 * Test class for the IstectrubeleriResource REST controller.
 *
 * @see IstectrubeleriResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class IstectrubeleriResourceIntTest {

    private static final CalismaAlani DEFAULT_CALISMA_ALANI = CalismaAlani.KAMU;
    private static final CalismaAlani UPDATED_CALISMA_ALANI = CalismaAlani.OZEL;

    private static final CalismaSekli DEFAULT_CALISMA_SEKLI = CalismaSekli.TAM_ZAMANLI;
    private static final CalismaSekli UPDATED_CALISMA_SEKLI = CalismaSekli.YARI_ZAMANLI;

    private static final String DEFAULT_KURUM_ADI = "AAAAAAAAAA";
    private static final String UPDATED_KURUM_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_GOREV_BIRIMI = "AAAAAAAAAA";
    private static final String UPDATED_GOREV_BIRIMI = "BBBBBBBBBB";

    private static final String DEFAULT_UNVAN = "AAAAAAAAAA";
    private static final String UPDATED_UNVAN = "BBBBBBBBBB";

    private static final Aylar DEFAULT_BASLANGIC_AYI = Aylar.OCAK;
    private static final Aylar UPDATED_BASLANGIC_AYI = Aylar.SUBAT;

    private static final Integer DEFAULT_BASLANGIC_YILI = 1;
    private static final Integer UPDATED_BASLANGIC_YILI = 2;

    private static final Aylar DEFAULT_BITIS_AYI = Aylar.OCAK;
    private static final Aylar UPDATED_BITIS_AYI = Aylar.SUBAT;

    private static final Integer DEFAULT_BITIS_YILI = 1;
    private static final Integer UPDATED_BITIS_YILI = 2;

    private static final String DEFAULT_GOREV_TANIMI = "AAAAAAAAAA";
    private static final String UPDATED_GOREV_TANIMI = "BBBBBBBBBB";

    private static final Ulke DEFAULT_ULKE = Ulke.TURKIYE;
    private static final Ulke UPDATED_ULKE = Ulke.INGILTERE;

    private static final Sehir DEFAULT_SEHIR = Sehir.ADANA;
    private static final Sehir UPDATED_SEHIR = Sehir.ADIYAMAN;

    private static final String DEFAULT_YARARLI_BILGIER = "AAAAAAAAAA";
    private static final String UPDATED_YARARLI_BILGIER = "BBBBBBBBBB";

    @Autowired
    private IstectrubeleriRepository istectrubeleriRepository;

    @Autowired
    private IstectrubeleriService istectrubeleriService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restIstectrubeleriMockMvc;

    private Istectrubeleri istectrubeleri;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IstectrubeleriResource istectrubeleriResource = new IstectrubeleriResource(istectrubeleriService);
        this.restIstectrubeleriMockMvc = MockMvcBuilders.standaloneSetup(istectrubeleriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Istectrubeleri createEntity(EntityManager em) {
        Istectrubeleri istectrubeleri = new Istectrubeleri()
            .calismaAlani(DEFAULT_CALISMA_ALANI)
            .calismaSekli(DEFAULT_CALISMA_SEKLI)
            .kurumAdi(DEFAULT_KURUM_ADI)
            .gorevBirimi(DEFAULT_GOREV_BIRIMI)
            .unvan(DEFAULT_UNVAN)
            .baslangicAyi(DEFAULT_BASLANGIC_AYI)
            .baslangicYili(DEFAULT_BASLANGIC_YILI)
            .bitisAyi(DEFAULT_BITIS_AYI)
            .bitisYili(DEFAULT_BITIS_YILI)
            .gorevTanimi(DEFAULT_GOREV_TANIMI)
            .ulke(DEFAULT_ULKE)
            .sehir(DEFAULT_SEHIR)
            .yararliBilgier(DEFAULT_YARARLI_BILGIER);
        return istectrubeleri;
    }

    @Before
    public void initTest() {
        istectrubeleri = createEntity(em);
    }

    @Test
    @Transactional
    public void createIstectrubeleri() throws Exception {
        int databaseSizeBeforeCreate = istectrubeleriRepository.findAll().size();

        // Create the Istectrubeleri
        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isCreated());

        // Validate the Istectrubeleri in the database
        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeCreate + 1);
        Istectrubeleri testIstectrubeleri = istectrubeleriList.get(istectrubeleriList.size() - 1);
        assertThat(testIstectrubeleri.getCalismaAlani()).isEqualTo(DEFAULT_CALISMA_ALANI);
        assertThat(testIstectrubeleri.getCalismaSekli()).isEqualTo(DEFAULT_CALISMA_SEKLI);
        assertThat(testIstectrubeleri.getKurumAdi()).isEqualTo(DEFAULT_KURUM_ADI);
        assertThat(testIstectrubeleri.getGorevBirimi()).isEqualTo(DEFAULT_GOREV_BIRIMI);
        assertThat(testIstectrubeleri.getUnvan()).isEqualTo(DEFAULT_UNVAN);
        assertThat(testIstectrubeleri.getBaslangicAyi()).isEqualTo(DEFAULT_BASLANGIC_AYI);
        assertThat(testIstectrubeleri.getBaslangicYili()).isEqualTo(DEFAULT_BASLANGIC_YILI);
        assertThat(testIstectrubeleri.getBitisAyi()).isEqualTo(DEFAULT_BITIS_AYI);
        assertThat(testIstectrubeleri.getBitisYili()).isEqualTo(DEFAULT_BITIS_YILI);
        assertThat(testIstectrubeleri.getGorevTanimi()).isEqualTo(DEFAULT_GOREV_TANIMI);
        assertThat(testIstectrubeleri.getUlke()).isEqualTo(DEFAULT_ULKE);
        assertThat(testIstectrubeleri.getSehir()).isEqualTo(DEFAULT_SEHIR);
        assertThat(testIstectrubeleri.getYararliBilgier()).isEqualTo(DEFAULT_YARARLI_BILGIER);
    }

    @Test
    @Transactional
    public void createIstectrubeleriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = istectrubeleriRepository.findAll().size();

        // Create the Istectrubeleri with an existing ID
        istectrubeleri.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        // Validate the Istectrubeleri in the database
        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCalismaAlaniIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setCalismaAlani(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCalismaSekliIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setCalismaSekli(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkKurumAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setKurumAdi(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGorevBirimiIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setGorevBirimi(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUnvanIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setUnvan(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBaslangicAyiIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setBaslangicAyi(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBaslangicYiliIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setBaslangicYili(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBitisAyiIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setBitisAyi(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBitisYiliIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setBitisYili(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUlkeIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setUlke(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSehirIsRequired() throws Exception {
        int databaseSizeBeforeTest = istectrubeleriRepository.findAll().size();
        // set the field null
        istectrubeleri.setSehir(null);

        // Create the Istectrubeleri, which fails.

        restIstectrubeleriMockMvc.perform(post("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIstectrubeleris() throws Exception {
        // Initialize the database
        istectrubeleriRepository.saveAndFlush(istectrubeleri);

        // Get all the istectrubeleriList
        restIstectrubeleriMockMvc.perform(get("/api/istectrubeleris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(istectrubeleri.getId().intValue())))
            .andExpect(jsonPath("$.[*].calismaAlani").value(hasItem(DEFAULT_CALISMA_ALANI.toString())))
            .andExpect(jsonPath("$.[*].calismaSekli").value(hasItem(DEFAULT_CALISMA_SEKLI.toString())))
            .andExpect(jsonPath("$.[*].kurumAdi").value(hasItem(DEFAULT_KURUM_ADI.toString())))
            .andExpect(jsonPath("$.[*].gorevBirimi").value(hasItem(DEFAULT_GOREV_BIRIMI.toString())))
            .andExpect(jsonPath("$.[*].unvan").value(hasItem(DEFAULT_UNVAN.toString())))
            .andExpect(jsonPath("$.[*].baslangicAyi").value(hasItem(DEFAULT_BASLANGIC_AYI.toString())))
            .andExpect(jsonPath("$.[*].baslangicYili").value(hasItem(DEFAULT_BASLANGIC_YILI)))
            .andExpect(jsonPath("$.[*].bitisAyi").value(hasItem(DEFAULT_BITIS_AYI.toString())))
            .andExpect(jsonPath("$.[*].bitisYili").value(hasItem(DEFAULT_BITIS_YILI)))
            .andExpect(jsonPath("$.[*].gorevTanimi").value(hasItem(DEFAULT_GOREV_TANIMI.toString())))
            .andExpect(jsonPath("$.[*].ulke").value(hasItem(DEFAULT_ULKE.toString())))
            .andExpect(jsonPath("$.[*].sehir").value(hasItem(DEFAULT_SEHIR.toString())))
            .andExpect(jsonPath("$.[*].yararliBilgier").value(hasItem(DEFAULT_YARARLI_BILGIER.toString())));
    }
    
    @Test
    @Transactional
    public void getIstectrubeleri() throws Exception {
        // Initialize the database
        istectrubeleriRepository.saveAndFlush(istectrubeleri);

        // Get the istectrubeleri
        restIstectrubeleriMockMvc.perform(get("/api/istectrubeleris/{id}", istectrubeleri.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(istectrubeleri.getId().intValue()))
            .andExpect(jsonPath("$.calismaAlani").value(DEFAULT_CALISMA_ALANI.toString()))
            .andExpect(jsonPath("$.calismaSekli").value(DEFAULT_CALISMA_SEKLI.toString()))
            .andExpect(jsonPath("$.kurumAdi").value(DEFAULT_KURUM_ADI.toString()))
            .andExpect(jsonPath("$.gorevBirimi").value(DEFAULT_GOREV_BIRIMI.toString()))
            .andExpect(jsonPath("$.unvan").value(DEFAULT_UNVAN.toString()))
            .andExpect(jsonPath("$.baslangicAyi").value(DEFAULT_BASLANGIC_AYI.toString()))
            .andExpect(jsonPath("$.baslangicYili").value(DEFAULT_BASLANGIC_YILI))
            .andExpect(jsonPath("$.bitisAyi").value(DEFAULT_BITIS_AYI.toString()))
            .andExpect(jsonPath("$.bitisYili").value(DEFAULT_BITIS_YILI))
            .andExpect(jsonPath("$.gorevTanimi").value(DEFAULT_GOREV_TANIMI.toString()))
            .andExpect(jsonPath("$.ulke").value(DEFAULT_ULKE.toString()))
            .andExpect(jsonPath("$.sehir").value(DEFAULT_SEHIR.toString()))
            .andExpect(jsonPath("$.yararliBilgier").value(DEFAULT_YARARLI_BILGIER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingIstectrubeleri() throws Exception {
        // Get the istectrubeleri
        restIstectrubeleriMockMvc.perform(get("/api/istectrubeleris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIstectrubeleri() throws Exception {
        // Initialize the database
        istectrubeleriService.save(istectrubeleri);

        int databaseSizeBeforeUpdate = istectrubeleriRepository.findAll().size();

        // Update the istectrubeleri
        Istectrubeleri updatedIstectrubeleri = istectrubeleriRepository.findById(istectrubeleri.getId()).get();
        // Disconnect from session so that the updates on updatedIstectrubeleri are not directly saved in db
        em.detach(updatedIstectrubeleri);
        updatedIstectrubeleri
            .calismaAlani(UPDATED_CALISMA_ALANI)
            .calismaSekli(UPDATED_CALISMA_SEKLI)
            .kurumAdi(UPDATED_KURUM_ADI)
            .gorevBirimi(UPDATED_GOREV_BIRIMI)
            .unvan(UPDATED_UNVAN)
            .baslangicAyi(UPDATED_BASLANGIC_AYI)
            .baslangicYili(UPDATED_BASLANGIC_YILI)
            .bitisAyi(UPDATED_BITIS_AYI)
            .bitisYili(UPDATED_BITIS_YILI)
            .gorevTanimi(UPDATED_GOREV_TANIMI)
            .ulke(UPDATED_ULKE)
            .sehir(UPDATED_SEHIR)
            .yararliBilgier(UPDATED_YARARLI_BILGIER);

        restIstectrubeleriMockMvc.perform(put("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIstectrubeleri)))
            .andExpect(status().isOk());

        // Validate the Istectrubeleri in the database
        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeUpdate);
        Istectrubeleri testIstectrubeleri = istectrubeleriList.get(istectrubeleriList.size() - 1);
        assertThat(testIstectrubeleri.getCalismaAlani()).isEqualTo(UPDATED_CALISMA_ALANI);
        assertThat(testIstectrubeleri.getCalismaSekli()).isEqualTo(UPDATED_CALISMA_SEKLI);
        assertThat(testIstectrubeleri.getKurumAdi()).isEqualTo(UPDATED_KURUM_ADI);
        assertThat(testIstectrubeleri.getGorevBirimi()).isEqualTo(UPDATED_GOREV_BIRIMI);
        assertThat(testIstectrubeleri.getUnvan()).isEqualTo(UPDATED_UNVAN);
        assertThat(testIstectrubeleri.getBaslangicAyi()).isEqualTo(UPDATED_BASLANGIC_AYI);
        assertThat(testIstectrubeleri.getBaslangicYili()).isEqualTo(UPDATED_BASLANGIC_YILI);
        assertThat(testIstectrubeleri.getBitisAyi()).isEqualTo(UPDATED_BITIS_AYI);
        assertThat(testIstectrubeleri.getBitisYili()).isEqualTo(UPDATED_BITIS_YILI);
        assertThat(testIstectrubeleri.getGorevTanimi()).isEqualTo(UPDATED_GOREV_TANIMI);
        assertThat(testIstectrubeleri.getUlke()).isEqualTo(UPDATED_ULKE);
        assertThat(testIstectrubeleri.getSehir()).isEqualTo(UPDATED_SEHIR);
        assertThat(testIstectrubeleri.getYararliBilgier()).isEqualTo(UPDATED_YARARLI_BILGIER);
    }

    @Test
    @Transactional
    public void updateNonExistingIstectrubeleri() throws Exception {
        int databaseSizeBeforeUpdate = istectrubeleriRepository.findAll().size();

        // Create the Istectrubeleri

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIstectrubeleriMockMvc.perform(put("/api/istectrubeleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(istectrubeleri)))
            .andExpect(status().isBadRequest());

        // Validate the Istectrubeleri in the database
        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIstectrubeleri() throws Exception {
        // Initialize the database
        istectrubeleriService.save(istectrubeleri);

        int databaseSizeBeforeDelete = istectrubeleriRepository.findAll().size();

        // Get the istectrubeleri
        restIstectrubeleriMockMvc.perform(delete("/api/istectrubeleris/{id}", istectrubeleri.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Istectrubeleri> istectrubeleriList = istectrubeleriRepository.findAll();
        assertThat(istectrubeleriList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Istectrubeleri.class);
        Istectrubeleri istectrubeleri1 = new Istectrubeleri();
        istectrubeleri1.setId(1L);
        Istectrubeleri istectrubeleri2 = new Istectrubeleri();
        istectrubeleri2.setId(istectrubeleri1.getId());
        assertThat(istectrubeleri1).isEqualTo(istectrubeleri2);
        istectrubeleri2.setId(2L);
        assertThat(istectrubeleri1).isNotEqualTo(istectrubeleri2);
        istectrubeleri1.setId(null);
        assertThat(istectrubeleri1).isNotEqualTo(istectrubeleri2);
    }
}
