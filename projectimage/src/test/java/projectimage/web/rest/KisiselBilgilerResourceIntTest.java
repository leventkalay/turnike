package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.KisiselBilgiler;
import projectimage.repository.KisiselBilgilerRepository;
import projectimage.service.KisiselBilgilerService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.MedeniDurum;
import projectimage.domain.enumeration.Cinsiyet;
import projectimage.domain.enumeration.IstihdamSekli;
import projectimage.domain.enumeration.Askerlik;
import projectimage.domain.enumeration.KanGrubu;
/**
 * Test class for the KisiselBilgilerResource REST controller.
 *
 * @see KisiselBilgilerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class KisiselBilgilerResourceIntTest {

    private static final String DEFAULT_AD = "AAAAAAAAAA";
    private static final String UPDATED_AD = "BBBBBBBBBB";

    private static final String DEFAULT_SOYAD = "AAAAAAAAAA";
    private static final String UPDATED_SOYAD = "BBBBBBBBBB";

    private static final Integer DEFAULT_PIRIM_GUN_SAYISI = 1;
    private static final Integer UPDATED_PIRIM_GUN_SAYISI = 2;

    private static final String DEFAULT_SGK_NO = "AAAAAAAAAA";
    private static final String UPDATED_SGK_NO = "BBBBBBBBBB";

    private static final String DEFAULT_SICIL_NO = "AAAAAAAAAA";
    private static final String UPDATED_SICIL_NO = "BBBBBBBBBB";

    private static final MedeniDurum DEFAULT_MEDENI_DURUM = MedeniDurum.EVLI;
    private static final MedeniDurum UPDATED_MEDENI_DURUM = MedeniDurum.BEKAR;

    private static final Cinsiyet DEFAULT_CINSIYET = Cinsiyet.Erkek;
    private static final Cinsiyet UPDATED_CINSIYET = Cinsiyet.KADIN;

    private static final IstihdamSekli DEFAULT_ISTIHDAM_SEKLI = IstihdamSekli.SK_657;
    private static final IstihdamSekli UPDATED_ISTIHDAM_SEKLI = IstihdamSekli.KHK_696;

    private static final Askerlik DEFAULT_ASKERLIK = Askerlik.YAPTI;
    private static final Askerlik UPDATED_ASKERLIK = Askerlik.YAPMADI;

    private static final KanGrubu DEFAULT_KANGRUBU = KanGrubu.BILINMIYOR;
    private static final KanGrubu UPDATED_KANGRUBU = KanGrubu.APozitif;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_ANY = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_ANY = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_ANY_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_ANY_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    @Autowired
    private KisiselBilgilerRepository kisiselBilgilerRepository;

    @Autowired
    private KisiselBilgilerService kisiselBilgilerService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restKisiselBilgilerMockMvc;

    private KisiselBilgiler kisiselBilgiler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KisiselBilgilerResource kisiselBilgilerResource = new KisiselBilgilerResource(kisiselBilgilerService);
        this.restKisiselBilgilerMockMvc = MockMvcBuilders.standaloneSetup(kisiselBilgilerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KisiselBilgiler createEntity(EntityManager em) {
        KisiselBilgiler kisiselBilgiler = new KisiselBilgiler()
            .ad(DEFAULT_AD)
            .soyad(DEFAULT_SOYAD)
            .pirimGunSayisi(DEFAULT_PIRIM_GUN_SAYISI)
            .sgkNo(DEFAULT_SGK_NO)
            .sicilNo(DEFAULT_SICIL_NO)
            .medeniDurum(DEFAULT_MEDENI_DURUM)
            .cinsiyet(DEFAULT_CINSIYET)
            .istihdamSekli(DEFAULT_ISTIHDAM_SEKLI)
            .askerlik(DEFAULT_ASKERLIK)
            .kangrubu(DEFAULT_KANGRUBU)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .any(DEFAULT_ANY)
            .anyContentType(DEFAULT_ANY_CONTENT_TYPE)
            .text(DEFAULT_TEXT);
        return kisiselBilgiler;
    }

    @Before
    public void initTest() {
        kisiselBilgiler = createEntity(em);
    }

    @Test
    @Transactional
    public void createKisiselBilgiler() throws Exception {
        int databaseSizeBeforeCreate = kisiselBilgilerRepository.findAll().size();

        // Create the KisiselBilgiler
        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isCreated());

        // Validate the KisiselBilgiler in the database
        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeCreate + 1);
        KisiselBilgiler testKisiselBilgiler = kisiselBilgilerList.get(kisiselBilgilerList.size() - 1);
        assertThat(testKisiselBilgiler.getAd()).isEqualTo(DEFAULT_AD);
        assertThat(testKisiselBilgiler.getSoyad()).isEqualTo(DEFAULT_SOYAD);
        assertThat(testKisiselBilgiler.getPirimGunSayisi()).isEqualTo(DEFAULT_PIRIM_GUN_SAYISI);
        assertThat(testKisiselBilgiler.getSgkNo()).isEqualTo(DEFAULT_SGK_NO);
        assertThat(testKisiselBilgiler.getSicilNo()).isEqualTo(DEFAULT_SICIL_NO);
        assertThat(testKisiselBilgiler.getMedeniDurum()).isEqualTo(DEFAULT_MEDENI_DURUM);
        assertThat(testKisiselBilgiler.getCinsiyet()).isEqualTo(DEFAULT_CINSIYET);
        assertThat(testKisiselBilgiler.getIstihdamSekli()).isEqualTo(DEFAULT_ISTIHDAM_SEKLI);
        assertThat(testKisiselBilgiler.getAskerlik()).isEqualTo(DEFAULT_ASKERLIK);
        assertThat(testKisiselBilgiler.getKangrubu()).isEqualTo(DEFAULT_KANGRUBU);
        assertThat(testKisiselBilgiler.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testKisiselBilgiler.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testKisiselBilgiler.getAny()).isEqualTo(DEFAULT_ANY);
        assertThat(testKisiselBilgiler.getAnyContentType()).isEqualTo(DEFAULT_ANY_CONTENT_TYPE);
        assertThat(testKisiselBilgiler.getText()).isEqualTo(DEFAULT_TEXT);
    }

    @Test
    @Transactional
    public void createKisiselBilgilerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kisiselBilgilerRepository.findAll().size();

        // Create the KisiselBilgiler with an existing ID
        kisiselBilgiler.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        // Validate the KisiselBilgiler in the database
        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAdIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setAd(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSoyadIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setSoyad(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSgkNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setSgkNo(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSicilNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setSicilNo(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMedeniDurumIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setMedeniDurum(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCinsiyetIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setCinsiyet(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIstihdamSekliIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setIstihdamSekli(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAskerlikIsRequired() throws Exception {
        int databaseSizeBeforeTest = kisiselBilgilerRepository.findAll().size();
        // set the field null
        kisiselBilgiler.setAskerlik(null);

        // Create the KisiselBilgiler, which fails.

        restKisiselBilgilerMockMvc.perform(post("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllKisiselBilgilers() throws Exception {
        // Initialize the database
        kisiselBilgilerRepository.saveAndFlush(kisiselBilgiler);

        // Get all the kisiselBilgilerList
        restKisiselBilgilerMockMvc.perform(get("/api/kisisel-bilgilers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kisiselBilgiler.getId().intValue())))
            .andExpect(jsonPath("$.[*].ad").value(hasItem(DEFAULT_AD.toString())))
            .andExpect(jsonPath("$.[*].soyad").value(hasItem(DEFAULT_SOYAD.toString())))
            .andExpect(jsonPath("$.[*].pirimGunSayisi").value(hasItem(DEFAULT_PIRIM_GUN_SAYISI)))
            .andExpect(jsonPath("$.[*].sgkNo").value(hasItem(DEFAULT_SGK_NO.toString())))
            .andExpect(jsonPath("$.[*].sicilNo").value(hasItem(DEFAULT_SICIL_NO.toString())))
            .andExpect(jsonPath("$.[*].medeniDurum").value(hasItem(DEFAULT_MEDENI_DURUM.toString())))
            .andExpect(jsonPath("$.[*].cinsiyet").value(hasItem(DEFAULT_CINSIYET.toString())))
            .andExpect(jsonPath("$.[*].istihdamSekli").value(hasItem(DEFAULT_ISTIHDAM_SEKLI.toString())))
            .andExpect(jsonPath("$.[*].askerlik").value(hasItem(DEFAULT_ASKERLIK.toString())))
            .andExpect(jsonPath("$.[*].kangrubu").value(hasItem(DEFAULT_KANGRUBU.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].anyContentType").value(hasItem(DEFAULT_ANY_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].any").value(hasItem(Base64Utils.encodeToString(DEFAULT_ANY))))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())));
    }
    
    @Test
    @Transactional
    public void getKisiselBilgiler() throws Exception {
        // Initialize the database
        kisiselBilgilerRepository.saveAndFlush(kisiselBilgiler);

        // Get the kisiselBilgiler
        restKisiselBilgilerMockMvc.perform(get("/api/kisisel-bilgilers/{id}", kisiselBilgiler.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(kisiselBilgiler.getId().intValue()))
            .andExpect(jsonPath("$.ad").value(DEFAULT_AD.toString()))
            .andExpect(jsonPath("$.soyad").value(DEFAULT_SOYAD.toString()))
            .andExpect(jsonPath("$.pirimGunSayisi").value(DEFAULT_PIRIM_GUN_SAYISI))
            .andExpect(jsonPath("$.sgkNo").value(DEFAULT_SGK_NO.toString()))
            .andExpect(jsonPath("$.sicilNo").value(DEFAULT_SICIL_NO.toString()))
            .andExpect(jsonPath("$.medeniDurum").value(DEFAULT_MEDENI_DURUM.toString()))
            .andExpect(jsonPath("$.cinsiyet").value(DEFAULT_CINSIYET.toString()))
            .andExpect(jsonPath("$.istihdamSekli").value(DEFAULT_ISTIHDAM_SEKLI.toString()))
            .andExpect(jsonPath("$.askerlik").value(DEFAULT_ASKERLIK.toString()))
            .andExpect(jsonPath("$.kangrubu").value(DEFAULT_KANGRUBU.toString()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.anyContentType").value(DEFAULT_ANY_CONTENT_TYPE))
            .andExpect(jsonPath("$.any").value(Base64Utils.encodeToString(DEFAULT_ANY)))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingKisiselBilgiler() throws Exception {
        // Get the kisiselBilgiler
        restKisiselBilgilerMockMvc.perform(get("/api/kisisel-bilgilers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKisiselBilgiler() throws Exception {
        // Initialize the database
        kisiselBilgilerService.save(kisiselBilgiler);

        int databaseSizeBeforeUpdate = kisiselBilgilerRepository.findAll().size();

        // Update the kisiselBilgiler
        KisiselBilgiler updatedKisiselBilgiler = kisiselBilgilerRepository.findById(kisiselBilgiler.getId()).get();
        // Disconnect from session so that the updates on updatedKisiselBilgiler are not directly saved in db
        em.detach(updatedKisiselBilgiler);
        updatedKisiselBilgiler
            .ad(UPDATED_AD)
            .soyad(UPDATED_SOYAD)
            .pirimGunSayisi(UPDATED_PIRIM_GUN_SAYISI)
            .sgkNo(UPDATED_SGK_NO)
            .sicilNo(UPDATED_SICIL_NO)
            .medeniDurum(UPDATED_MEDENI_DURUM)
            .cinsiyet(UPDATED_CINSIYET)
            .istihdamSekli(UPDATED_ISTIHDAM_SEKLI)
            .askerlik(UPDATED_ASKERLIK)
            .kangrubu(UPDATED_KANGRUBU)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .any(UPDATED_ANY)
            .anyContentType(UPDATED_ANY_CONTENT_TYPE)
            .text(UPDATED_TEXT);

        restKisiselBilgilerMockMvc.perform(put("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedKisiselBilgiler)))
            .andExpect(status().isOk());

        // Validate the KisiselBilgiler in the database
        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeUpdate);
        KisiselBilgiler testKisiselBilgiler = kisiselBilgilerList.get(kisiselBilgilerList.size() - 1);
        assertThat(testKisiselBilgiler.getAd()).isEqualTo(UPDATED_AD);
        assertThat(testKisiselBilgiler.getSoyad()).isEqualTo(UPDATED_SOYAD);
        assertThat(testKisiselBilgiler.getPirimGunSayisi()).isEqualTo(UPDATED_PIRIM_GUN_SAYISI);
        assertThat(testKisiselBilgiler.getSgkNo()).isEqualTo(UPDATED_SGK_NO);
        assertThat(testKisiselBilgiler.getSicilNo()).isEqualTo(UPDATED_SICIL_NO);
        assertThat(testKisiselBilgiler.getMedeniDurum()).isEqualTo(UPDATED_MEDENI_DURUM);
        assertThat(testKisiselBilgiler.getCinsiyet()).isEqualTo(UPDATED_CINSIYET);
        assertThat(testKisiselBilgiler.getIstihdamSekli()).isEqualTo(UPDATED_ISTIHDAM_SEKLI);
        assertThat(testKisiselBilgiler.getAskerlik()).isEqualTo(UPDATED_ASKERLIK);
        assertThat(testKisiselBilgiler.getKangrubu()).isEqualTo(UPDATED_KANGRUBU);
        assertThat(testKisiselBilgiler.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testKisiselBilgiler.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testKisiselBilgiler.getAny()).isEqualTo(UPDATED_ANY);
        assertThat(testKisiselBilgiler.getAnyContentType()).isEqualTo(UPDATED_ANY_CONTENT_TYPE);
        assertThat(testKisiselBilgiler.getText()).isEqualTo(UPDATED_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingKisiselBilgiler() throws Exception {
        int databaseSizeBeforeUpdate = kisiselBilgilerRepository.findAll().size();

        // Create the KisiselBilgiler

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKisiselBilgilerMockMvc.perform(put("/api/kisisel-bilgilers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kisiselBilgiler)))
            .andExpect(status().isBadRequest());

        // Validate the KisiselBilgiler in the database
        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKisiselBilgiler() throws Exception {
        // Initialize the database
        kisiselBilgilerService.save(kisiselBilgiler);

        int databaseSizeBeforeDelete = kisiselBilgilerRepository.findAll().size();

        // Get the kisiselBilgiler
        restKisiselBilgilerMockMvc.perform(delete("/api/kisisel-bilgilers/{id}", kisiselBilgiler.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<KisiselBilgiler> kisiselBilgilerList = kisiselBilgilerRepository.findAll();
        assertThat(kisiselBilgilerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KisiselBilgiler.class);
        KisiselBilgiler kisiselBilgiler1 = new KisiselBilgiler();
        kisiselBilgiler1.setId(1L);
        KisiselBilgiler kisiselBilgiler2 = new KisiselBilgiler();
        kisiselBilgiler2.setId(kisiselBilgiler1.getId());
        assertThat(kisiselBilgiler1).isEqualTo(kisiselBilgiler2);
        kisiselBilgiler2.setId(2L);
        assertThat(kisiselBilgiler1).isNotEqualTo(kisiselBilgiler2);
        kisiselBilgiler1.setId(null);
        assertThat(kisiselBilgiler1).isNotEqualTo(kisiselBilgiler2);
    }
}
