package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.OgrenimBilgileri;
import projectimage.repository.OgrenimBilgileriRepository;
import projectimage.service.OgrenimBilgileriService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.OgrenimDurumu;
import projectimage.domain.enumeration.Diller;
import projectimage.domain.enumeration.Ulke;
import projectimage.domain.enumeration.Sehir;
/**
 * Test class for the OgrenimBilgileriResource REST controller.
 *
 * @see OgrenimBilgileriResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class OgrenimBilgileriResourceIntTest {

    private static final OgrenimDurumu DEFAULT_OGRENIM_DURUMU = OgrenimDurumu.ILKOKUL;
    private static final OgrenimDurumu UPDATED_OGRENIM_DURUMU = OgrenimDurumu.ORTAOKUL;

    private static final String DEFAULT_OKUL = "AAAAAAAAAA";
    private static final String UPDATED_OKUL = "BBBBBBBBBB";

    private static final Integer DEFAULT_MEZUNIYET_YILI = 1;
    private static final Integer UPDATED_MEZUNIYET_YILI = 2;

    private static final Diller DEFAULT_OGRENIM_DILI = Diller.TURKCE;
    private static final Diller UPDATED_OGRENIM_DILI = Diller.INGILIZCE;

    private static final Ulke DEFAULT_ULKE = Ulke.TURKIYE;
    private static final Ulke UPDATED_ULKE = Ulke.INGILTERE;

    private static final Sehir DEFAULT_SEHIR = Sehir.ADANA;
    private static final Sehir UPDATED_SEHIR = Sehir.ADIYAMAN;

    @Autowired
    private OgrenimBilgileriRepository ogrenimBilgileriRepository;

    @Autowired
    private OgrenimBilgileriService ogrenimBilgileriService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOgrenimBilgileriMockMvc;

    private OgrenimBilgileri ogrenimBilgileri;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OgrenimBilgileriResource ogrenimBilgileriResource = new OgrenimBilgileriResource(ogrenimBilgileriService);
        this.restOgrenimBilgileriMockMvc = MockMvcBuilders.standaloneSetup(ogrenimBilgileriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OgrenimBilgileri createEntity(EntityManager em) {
        OgrenimBilgileri ogrenimBilgileri = new OgrenimBilgileri()
            .ogrenimDurumu(DEFAULT_OGRENIM_DURUMU)
            .okul(DEFAULT_OKUL)
            .mezuniyetYili(DEFAULT_MEZUNIYET_YILI)
            .ogrenimDili(DEFAULT_OGRENIM_DILI)
            .ulke(DEFAULT_ULKE)
            .sehir(DEFAULT_SEHIR);
        return ogrenimBilgileri;
    }

    @Before
    public void initTest() {
        ogrenimBilgileri = createEntity(em);
    }

    @Test
    @Transactional
    public void createOgrenimBilgileri() throws Exception {
        int databaseSizeBeforeCreate = ogrenimBilgileriRepository.findAll().size();

        // Create the OgrenimBilgileri
        restOgrenimBilgileriMockMvc.perform(post("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isCreated());

        // Validate the OgrenimBilgileri in the database
        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeCreate + 1);
        OgrenimBilgileri testOgrenimBilgileri = ogrenimBilgileriList.get(ogrenimBilgileriList.size() - 1);
        assertThat(testOgrenimBilgileri.getOgrenimDurumu()).isEqualTo(DEFAULT_OGRENIM_DURUMU);
        assertThat(testOgrenimBilgileri.getOkul()).isEqualTo(DEFAULT_OKUL);
        assertThat(testOgrenimBilgileri.getMezuniyetYili()).isEqualTo(DEFAULT_MEZUNIYET_YILI);
        assertThat(testOgrenimBilgileri.getOgrenimDili()).isEqualTo(DEFAULT_OGRENIM_DILI);
        assertThat(testOgrenimBilgileri.getUlke()).isEqualTo(DEFAULT_ULKE);
        assertThat(testOgrenimBilgileri.getSehir()).isEqualTo(DEFAULT_SEHIR);
    }

    @Test
    @Transactional
    public void createOgrenimBilgileriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ogrenimBilgileriRepository.findAll().size();

        // Create the OgrenimBilgileri with an existing ID
        ogrenimBilgileri.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOgrenimBilgileriMockMvc.perform(post("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isBadRequest());

        // Validate the OgrenimBilgileri in the database
        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkOgrenimDurumuIsRequired() throws Exception {
        int databaseSizeBeforeTest = ogrenimBilgileriRepository.findAll().size();
        // set the field null
        ogrenimBilgileri.setOgrenimDurumu(null);

        // Create the OgrenimBilgileri, which fails.

        restOgrenimBilgileriMockMvc.perform(post("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isBadRequest());

        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOkulIsRequired() throws Exception {
        int databaseSizeBeforeTest = ogrenimBilgileriRepository.findAll().size();
        // set the field null
        ogrenimBilgileri.setOkul(null);

        // Create the OgrenimBilgileri, which fails.

        restOgrenimBilgileriMockMvc.perform(post("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isBadRequest());

        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOgrenimDiliIsRequired() throws Exception {
        int databaseSizeBeforeTest = ogrenimBilgileriRepository.findAll().size();
        // set the field null
        ogrenimBilgileri.setOgrenimDili(null);

        // Create the OgrenimBilgileri, which fails.

        restOgrenimBilgileriMockMvc.perform(post("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isBadRequest());

        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUlkeIsRequired() throws Exception {
        int databaseSizeBeforeTest = ogrenimBilgileriRepository.findAll().size();
        // set the field null
        ogrenimBilgileri.setUlke(null);

        // Create the OgrenimBilgileri, which fails.

        restOgrenimBilgileriMockMvc.perform(post("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isBadRequest());

        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSehirIsRequired() throws Exception {
        int databaseSizeBeforeTest = ogrenimBilgileriRepository.findAll().size();
        // set the field null
        ogrenimBilgileri.setSehir(null);

        // Create the OgrenimBilgileri, which fails.

        restOgrenimBilgileriMockMvc.perform(post("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isBadRequest());

        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOgrenimBilgileris() throws Exception {
        // Initialize the database
        ogrenimBilgileriRepository.saveAndFlush(ogrenimBilgileri);

        // Get all the ogrenimBilgileriList
        restOgrenimBilgileriMockMvc.perform(get("/api/ogrenim-bilgileris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ogrenimBilgileri.getId().intValue())))
            .andExpect(jsonPath("$.[*].ogrenimDurumu").value(hasItem(DEFAULT_OGRENIM_DURUMU.toString())))
            .andExpect(jsonPath("$.[*].okul").value(hasItem(DEFAULT_OKUL.toString())))
            .andExpect(jsonPath("$.[*].mezuniyetYili").value(hasItem(DEFAULT_MEZUNIYET_YILI)))
            .andExpect(jsonPath("$.[*].ogrenimDili").value(hasItem(DEFAULT_OGRENIM_DILI.toString())))
            .andExpect(jsonPath("$.[*].ulke").value(hasItem(DEFAULT_ULKE.toString())))
            .andExpect(jsonPath("$.[*].sehir").value(hasItem(DEFAULT_SEHIR.toString())));
    }
    
    @Test
    @Transactional
    public void getOgrenimBilgileri() throws Exception {
        // Initialize the database
        ogrenimBilgileriRepository.saveAndFlush(ogrenimBilgileri);

        // Get the ogrenimBilgileri
        restOgrenimBilgileriMockMvc.perform(get("/api/ogrenim-bilgileris/{id}", ogrenimBilgileri.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ogrenimBilgileri.getId().intValue()))
            .andExpect(jsonPath("$.ogrenimDurumu").value(DEFAULT_OGRENIM_DURUMU.toString()))
            .andExpect(jsonPath("$.okul").value(DEFAULT_OKUL.toString()))
            .andExpect(jsonPath("$.mezuniyetYili").value(DEFAULT_MEZUNIYET_YILI))
            .andExpect(jsonPath("$.ogrenimDili").value(DEFAULT_OGRENIM_DILI.toString()))
            .andExpect(jsonPath("$.ulke").value(DEFAULT_ULKE.toString()))
            .andExpect(jsonPath("$.sehir").value(DEFAULT_SEHIR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOgrenimBilgileri() throws Exception {
        // Get the ogrenimBilgileri
        restOgrenimBilgileriMockMvc.perform(get("/api/ogrenim-bilgileris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOgrenimBilgileri() throws Exception {
        // Initialize the database
        ogrenimBilgileriService.save(ogrenimBilgileri);

        int databaseSizeBeforeUpdate = ogrenimBilgileriRepository.findAll().size();

        // Update the ogrenimBilgileri
        OgrenimBilgileri updatedOgrenimBilgileri = ogrenimBilgileriRepository.findById(ogrenimBilgileri.getId()).get();
        // Disconnect from session so that the updates on updatedOgrenimBilgileri are not directly saved in db
        em.detach(updatedOgrenimBilgileri);
        updatedOgrenimBilgileri
            .ogrenimDurumu(UPDATED_OGRENIM_DURUMU)
            .okul(UPDATED_OKUL)
            .mezuniyetYili(UPDATED_MEZUNIYET_YILI)
            .ogrenimDili(UPDATED_OGRENIM_DILI)
            .ulke(UPDATED_ULKE)
            .sehir(UPDATED_SEHIR);

        restOgrenimBilgileriMockMvc.perform(put("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOgrenimBilgileri)))
            .andExpect(status().isOk());

        // Validate the OgrenimBilgileri in the database
        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeUpdate);
        OgrenimBilgileri testOgrenimBilgileri = ogrenimBilgileriList.get(ogrenimBilgileriList.size() - 1);
        assertThat(testOgrenimBilgileri.getOgrenimDurumu()).isEqualTo(UPDATED_OGRENIM_DURUMU);
        assertThat(testOgrenimBilgileri.getOkul()).isEqualTo(UPDATED_OKUL);
        assertThat(testOgrenimBilgileri.getMezuniyetYili()).isEqualTo(UPDATED_MEZUNIYET_YILI);
        assertThat(testOgrenimBilgileri.getOgrenimDili()).isEqualTo(UPDATED_OGRENIM_DILI);
        assertThat(testOgrenimBilgileri.getUlke()).isEqualTo(UPDATED_ULKE);
        assertThat(testOgrenimBilgileri.getSehir()).isEqualTo(UPDATED_SEHIR);
    }

    @Test
    @Transactional
    public void updateNonExistingOgrenimBilgileri() throws Exception {
        int databaseSizeBeforeUpdate = ogrenimBilgileriRepository.findAll().size();

        // Create the OgrenimBilgileri

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOgrenimBilgileriMockMvc.perform(put("/api/ogrenim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ogrenimBilgileri)))
            .andExpect(status().isBadRequest());

        // Validate the OgrenimBilgileri in the database
        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOgrenimBilgileri() throws Exception {
        // Initialize the database
        ogrenimBilgileriService.save(ogrenimBilgileri);

        int databaseSizeBeforeDelete = ogrenimBilgileriRepository.findAll().size();

        // Get the ogrenimBilgileri
        restOgrenimBilgileriMockMvc.perform(delete("/api/ogrenim-bilgileris/{id}", ogrenimBilgileri.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OgrenimBilgileri> ogrenimBilgileriList = ogrenimBilgileriRepository.findAll();
        assertThat(ogrenimBilgileriList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OgrenimBilgileri.class);
        OgrenimBilgileri ogrenimBilgileri1 = new OgrenimBilgileri();
        ogrenimBilgileri1.setId(1L);
        OgrenimBilgileri ogrenimBilgileri2 = new OgrenimBilgileri();
        ogrenimBilgileri2.setId(ogrenimBilgileri1.getId());
        assertThat(ogrenimBilgileri1).isEqualTo(ogrenimBilgileri2);
        ogrenimBilgileri2.setId(2L);
        assertThat(ogrenimBilgileri1).isNotEqualTo(ogrenimBilgileri2);
        ogrenimBilgileri1.setId(null);
        assertThat(ogrenimBilgileri1).isNotEqualTo(ogrenimBilgileri2);
    }
}
