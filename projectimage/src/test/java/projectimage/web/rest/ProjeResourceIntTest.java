package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.Proje;
import projectimage.repository.ProjeRepository;
import projectimage.service.ProjeService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.ProjeCiktisi;
/**
 * Test class for the ProjeResource REST controller.
 *
 * @see ProjeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class ProjeResourceIntTest {

    private static final String DEFAULT_PROJE_ADI = "AAAAAAAAAA";
    private static final String UPDATED_PROJE_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_GOREVI = "AAAAAAAAAA";
    private static final String UPDATED_GOREVI = "BBBBBBBBBB";

    private static final String DEFAULT_KONUSU = "AAAAAAAAAA";
    private static final String UPDATED_KONUSU = "BBBBBBBBBB";

    private static final String DEFAULT_AMACI = "AAAAAAAAAA";
    private static final String UPDATED_AMACI = "BBBBBBBBBB";

    private static final Integer DEFAULT_YIL = 1;
    private static final Integer UPDATED_YIL = 2;

    private static final String DEFAULT_YURUTEN_KURUM = "AAAAAAAAAA";
    private static final String UPDATED_YURUTEN_KURUM = "BBBBBBBBBB";

    private static final ProjeCiktisi DEFAULT_PROJE_CIKTISI = ProjeCiktisi.PROJE_OZETI;
    private static final ProjeCiktisi UPDATED_PROJE_CIKTISI = ProjeCiktisi.PROJE_RAPORU;

    private static final String DEFAULT_ANAHTAR_KELIMELER = "AAAAAAAAAA";
    private static final String UPDATED_ANAHTAR_KELIMELER = "BBBBBBBBBB";

    @Autowired
    private ProjeRepository projeRepository;

    @Autowired
    private ProjeService projeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProjeMockMvc;

    private Proje proje;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProjeResource projeResource = new ProjeResource(projeService);
        this.restProjeMockMvc = MockMvcBuilders.standaloneSetup(projeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Proje createEntity(EntityManager em) {
        Proje proje = new Proje()
            .projeAdi(DEFAULT_PROJE_ADI)
            .gorevi(DEFAULT_GOREVI)
            .konusu(DEFAULT_KONUSU)
            .amaci(DEFAULT_AMACI)
            .yil(DEFAULT_YIL)
            .yurutenKurum(DEFAULT_YURUTEN_KURUM)
            .projeCiktisi(DEFAULT_PROJE_CIKTISI)
            .anahtarKelimeler(DEFAULT_ANAHTAR_KELIMELER);
        return proje;
    }

    @Before
    public void initTest() {
        proje = createEntity(em);
    }

    @Test
    @Transactional
    public void createProje() throws Exception {
        int databaseSizeBeforeCreate = projeRepository.findAll().size();

        // Create the Proje
        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isCreated());

        // Validate the Proje in the database
        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeCreate + 1);
        Proje testProje = projeList.get(projeList.size() - 1);
        assertThat(testProje.getProjeAdi()).isEqualTo(DEFAULT_PROJE_ADI);
        assertThat(testProje.getGorevi()).isEqualTo(DEFAULT_GOREVI);
        assertThat(testProje.getKonusu()).isEqualTo(DEFAULT_KONUSU);
        assertThat(testProje.getAmaci()).isEqualTo(DEFAULT_AMACI);
        assertThat(testProje.getYil()).isEqualTo(DEFAULT_YIL);
        assertThat(testProje.getYurutenKurum()).isEqualTo(DEFAULT_YURUTEN_KURUM);
        assertThat(testProje.getProjeCiktisi()).isEqualTo(DEFAULT_PROJE_CIKTISI);
        assertThat(testProje.getAnahtarKelimeler()).isEqualTo(DEFAULT_ANAHTAR_KELIMELER);
    }

    @Test
    @Transactional
    public void createProjeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = projeRepository.findAll().size();

        // Create the Proje with an existing ID
        proje.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        // Validate the Proje in the database
        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkProjeAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = projeRepository.findAll().size();
        // set the field null
        proje.setProjeAdi(null);

        // Create the Proje, which fails.

        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGoreviIsRequired() throws Exception {
        int databaseSizeBeforeTest = projeRepository.findAll().size();
        // set the field null
        proje.setGorevi(null);

        // Create the Proje, which fails.

        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkKonusuIsRequired() throws Exception {
        int databaseSizeBeforeTest = projeRepository.findAll().size();
        // set the field null
        proje.setKonusu(null);

        // Create the Proje, which fails.

        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAmaciIsRequired() throws Exception {
        int databaseSizeBeforeTest = projeRepository.findAll().size();
        // set the field null
        proje.setAmaci(null);

        // Create the Proje, which fails.

        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYilIsRequired() throws Exception {
        int databaseSizeBeforeTest = projeRepository.findAll().size();
        // set the field null
        proje.setYil(null);

        // Create the Proje, which fails.

        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYurutenKurumIsRequired() throws Exception {
        int databaseSizeBeforeTest = projeRepository.findAll().size();
        // set the field null
        proje.setYurutenKurum(null);

        // Create the Proje, which fails.

        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProjeCiktisiIsRequired() throws Exception {
        int databaseSizeBeforeTest = projeRepository.findAll().size();
        // set the field null
        proje.setProjeCiktisi(null);

        // Create the Proje, which fails.

        restProjeMockMvc.perform(post("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProjes() throws Exception {
        // Initialize the database
        projeRepository.saveAndFlush(proje);

        // Get all the projeList
        restProjeMockMvc.perform(get("/api/projes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(proje.getId().intValue())))
            .andExpect(jsonPath("$.[*].projeAdi").value(hasItem(DEFAULT_PROJE_ADI.toString())))
            .andExpect(jsonPath("$.[*].gorevi").value(hasItem(DEFAULT_GOREVI.toString())))
            .andExpect(jsonPath("$.[*].konusu").value(hasItem(DEFAULT_KONUSU.toString())))
            .andExpect(jsonPath("$.[*].amaci").value(hasItem(DEFAULT_AMACI.toString())))
            .andExpect(jsonPath("$.[*].yil").value(hasItem(DEFAULT_YIL)))
            .andExpect(jsonPath("$.[*].yurutenKurum").value(hasItem(DEFAULT_YURUTEN_KURUM.toString())))
            .andExpect(jsonPath("$.[*].projeCiktisi").value(hasItem(DEFAULT_PROJE_CIKTISI.toString())))
            .andExpect(jsonPath("$.[*].anahtarKelimeler").value(hasItem(DEFAULT_ANAHTAR_KELIMELER.toString())));
    }
    
    @Test
    @Transactional
    public void getProje() throws Exception {
        // Initialize the database
        projeRepository.saveAndFlush(proje);

        // Get the proje
        restProjeMockMvc.perform(get("/api/projes/{id}", proje.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(proje.getId().intValue()))
            .andExpect(jsonPath("$.projeAdi").value(DEFAULT_PROJE_ADI.toString()))
            .andExpect(jsonPath("$.gorevi").value(DEFAULT_GOREVI.toString()))
            .andExpect(jsonPath("$.konusu").value(DEFAULT_KONUSU.toString()))
            .andExpect(jsonPath("$.amaci").value(DEFAULT_AMACI.toString()))
            .andExpect(jsonPath("$.yil").value(DEFAULT_YIL))
            .andExpect(jsonPath("$.yurutenKurum").value(DEFAULT_YURUTEN_KURUM.toString()))
            .andExpect(jsonPath("$.projeCiktisi").value(DEFAULT_PROJE_CIKTISI.toString()))
            .andExpect(jsonPath("$.anahtarKelimeler").value(DEFAULT_ANAHTAR_KELIMELER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProje() throws Exception {
        // Get the proje
        restProjeMockMvc.perform(get("/api/projes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProje() throws Exception {
        // Initialize the database
        projeService.save(proje);

        int databaseSizeBeforeUpdate = projeRepository.findAll().size();

        // Update the proje
        Proje updatedProje = projeRepository.findById(proje.getId()).get();
        // Disconnect from session so that the updates on updatedProje are not directly saved in db
        em.detach(updatedProje);
        updatedProje
            .projeAdi(UPDATED_PROJE_ADI)
            .gorevi(UPDATED_GOREVI)
            .konusu(UPDATED_KONUSU)
            .amaci(UPDATED_AMACI)
            .yil(UPDATED_YIL)
            .yurutenKurum(UPDATED_YURUTEN_KURUM)
            .projeCiktisi(UPDATED_PROJE_CIKTISI)
            .anahtarKelimeler(UPDATED_ANAHTAR_KELIMELER);

        restProjeMockMvc.perform(put("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProje)))
            .andExpect(status().isOk());

        // Validate the Proje in the database
        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeUpdate);
        Proje testProje = projeList.get(projeList.size() - 1);
        assertThat(testProje.getProjeAdi()).isEqualTo(UPDATED_PROJE_ADI);
        assertThat(testProje.getGorevi()).isEqualTo(UPDATED_GOREVI);
        assertThat(testProje.getKonusu()).isEqualTo(UPDATED_KONUSU);
        assertThat(testProje.getAmaci()).isEqualTo(UPDATED_AMACI);
        assertThat(testProje.getYil()).isEqualTo(UPDATED_YIL);
        assertThat(testProje.getYurutenKurum()).isEqualTo(UPDATED_YURUTEN_KURUM);
        assertThat(testProje.getProjeCiktisi()).isEqualTo(UPDATED_PROJE_CIKTISI);
        assertThat(testProje.getAnahtarKelimeler()).isEqualTo(UPDATED_ANAHTAR_KELIMELER);
    }

    @Test
    @Transactional
    public void updateNonExistingProje() throws Exception {
        int databaseSizeBeforeUpdate = projeRepository.findAll().size();

        // Create the Proje

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjeMockMvc.perform(put("/api/projes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proje)))
            .andExpect(status().isBadRequest());

        // Validate the Proje in the database
        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProje() throws Exception {
        // Initialize the database
        projeService.save(proje);

        int databaseSizeBeforeDelete = projeRepository.findAll().size();

        // Get the proje
        restProjeMockMvc.perform(delete("/api/projes/{id}", proje.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Proje> projeList = projeRepository.findAll();
        assertThat(projeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Proje.class);
        Proje proje1 = new Proje();
        proje1.setId(1L);
        Proje proje2 = new Proje();
        proje2.setId(proje1.getId());
        assertThat(proje1).isEqualTo(proje2);
        proje2.setId(2L);
        assertThat(proje1).isNotEqualTo(proje2);
        proje1.setId(null);
        assertThat(proje1).isNotEqualTo(proje2);
    }
}
