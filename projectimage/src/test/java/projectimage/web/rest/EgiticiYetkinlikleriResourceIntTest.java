package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.EgiticiYetkinlikleri;
import projectimage.repository.EgiticiYetkinlikleriRepository;
import projectimage.service.EgiticiYetkinlikleriService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.KurumTipi;
/**
 * Test class for the EgiticiYetkinlikleriResource REST controller.
 *
 * @see EgiticiYetkinlikleriResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class EgiticiYetkinlikleriResourceIntTest {

    private static final KurumTipi DEFAULT_KURUM_TIPI = KurumTipi.UNIVERSITE;
    private static final KurumTipi UPDATED_KURUM_TIPI = KurumTipi.DIGER;

    private static final String DEFAULT_UNIVERSITE_ADI = "AAAAAAAAAA";
    private static final String UPDATED_UNIVERSITE_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_KURUM_ADI = "AAAAAAAAAA";
    private static final String UPDATED_KURUM_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_FAKULTE = "AAAAAAAAAA";
    private static final String UPDATED_FAKULTE = "BBBBBBBBBB";

    private static final String DEFAULT_BOLUM = "AAAAAAAAAA";
    private static final String UPDATED_BOLUM = "BBBBBBBBBB";

    private static final String DEFAULT_KONU_DERS_ADI = "AAAAAAAAAA";
    private static final String UPDATED_KONU_DERS_ADI = "BBBBBBBBBB";

    @Autowired
    private EgiticiYetkinlikleriRepository egiticiYetkinlikleriRepository;

    @Autowired
    private EgiticiYetkinlikleriService egiticiYetkinlikleriService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEgiticiYetkinlikleriMockMvc;

    private EgiticiYetkinlikleri egiticiYetkinlikleri;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EgiticiYetkinlikleriResource egiticiYetkinlikleriResource = new EgiticiYetkinlikleriResource(egiticiYetkinlikleriService);
        this.restEgiticiYetkinlikleriMockMvc = MockMvcBuilders.standaloneSetup(egiticiYetkinlikleriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EgiticiYetkinlikleri createEntity(EntityManager em) {
        EgiticiYetkinlikleri egiticiYetkinlikleri = new EgiticiYetkinlikleri()
            .kurumTipi(DEFAULT_KURUM_TIPI)
            .universiteAdi(DEFAULT_UNIVERSITE_ADI)
            .kurumAdi(DEFAULT_KURUM_ADI)
            .fakulte(DEFAULT_FAKULTE)
            .bolum(DEFAULT_BOLUM)
            .konuDersAdi(DEFAULT_KONU_DERS_ADI);
        return egiticiYetkinlikleri;
    }

    @Before
    public void initTest() {
        egiticiYetkinlikleri = createEntity(em);
    }

    @Test
    @Transactional
    public void createEgiticiYetkinlikleri() throws Exception {
        int databaseSizeBeforeCreate = egiticiYetkinlikleriRepository.findAll().size();

        // Create the EgiticiYetkinlikleri
        restEgiticiYetkinlikleriMockMvc.perform(post("/api/egitici-yetkinlikleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(egiticiYetkinlikleri)))
            .andExpect(status().isCreated());

        // Validate the EgiticiYetkinlikleri in the database
        List<EgiticiYetkinlikleri> egiticiYetkinlikleriList = egiticiYetkinlikleriRepository.findAll();
        assertThat(egiticiYetkinlikleriList).hasSize(databaseSizeBeforeCreate + 1);
        EgiticiYetkinlikleri testEgiticiYetkinlikleri = egiticiYetkinlikleriList.get(egiticiYetkinlikleriList.size() - 1);
        assertThat(testEgiticiYetkinlikleri.getKurumTipi()).isEqualTo(DEFAULT_KURUM_TIPI);
        assertThat(testEgiticiYetkinlikleri.getUniversiteAdi()).isEqualTo(DEFAULT_UNIVERSITE_ADI);
        assertThat(testEgiticiYetkinlikleri.getKurumAdi()).isEqualTo(DEFAULT_KURUM_ADI);
        assertThat(testEgiticiYetkinlikleri.getFakulte()).isEqualTo(DEFAULT_FAKULTE);
        assertThat(testEgiticiYetkinlikleri.getBolum()).isEqualTo(DEFAULT_BOLUM);
        assertThat(testEgiticiYetkinlikleri.getKonuDersAdi()).isEqualTo(DEFAULT_KONU_DERS_ADI);
    }

    @Test
    @Transactional
    public void createEgiticiYetkinlikleriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = egiticiYetkinlikleriRepository.findAll().size();

        // Create the EgiticiYetkinlikleri with an existing ID
        egiticiYetkinlikleri.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEgiticiYetkinlikleriMockMvc.perform(post("/api/egitici-yetkinlikleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(egiticiYetkinlikleri)))
            .andExpect(status().isBadRequest());

        // Validate the EgiticiYetkinlikleri in the database
        List<EgiticiYetkinlikleri> egiticiYetkinlikleriList = egiticiYetkinlikleriRepository.findAll();
        assertThat(egiticiYetkinlikleriList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEgiticiYetkinlikleris() throws Exception {
        // Initialize the database
        egiticiYetkinlikleriRepository.saveAndFlush(egiticiYetkinlikleri);

        // Get all the egiticiYetkinlikleriList
        restEgiticiYetkinlikleriMockMvc.perform(get("/api/egitici-yetkinlikleris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(egiticiYetkinlikleri.getId().intValue())))
            .andExpect(jsonPath("$.[*].kurumTipi").value(hasItem(DEFAULT_KURUM_TIPI.toString())))
            .andExpect(jsonPath("$.[*].universiteAdi").value(hasItem(DEFAULT_UNIVERSITE_ADI.toString())))
            .andExpect(jsonPath("$.[*].kurumAdi").value(hasItem(DEFAULT_KURUM_ADI.toString())))
            .andExpect(jsonPath("$.[*].fakulte").value(hasItem(DEFAULT_FAKULTE.toString())))
            .andExpect(jsonPath("$.[*].bolum").value(hasItem(DEFAULT_BOLUM.toString())))
            .andExpect(jsonPath("$.[*].konuDersAdi").value(hasItem(DEFAULT_KONU_DERS_ADI.toString())));
    }
    
    @Test
    @Transactional
    public void getEgiticiYetkinlikleri() throws Exception {
        // Initialize the database
        egiticiYetkinlikleriRepository.saveAndFlush(egiticiYetkinlikleri);

        // Get the egiticiYetkinlikleri
        restEgiticiYetkinlikleriMockMvc.perform(get("/api/egitici-yetkinlikleris/{id}", egiticiYetkinlikleri.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(egiticiYetkinlikleri.getId().intValue()))
            .andExpect(jsonPath("$.kurumTipi").value(DEFAULT_KURUM_TIPI.toString()))
            .andExpect(jsonPath("$.universiteAdi").value(DEFAULT_UNIVERSITE_ADI.toString()))
            .andExpect(jsonPath("$.kurumAdi").value(DEFAULT_KURUM_ADI.toString()))
            .andExpect(jsonPath("$.fakulte").value(DEFAULT_FAKULTE.toString()))
            .andExpect(jsonPath("$.bolum").value(DEFAULT_BOLUM.toString()))
            .andExpect(jsonPath("$.konuDersAdi").value(DEFAULT_KONU_DERS_ADI.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEgiticiYetkinlikleri() throws Exception {
        // Get the egiticiYetkinlikleri
        restEgiticiYetkinlikleriMockMvc.perform(get("/api/egitici-yetkinlikleris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEgiticiYetkinlikleri() throws Exception {
        // Initialize the database
        egiticiYetkinlikleriService.save(egiticiYetkinlikleri);

        int databaseSizeBeforeUpdate = egiticiYetkinlikleriRepository.findAll().size();

        // Update the egiticiYetkinlikleri
        EgiticiYetkinlikleri updatedEgiticiYetkinlikleri = egiticiYetkinlikleriRepository.findById(egiticiYetkinlikleri.getId()).get();
        // Disconnect from session so that the updates on updatedEgiticiYetkinlikleri are not directly saved in db
        em.detach(updatedEgiticiYetkinlikleri);
        updatedEgiticiYetkinlikleri
            .kurumTipi(UPDATED_KURUM_TIPI)
            .universiteAdi(UPDATED_UNIVERSITE_ADI)
            .kurumAdi(UPDATED_KURUM_ADI)
            .fakulte(UPDATED_FAKULTE)
            .bolum(UPDATED_BOLUM)
            .konuDersAdi(UPDATED_KONU_DERS_ADI);

        restEgiticiYetkinlikleriMockMvc.perform(put("/api/egitici-yetkinlikleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEgiticiYetkinlikleri)))
            .andExpect(status().isOk());

        // Validate the EgiticiYetkinlikleri in the database
        List<EgiticiYetkinlikleri> egiticiYetkinlikleriList = egiticiYetkinlikleriRepository.findAll();
        assertThat(egiticiYetkinlikleriList).hasSize(databaseSizeBeforeUpdate);
        EgiticiYetkinlikleri testEgiticiYetkinlikleri = egiticiYetkinlikleriList.get(egiticiYetkinlikleriList.size() - 1);
        assertThat(testEgiticiYetkinlikleri.getKurumTipi()).isEqualTo(UPDATED_KURUM_TIPI);
        assertThat(testEgiticiYetkinlikleri.getUniversiteAdi()).isEqualTo(UPDATED_UNIVERSITE_ADI);
        assertThat(testEgiticiYetkinlikleri.getKurumAdi()).isEqualTo(UPDATED_KURUM_ADI);
        assertThat(testEgiticiYetkinlikleri.getFakulte()).isEqualTo(UPDATED_FAKULTE);
        assertThat(testEgiticiYetkinlikleri.getBolum()).isEqualTo(UPDATED_BOLUM);
        assertThat(testEgiticiYetkinlikleri.getKonuDersAdi()).isEqualTo(UPDATED_KONU_DERS_ADI);
    }

    @Test
    @Transactional
    public void updateNonExistingEgiticiYetkinlikleri() throws Exception {
        int databaseSizeBeforeUpdate = egiticiYetkinlikleriRepository.findAll().size();

        // Create the EgiticiYetkinlikleri

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEgiticiYetkinlikleriMockMvc.perform(put("/api/egitici-yetkinlikleris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(egiticiYetkinlikleri)))
            .andExpect(status().isBadRequest());

        // Validate the EgiticiYetkinlikleri in the database
        List<EgiticiYetkinlikleri> egiticiYetkinlikleriList = egiticiYetkinlikleriRepository.findAll();
        assertThat(egiticiYetkinlikleriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEgiticiYetkinlikleri() throws Exception {
        // Initialize the database
        egiticiYetkinlikleriService.save(egiticiYetkinlikleri);

        int databaseSizeBeforeDelete = egiticiYetkinlikleriRepository.findAll().size();

        // Get the egiticiYetkinlikleri
        restEgiticiYetkinlikleriMockMvc.perform(delete("/api/egitici-yetkinlikleris/{id}", egiticiYetkinlikleri.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EgiticiYetkinlikleri> egiticiYetkinlikleriList = egiticiYetkinlikleriRepository.findAll();
        assertThat(egiticiYetkinlikleriList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EgiticiYetkinlikleri.class);
        EgiticiYetkinlikleri egiticiYetkinlikleri1 = new EgiticiYetkinlikleri();
        egiticiYetkinlikleri1.setId(1L);
        EgiticiYetkinlikleri egiticiYetkinlikleri2 = new EgiticiYetkinlikleri();
        egiticiYetkinlikleri2.setId(egiticiYetkinlikleri1.getId());
        assertThat(egiticiYetkinlikleri1).isEqualTo(egiticiYetkinlikleri2);
        egiticiYetkinlikleri2.setId(2L);
        assertThat(egiticiYetkinlikleri1).isNotEqualTo(egiticiYetkinlikleri2);
        egiticiYetkinlikleri1.setId(null);
        assertThat(egiticiYetkinlikleri1).isNotEqualTo(egiticiYetkinlikleri2);
    }
}
