package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.YabanciDilBilgileri;
import projectimage.repository.YabanciDilBilgileriRepository;
import projectimage.service.YabanciDilBilgileriService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.Diller;
import projectimage.domain.enumeration.BelgeTuru;
/**
 * Test class for the YabanciDilBilgileriResource REST controller.
 *
 * @see YabanciDilBilgileriResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class YabanciDilBilgileriResourceIntTest {

    private static final Diller DEFAULT_DIL = Diller.TURKCE;
    private static final Diller UPDATED_DIL = Diller.INGILIZCE;

    private static final BelgeTuru DEFAULT_BELGE_TURU = BelgeTuru.CAE;
    private static final BelgeTuru UPDATED_BELGE_TURU = BelgeTuru.CPE;

    private static final LocalDate DEFAULT_BELGE_TARIHI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BELGE_TARIHI = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_PUAN = 1D;
    private static final Double UPDATED_PUAN = 2D;

    @Autowired
    private YabanciDilBilgileriRepository yabanciDilBilgileriRepository;

    @Autowired
    private YabanciDilBilgileriService yabanciDilBilgileriService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restYabanciDilBilgileriMockMvc;

    private YabanciDilBilgileri yabanciDilBilgileri;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final YabanciDilBilgileriResource yabanciDilBilgileriResource = new YabanciDilBilgileriResource(yabanciDilBilgileriService);
        this.restYabanciDilBilgileriMockMvc = MockMvcBuilders.standaloneSetup(yabanciDilBilgileriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static YabanciDilBilgileri createEntity(EntityManager em) {
        YabanciDilBilgileri yabanciDilBilgileri = new YabanciDilBilgileri()
            .dil(DEFAULT_DIL)
            .belgeTuru(DEFAULT_BELGE_TURU)
            .belgeTarihi(DEFAULT_BELGE_TARIHI)
            .puan(DEFAULT_PUAN);
        return yabanciDilBilgileri;
    }

    @Before
    public void initTest() {
        yabanciDilBilgileri = createEntity(em);
    }

    @Test
    @Transactional
    public void createYabanciDilBilgileri() throws Exception {
        int databaseSizeBeforeCreate = yabanciDilBilgileriRepository.findAll().size();

        // Create the YabanciDilBilgileri
        restYabanciDilBilgileriMockMvc.perform(post("/api/yabanci-dil-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yabanciDilBilgileri)))
            .andExpect(status().isCreated());

        // Validate the YabanciDilBilgileri in the database
        List<YabanciDilBilgileri> yabanciDilBilgileriList = yabanciDilBilgileriRepository.findAll();
        assertThat(yabanciDilBilgileriList).hasSize(databaseSizeBeforeCreate + 1);
        YabanciDilBilgileri testYabanciDilBilgileri = yabanciDilBilgileriList.get(yabanciDilBilgileriList.size() - 1);
        assertThat(testYabanciDilBilgileri.getDil()).isEqualTo(DEFAULT_DIL);
        assertThat(testYabanciDilBilgileri.getBelgeTuru()).isEqualTo(DEFAULT_BELGE_TURU);
        assertThat(testYabanciDilBilgileri.getBelgeTarihi()).isEqualTo(DEFAULT_BELGE_TARIHI);
        assertThat(testYabanciDilBilgileri.getPuan()).isEqualTo(DEFAULT_PUAN);
    }

    @Test
    @Transactional
    public void createYabanciDilBilgileriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = yabanciDilBilgileriRepository.findAll().size();

        // Create the YabanciDilBilgileri with an existing ID
        yabanciDilBilgileri.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restYabanciDilBilgileriMockMvc.perform(post("/api/yabanci-dil-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yabanciDilBilgileri)))
            .andExpect(status().isBadRequest());

        // Validate the YabanciDilBilgileri in the database
        List<YabanciDilBilgileri> yabanciDilBilgileriList = yabanciDilBilgileriRepository.findAll();
        assertThat(yabanciDilBilgileriList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkBelgeTarihiIsRequired() throws Exception {
        int databaseSizeBeforeTest = yabanciDilBilgileriRepository.findAll().size();
        // set the field null
        yabanciDilBilgileri.setBelgeTarihi(null);

        // Create the YabanciDilBilgileri, which fails.

        restYabanciDilBilgileriMockMvc.perform(post("/api/yabanci-dil-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yabanciDilBilgileri)))
            .andExpect(status().isBadRequest());

        List<YabanciDilBilgileri> yabanciDilBilgileriList = yabanciDilBilgileriRepository.findAll();
        assertThat(yabanciDilBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPuanIsRequired() throws Exception {
        int databaseSizeBeforeTest = yabanciDilBilgileriRepository.findAll().size();
        // set the field null
        yabanciDilBilgileri.setPuan(null);

        // Create the YabanciDilBilgileri, which fails.

        restYabanciDilBilgileriMockMvc.perform(post("/api/yabanci-dil-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yabanciDilBilgileri)))
            .andExpect(status().isBadRequest());

        List<YabanciDilBilgileri> yabanciDilBilgileriList = yabanciDilBilgileriRepository.findAll();
        assertThat(yabanciDilBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllYabanciDilBilgileris() throws Exception {
        // Initialize the database
        yabanciDilBilgileriRepository.saveAndFlush(yabanciDilBilgileri);

        // Get all the yabanciDilBilgileriList
        restYabanciDilBilgileriMockMvc.perform(get("/api/yabanci-dil-bilgileris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(yabanciDilBilgileri.getId().intValue())))
            .andExpect(jsonPath("$.[*].dil").value(hasItem(DEFAULT_DIL.toString())))
            .andExpect(jsonPath("$.[*].belgeTuru").value(hasItem(DEFAULT_BELGE_TURU.toString())))
            .andExpect(jsonPath("$.[*].belgeTarihi").value(hasItem(DEFAULT_BELGE_TARIHI.toString())))
            .andExpect(jsonPath("$.[*].puan").value(hasItem(DEFAULT_PUAN.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getYabanciDilBilgileri() throws Exception {
        // Initialize the database
        yabanciDilBilgileriRepository.saveAndFlush(yabanciDilBilgileri);

        // Get the yabanciDilBilgileri
        restYabanciDilBilgileriMockMvc.perform(get("/api/yabanci-dil-bilgileris/{id}", yabanciDilBilgileri.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(yabanciDilBilgileri.getId().intValue()))
            .andExpect(jsonPath("$.dil").value(DEFAULT_DIL.toString()))
            .andExpect(jsonPath("$.belgeTuru").value(DEFAULT_BELGE_TURU.toString()))
            .andExpect(jsonPath("$.belgeTarihi").value(DEFAULT_BELGE_TARIHI.toString()))
            .andExpect(jsonPath("$.puan").value(DEFAULT_PUAN.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingYabanciDilBilgileri() throws Exception {
        // Get the yabanciDilBilgileri
        restYabanciDilBilgileriMockMvc.perform(get("/api/yabanci-dil-bilgileris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateYabanciDilBilgileri() throws Exception {
        // Initialize the database
        yabanciDilBilgileriService.save(yabanciDilBilgileri);

        int databaseSizeBeforeUpdate = yabanciDilBilgileriRepository.findAll().size();

        // Update the yabanciDilBilgileri
        YabanciDilBilgileri updatedYabanciDilBilgileri = yabanciDilBilgileriRepository.findById(yabanciDilBilgileri.getId()).get();
        // Disconnect from session so that the updates on updatedYabanciDilBilgileri are not directly saved in db
        em.detach(updatedYabanciDilBilgileri);
        updatedYabanciDilBilgileri
            .dil(UPDATED_DIL)
            .belgeTuru(UPDATED_BELGE_TURU)
            .belgeTarihi(UPDATED_BELGE_TARIHI)
            .puan(UPDATED_PUAN);

        restYabanciDilBilgileriMockMvc.perform(put("/api/yabanci-dil-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedYabanciDilBilgileri)))
            .andExpect(status().isOk());

        // Validate the YabanciDilBilgileri in the database
        List<YabanciDilBilgileri> yabanciDilBilgileriList = yabanciDilBilgileriRepository.findAll();
        assertThat(yabanciDilBilgileriList).hasSize(databaseSizeBeforeUpdate);
        YabanciDilBilgileri testYabanciDilBilgileri = yabanciDilBilgileriList.get(yabanciDilBilgileriList.size() - 1);
        assertThat(testYabanciDilBilgileri.getDil()).isEqualTo(UPDATED_DIL);
        assertThat(testYabanciDilBilgileri.getBelgeTuru()).isEqualTo(UPDATED_BELGE_TURU);
        assertThat(testYabanciDilBilgileri.getBelgeTarihi()).isEqualTo(UPDATED_BELGE_TARIHI);
        assertThat(testYabanciDilBilgileri.getPuan()).isEqualTo(UPDATED_PUAN);
    }

    @Test
    @Transactional
    public void updateNonExistingYabanciDilBilgileri() throws Exception {
        int databaseSizeBeforeUpdate = yabanciDilBilgileriRepository.findAll().size();

        // Create the YabanciDilBilgileri

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restYabanciDilBilgileriMockMvc.perform(put("/api/yabanci-dil-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yabanciDilBilgileri)))
            .andExpect(status().isBadRequest());

        // Validate the YabanciDilBilgileri in the database
        List<YabanciDilBilgileri> yabanciDilBilgileriList = yabanciDilBilgileriRepository.findAll();
        assertThat(yabanciDilBilgileriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteYabanciDilBilgileri() throws Exception {
        // Initialize the database
        yabanciDilBilgileriService.save(yabanciDilBilgileri);

        int databaseSizeBeforeDelete = yabanciDilBilgileriRepository.findAll().size();

        // Get the yabanciDilBilgileri
        restYabanciDilBilgileriMockMvc.perform(delete("/api/yabanci-dil-bilgileris/{id}", yabanciDilBilgileri.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<YabanciDilBilgileri> yabanciDilBilgileriList = yabanciDilBilgileriRepository.findAll();
        assertThat(yabanciDilBilgileriList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(YabanciDilBilgileri.class);
        YabanciDilBilgileri yabanciDilBilgileri1 = new YabanciDilBilgileri();
        yabanciDilBilgileri1.setId(1L);
        YabanciDilBilgileri yabanciDilBilgileri2 = new YabanciDilBilgileri();
        yabanciDilBilgileri2.setId(yabanciDilBilgileri1.getId());
        assertThat(yabanciDilBilgileri1).isEqualTo(yabanciDilBilgileri2);
        yabanciDilBilgileri2.setId(2L);
        assertThat(yabanciDilBilgileri1).isNotEqualTo(yabanciDilBilgileri2);
        yabanciDilBilgileri1.setId(null);
        assertThat(yabanciDilBilgileri1).isNotEqualTo(yabanciDilBilgileri2);
    }
}
