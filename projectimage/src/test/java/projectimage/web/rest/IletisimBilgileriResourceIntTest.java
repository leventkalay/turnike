package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.IletisimBilgileri;
import projectimage.repository.IletisimBilgileriRepository;
import projectimage.service.IletisimBilgileriService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.Yakinlik;
/**
 * Test class for the IletisimBilgileriResource REST controller.
 *
 * @see IletisimBilgileriResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class IletisimBilgileriResourceIntTest {

    private static final String DEFAULT_ADRES = "AAAAAAAAAA";
    private static final String UPDATED_ADRES = "BBBBBBBBBB";

    private static final String DEFAULT_CEP_TELEFONU = "AAAAAAAAAA";
    private static final String UPDATED_CEP_TELEFONU = "BBBBBBBBBB";

    private static final String DEFAULT_CEP_TELEFONU_ALT = "AAAAAAAAAA";
    private static final String UPDATED_CEP_TELEFONU_ALT = "BBBBBBBBBB";

    private static final String DEFAULT_EV_TELEFONU = "AAAAAAAAAA";
    private static final String UPDATED_EV_TELEFONU = "BBBBBBBBBB";

    private static final String DEFAULT_DAHILI_TELEFON = "AAAAAAAAAA";
    private static final String UPDATED_DAHILI_TELEFON = "BBBBBBBBBB";

    private static final String DEFAULT_EPOSTA = "AAAAAAAAAA";
    private static final String UPDATED_EPOSTA = "BBBBBBBBBB";

    private static final String DEFAULT_ACIL_AD_SOYAD = "AAAAAAAAAA";
    private static final String UPDATED_ACIL_AD_SOYAD = "BBBBBBBBBB";

    private static final Yakinlik DEFAULT_YAKINLIK = Yakinlik.BABA;
    private static final Yakinlik UPDATED_YAKINLIK = Yakinlik.ANNE;

    private static final String DEFAULT_ACIL_TELEFON = "AAAAAAAAAA";
    private static final String UPDATED_ACIL_TELEFON = "BBBBBBBBBB";

    @Autowired
    private IletisimBilgileriRepository iletisimBilgileriRepository;

    @Autowired
    private IletisimBilgileriService iletisimBilgileriService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restIletisimBilgileriMockMvc;

    private IletisimBilgileri iletisimBilgileri;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IletisimBilgileriResource iletisimBilgileriResource = new IletisimBilgileriResource(iletisimBilgileriService);
        this.restIletisimBilgileriMockMvc = MockMvcBuilders.standaloneSetup(iletisimBilgileriResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IletisimBilgileri createEntity(EntityManager em) {
        IletisimBilgileri iletisimBilgileri = new IletisimBilgileri()
            .adres(DEFAULT_ADRES)
            .cepTelefonu(DEFAULT_CEP_TELEFONU)
            .cepTelefonuAlt(DEFAULT_CEP_TELEFONU_ALT)
            .evTelefonu(DEFAULT_EV_TELEFONU)
            .dahiliTelefon(DEFAULT_DAHILI_TELEFON)
            .eposta(DEFAULT_EPOSTA)
            .acilAdSoyad(DEFAULT_ACIL_AD_SOYAD)
            .yakinlik(DEFAULT_YAKINLIK)
            .acilTelefon(DEFAULT_ACIL_TELEFON);
        return iletisimBilgileri;
    }

    @Before
    public void initTest() {
        iletisimBilgileri = createEntity(em);
    }

    @Test
    @Transactional
    public void createIletisimBilgileri() throws Exception {
        int databaseSizeBeforeCreate = iletisimBilgileriRepository.findAll().size();

        // Create the IletisimBilgileri
        restIletisimBilgileriMockMvc.perform(post("/api/iletisim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(iletisimBilgileri)))
            .andExpect(status().isCreated());

        // Validate the IletisimBilgileri in the database
        List<IletisimBilgileri> iletisimBilgileriList = iletisimBilgileriRepository.findAll();
        assertThat(iletisimBilgileriList).hasSize(databaseSizeBeforeCreate + 1);
        IletisimBilgileri testIletisimBilgileri = iletisimBilgileriList.get(iletisimBilgileriList.size() - 1);
        assertThat(testIletisimBilgileri.getAdres()).isEqualTo(DEFAULT_ADRES);
        assertThat(testIletisimBilgileri.getCepTelefonu()).isEqualTo(DEFAULT_CEP_TELEFONU);
        assertThat(testIletisimBilgileri.getCepTelefonuAlt()).isEqualTo(DEFAULT_CEP_TELEFONU_ALT);
        assertThat(testIletisimBilgileri.getEvTelefonu()).isEqualTo(DEFAULT_EV_TELEFONU);
        assertThat(testIletisimBilgileri.getDahiliTelefon()).isEqualTo(DEFAULT_DAHILI_TELEFON);
        assertThat(testIletisimBilgileri.getEposta()).isEqualTo(DEFAULT_EPOSTA);
        assertThat(testIletisimBilgileri.getAcilAdSoyad()).isEqualTo(DEFAULT_ACIL_AD_SOYAD);
        assertThat(testIletisimBilgileri.getYakinlik()).isEqualTo(DEFAULT_YAKINLIK);
        assertThat(testIletisimBilgileri.getAcilTelefon()).isEqualTo(DEFAULT_ACIL_TELEFON);
    }

    @Test
    @Transactional
    public void createIletisimBilgileriWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = iletisimBilgileriRepository.findAll().size();

        // Create the IletisimBilgileri with an existing ID
        iletisimBilgileri.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIletisimBilgileriMockMvc.perform(post("/api/iletisim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(iletisimBilgileri)))
            .andExpect(status().isBadRequest());

        // Validate the IletisimBilgileri in the database
        List<IletisimBilgileri> iletisimBilgileriList = iletisimBilgileriRepository.findAll();
        assertThat(iletisimBilgileriList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEpostaIsRequired() throws Exception {
        int databaseSizeBeforeTest = iletisimBilgileriRepository.findAll().size();
        // set the field null
        iletisimBilgileri.setEposta(null);

        // Create the IletisimBilgileri, which fails.

        restIletisimBilgileriMockMvc.perform(post("/api/iletisim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(iletisimBilgileri)))
            .andExpect(status().isBadRequest());

        List<IletisimBilgileri> iletisimBilgileriList = iletisimBilgileriRepository.findAll();
        assertThat(iletisimBilgileriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIletisimBilgileris() throws Exception {
        // Initialize the database
        iletisimBilgileriRepository.saveAndFlush(iletisimBilgileri);

        // Get all the iletisimBilgileriList
        restIletisimBilgileriMockMvc.perform(get("/api/iletisim-bilgileris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(iletisimBilgileri.getId().intValue())))
            .andExpect(jsonPath("$.[*].adres").value(hasItem(DEFAULT_ADRES.toString())))
            .andExpect(jsonPath("$.[*].cepTelefonu").value(hasItem(DEFAULT_CEP_TELEFONU.toString())))
            .andExpect(jsonPath("$.[*].cepTelefonuAlt").value(hasItem(DEFAULT_CEP_TELEFONU_ALT.toString())))
            .andExpect(jsonPath("$.[*].evTelefonu").value(hasItem(DEFAULT_EV_TELEFONU.toString())))
            .andExpect(jsonPath("$.[*].dahiliTelefon").value(hasItem(DEFAULT_DAHILI_TELEFON.toString())))
            .andExpect(jsonPath("$.[*].eposta").value(hasItem(DEFAULT_EPOSTA.toString())))
            .andExpect(jsonPath("$.[*].acilAdSoyad").value(hasItem(DEFAULT_ACIL_AD_SOYAD.toString())))
            .andExpect(jsonPath("$.[*].yakinlik").value(hasItem(DEFAULT_YAKINLIK.toString())))
            .andExpect(jsonPath("$.[*].acilTelefon").value(hasItem(DEFAULT_ACIL_TELEFON.toString())));
    }
    
    @Test
    @Transactional
    public void getIletisimBilgileri() throws Exception {
        // Initialize the database
        iletisimBilgileriRepository.saveAndFlush(iletisimBilgileri);

        // Get the iletisimBilgileri
        restIletisimBilgileriMockMvc.perform(get("/api/iletisim-bilgileris/{id}", iletisimBilgileri.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(iletisimBilgileri.getId().intValue()))
            .andExpect(jsonPath("$.adres").value(DEFAULT_ADRES.toString()))
            .andExpect(jsonPath("$.cepTelefonu").value(DEFAULT_CEP_TELEFONU.toString()))
            .andExpect(jsonPath("$.cepTelefonuAlt").value(DEFAULT_CEP_TELEFONU_ALT.toString()))
            .andExpect(jsonPath("$.evTelefonu").value(DEFAULT_EV_TELEFONU.toString()))
            .andExpect(jsonPath("$.dahiliTelefon").value(DEFAULT_DAHILI_TELEFON.toString()))
            .andExpect(jsonPath("$.eposta").value(DEFAULT_EPOSTA.toString()))
            .andExpect(jsonPath("$.acilAdSoyad").value(DEFAULT_ACIL_AD_SOYAD.toString()))
            .andExpect(jsonPath("$.yakinlik").value(DEFAULT_YAKINLIK.toString()))
            .andExpect(jsonPath("$.acilTelefon").value(DEFAULT_ACIL_TELEFON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingIletisimBilgileri() throws Exception {
        // Get the iletisimBilgileri
        restIletisimBilgileriMockMvc.perform(get("/api/iletisim-bilgileris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIletisimBilgileri() throws Exception {
        // Initialize the database
        iletisimBilgileriService.save(iletisimBilgileri);

        int databaseSizeBeforeUpdate = iletisimBilgileriRepository.findAll().size();

        // Update the iletisimBilgileri
        IletisimBilgileri updatedIletisimBilgileri = iletisimBilgileriRepository.findById(iletisimBilgileri.getId()).get();
        // Disconnect from session so that the updates on updatedIletisimBilgileri are not directly saved in db
        em.detach(updatedIletisimBilgileri);
        updatedIletisimBilgileri
            .adres(UPDATED_ADRES)
            .cepTelefonu(UPDATED_CEP_TELEFONU)
            .cepTelefonuAlt(UPDATED_CEP_TELEFONU_ALT)
            .evTelefonu(UPDATED_EV_TELEFONU)
            .dahiliTelefon(UPDATED_DAHILI_TELEFON)
            .eposta(UPDATED_EPOSTA)
            .acilAdSoyad(UPDATED_ACIL_AD_SOYAD)
            .yakinlik(UPDATED_YAKINLIK)
            .acilTelefon(UPDATED_ACIL_TELEFON);

        restIletisimBilgileriMockMvc.perform(put("/api/iletisim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIletisimBilgileri)))
            .andExpect(status().isOk());

        // Validate the IletisimBilgileri in the database
        List<IletisimBilgileri> iletisimBilgileriList = iletisimBilgileriRepository.findAll();
        assertThat(iletisimBilgileriList).hasSize(databaseSizeBeforeUpdate);
        IletisimBilgileri testIletisimBilgileri = iletisimBilgileriList.get(iletisimBilgileriList.size() - 1);
        assertThat(testIletisimBilgileri.getAdres()).isEqualTo(UPDATED_ADRES);
        assertThat(testIletisimBilgileri.getCepTelefonu()).isEqualTo(UPDATED_CEP_TELEFONU);
        assertThat(testIletisimBilgileri.getCepTelefonuAlt()).isEqualTo(UPDATED_CEP_TELEFONU_ALT);
        assertThat(testIletisimBilgileri.getEvTelefonu()).isEqualTo(UPDATED_EV_TELEFONU);
        assertThat(testIletisimBilgileri.getDahiliTelefon()).isEqualTo(UPDATED_DAHILI_TELEFON);
        assertThat(testIletisimBilgileri.getEposta()).isEqualTo(UPDATED_EPOSTA);
        assertThat(testIletisimBilgileri.getAcilAdSoyad()).isEqualTo(UPDATED_ACIL_AD_SOYAD);
        assertThat(testIletisimBilgileri.getYakinlik()).isEqualTo(UPDATED_YAKINLIK);
        assertThat(testIletisimBilgileri.getAcilTelefon()).isEqualTo(UPDATED_ACIL_TELEFON);
    }

    @Test
    @Transactional
    public void updateNonExistingIletisimBilgileri() throws Exception {
        int databaseSizeBeforeUpdate = iletisimBilgileriRepository.findAll().size();

        // Create the IletisimBilgileri

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIletisimBilgileriMockMvc.perform(put("/api/iletisim-bilgileris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(iletisimBilgileri)))
            .andExpect(status().isBadRequest());

        // Validate the IletisimBilgileri in the database
        List<IletisimBilgileri> iletisimBilgileriList = iletisimBilgileriRepository.findAll();
        assertThat(iletisimBilgileriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIletisimBilgileri() throws Exception {
        // Initialize the database
        iletisimBilgileriService.save(iletisimBilgileri);

        int databaseSizeBeforeDelete = iletisimBilgileriRepository.findAll().size();

        // Get the iletisimBilgileri
        restIletisimBilgileriMockMvc.perform(delete("/api/iletisim-bilgileris/{id}", iletisimBilgileri.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<IletisimBilgileri> iletisimBilgileriList = iletisimBilgileriRepository.findAll();
        assertThat(iletisimBilgileriList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IletisimBilgileri.class);
        IletisimBilgileri iletisimBilgileri1 = new IletisimBilgileri();
        iletisimBilgileri1.setId(1L);
        IletisimBilgileri iletisimBilgileri2 = new IletisimBilgileri();
        iletisimBilgileri2.setId(iletisimBilgileri1.getId());
        assertThat(iletisimBilgileri1).isEqualTo(iletisimBilgileri2);
        iletisimBilgileri2.setId(2L);
        assertThat(iletisimBilgileri1).isNotEqualTo(iletisimBilgileri2);
        iletisimBilgileri1.setId(null);
        assertThat(iletisimBilgileri1).isNotEqualTo(iletisimBilgileri2);
    }
}
