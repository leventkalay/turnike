package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.UzmanlikAlanlari;
import projectimage.repository.UzmanlikAlanlariRepository;
import projectimage.service.UzmanlikAlanlariService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.DetayBilgiler;
/**
 * Test class for the UzmanlikAlanlariResource REST controller.
 *
 * @see UzmanlikAlanlariResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class UzmanlikAlanlariResourceIntTest {

    private static final DetayBilgiler DEFAULT_DETAY_BILGILER = DetayBilgiler.ALTYAPI_YATIRIMLARI;
    private static final DetayBilgiler UPDATED_DETAY_BILGILER = DetayBilgiler.ARGE;

    private static final String DEFAULT_ACIKLAMA = "AAAAAAAAAA";
    private static final String UPDATED_ACIKLAMA = "BBBBBBBBBB";

    @Autowired
    private UzmanlikAlanlariRepository uzmanlikAlanlariRepository;

    @Autowired
    private UzmanlikAlanlariService uzmanlikAlanlariService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUzmanlikAlanlariMockMvc;

    private UzmanlikAlanlari uzmanlikAlanlari;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UzmanlikAlanlariResource uzmanlikAlanlariResource = new UzmanlikAlanlariResource(uzmanlikAlanlariService);
        this.restUzmanlikAlanlariMockMvc = MockMvcBuilders.standaloneSetup(uzmanlikAlanlariResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UzmanlikAlanlari createEntity(EntityManager em) {
        UzmanlikAlanlari uzmanlikAlanlari = new UzmanlikAlanlari()
            .detayBilgiler(DEFAULT_DETAY_BILGILER)
            .aciklama(DEFAULT_ACIKLAMA);
        return uzmanlikAlanlari;
    }

    @Before
    public void initTest() {
        uzmanlikAlanlari = createEntity(em);
    }

    @Test
    @Transactional
    public void createUzmanlikAlanlari() throws Exception {
        int databaseSizeBeforeCreate = uzmanlikAlanlariRepository.findAll().size();

        // Create the UzmanlikAlanlari
        restUzmanlikAlanlariMockMvc.perform(post("/api/uzmanlik-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uzmanlikAlanlari)))
            .andExpect(status().isCreated());

        // Validate the UzmanlikAlanlari in the database
        List<UzmanlikAlanlari> uzmanlikAlanlariList = uzmanlikAlanlariRepository.findAll();
        assertThat(uzmanlikAlanlariList).hasSize(databaseSizeBeforeCreate + 1);
        UzmanlikAlanlari testUzmanlikAlanlari = uzmanlikAlanlariList.get(uzmanlikAlanlariList.size() - 1);
        assertThat(testUzmanlikAlanlari.getDetayBilgiler()).isEqualTo(DEFAULT_DETAY_BILGILER);
        assertThat(testUzmanlikAlanlari.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
    }

    @Test
    @Transactional
    public void createUzmanlikAlanlariWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = uzmanlikAlanlariRepository.findAll().size();

        // Create the UzmanlikAlanlari with an existing ID
        uzmanlikAlanlari.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUzmanlikAlanlariMockMvc.perform(post("/api/uzmanlik-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uzmanlikAlanlari)))
            .andExpect(status().isBadRequest());

        // Validate the UzmanlikAlanlari in the database
        List<UzmanlikAlanlari> uzmanlikAlanlariList = uzmanlikAlanlariRepository.findAll();
        assertThat(uzmanlikAlanlariList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDetayBilgilerIsRequired() throws Exception {
        int databaseSizeBeforeTest = uzmanlikAlanlariRepository.findAll().size();
        // set the field null
        uzmanlikAlanlari.setDetayBilgiler(null);

        // Create the UzmanlikAlanlari, which fails.

        restUzmanlikAlanlariMockMvc.perform(post("/api/uzmanlik-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uzmanlikAlanlari)))
            .andExpect(status().isBadRequest());

        List<UzmanlikAlanlari> uzmanlikAlanlariList = uzmanlikAlanlariRepository.findAll();
        assertThat(uzmanlikAlanlariList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAciklamaIsRequired() throws Exception {
        int databaseSizeBeforeTest = uzmanlikAlanlariRepository.findAll().size();
        // set the field null
        uzmanlikAlanlari.setAciklama(null);

        // Create the UzmanlikAlanlari, which fails.

        restUzmanlikAlanlariMockMvc.perform(post("/api/uzmanlik-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uzmanlikAlanlari)))
            .andExpect(status().isBadRequest());

        List<UzmanlikAlanlari> uzmanlikAlanlariList = uzmanlikAlanlariRepository.findAll();
        assertThat(uzmanlikAlanlariList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUzmanlikAlanlaris() throws Exception {
        // Initialize the database
        uzmanlikAlanlariRepository.saveAndFlush(uzmanlikAlanlari);

        // Get all the uzmanlikAlanlariList
        restUzmanlikAlanlariMockMvc.perform(get("/api/uzmanlik-alanlaris?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uzmanlikAlanlari.getId().intValue())))
            .andExpect(jsonPath("$.[*].detayBilgiler").value(hasItem(DEFAULT_DETAY_BILGILER.toString())))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA.toString())));
    }
    
    @Test
    @Transactional
    public void getUzmanlikAlanlari() throws Exception {
        // Initialize the database
        uzmanlikAlanlariRepository.saveAndFlush(uzmanlikAlanlari);

        // Get the uzmanlikAlanlari
        restUzmanlikAlanlariMockMvc.perform(get("/api/uzmanlik-alanlaris/{id}", uzmanlikAlanlari.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(uzmanlikAlanlari.getId().intValue()))
            .andExpect(jsonPath("$.detayBilgiler").value(DEFAULT_DETAY_BILGILER.toString()))
            .andExpect(jsonPath("$.aciklama").value(DEFAULT_ACIKLAMA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUzmanlikAlanlari() throws Exception {
        // Get the uzmanlikAlanlari
        restUzmanlikAlanlariMockMvc.perform(get("/api/uzmanlik-alanlaris/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUzmanlikAlanlari() throws Exception {
        // Initialize the database
        uzmanlikAlanlariService.save(uzmanlikAlanlari);

        int databaseSizeBeforeUpdate = uzmanlikAlanlariRepository.findAll().size();

        // Update the uzmanlikAlanlari
        UzmanlikAlanlari updatedUzmanlikAlanlari = uzmanlikAlanlariRepository.findById(uzmanlikAlanlari.getId()).get();
        // Disconnect from session so that the updates on updatedUzmanlikAlanlari are not directly saved in db
        em.detach(updatedUzmanlikAlanlari);
        updatedUzmanlikAlanlari
            .detayBilgiler(UPDATED_DETAY_BILGILER)
            .aciklama(UPDATED_ACIKLAMA);

        restUzmanlikAlanlariMockMvc.perform(put("/api/uzmanlik-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUzmanlikAlanlari)))
            .andExpect(status().isOk());

        // Validate the UzmanlikAlanlari in the database
        List<UzmanlikAlanlari> uzmanlikAlanlariList = uzmanlikAlanlariRepository.findAll();
        assertThat(uzmanlikAlanlariList).hasSize(databaseSizeBeforeUpdate);
        UzmanlikAlanlari testUzmanlikAlanlari = uzmanlikAlanlariList.get(uzmanlikAlanlariList.size() - 1);
        assertThat(testUzmanlikAlanlari.getDetayBilgiler()).isEqualTo(UPDATED_DETAY_BILGILER);
        assertThat(testUzmanlikAlanlari.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    public void updateNonExistingUzmanlikAlanlari() throws Exception {
        int databaseSizeBeforeUpdate = uzmanlikAlanlariRepository.findAll().size();

        // Create the UzmanlikAlanlari

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUzmanlikAlanlariMockMvc.perform(put("/api/uzmanlik-alanlaris")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uzmanlikAlanlari)))
            .andExpect(status().isBadRequest());

        // Validate the UzmanlikAlanlari in the database
        List<UzmanlikAlanlari> uzmanlikAlanlariList = uzmanlikAlanlariRepository.findAll();
        assertThat(uzmanlikAlanlariList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUzmanlikAlanlari() throws Exception {
        // Initialize the database
        uzmanlikAlanlariService.save(uzmanlikAlanlari);

        int databaseSizeBeforeDelete = uzmanlikAlanlariRepository.findAll().size();

        // Get the uzmanlikAlanlari
        restUzmanlikAlanlariMockMvc.perform(delete("/api/uzmanlik-alanlaris/{id}", uzmanlikAlanlari.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UzmanlikAlanlari> uzmanlikAlanlariList = uzmanlikAlanlariRepository.findAll();
        assertThat(uzmanlikAlanlariList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UzmanlikAlanlari.class);
        UzmanlikAlanlari uzmanlikAlanlari1 = new UzmanlikAlanlari();
        uzmanlikAlanlari1.setId(1L);
        UzmanlikAlanlari uzmanlikAlanlari2 = new UzmanlikAlanlari();
        uzmanlikAlanlari2.setId(uzmanlikAlanlari1.getId());
        assertThat(uzmanlikAlanlari1).isEqualTo(uzmanlikAlanlari2);
        uzmanlikAlanlari2.setId(2L);
        assertThat(uzmanlikAlanlari1).isNotEqualTo(uzmanlikAlanlari2);
        uzmanlikAlanlari1.setId(null);
        assertThat(uzmanlikAlanlari1).isNotEqualTo(uzmanlikAlanlari2);
    }
}
