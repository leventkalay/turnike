package projectimage.web.rest;

import projectimage.ProjectimageApp;

import projectimage.domain.Yayinlar;
import projectimage.repository.YayinlarRepository;
import projectimage.service.YayinlarService;
import projectimage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static projectimage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import projectimage.domain.enumeration.YayinTuru;
/**
 * Test class for the YayinlarResource REST controller.
 *
 * @see YayinlarResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectimageApp.class)
public class YayinlarResourceIntTest {

    private static final YayinTuru DEFAULT_YAYIN_TURU = YayinTuru.OZEL;
    private static final YayinTuru UPDATED_YAYIN_TURU = YayinTuru.KAMU;

    private static final String DEFAULT_YAZAR_ADI = "AAAAAAAAAA";
    private static final String UPDATED_YAZAR_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_ISBN = "AAAAAAAAAA";
    private static final String UPDATED_ISBN = "BBBBBBBBBB";

    private static final Integer DEFAULT_SAYFA_SAYISI = 1;
    private static final Integer UPDATED_SAYFA_SAYISI = 2;

    private static final String DEFAULT_YAYIN_BILGISI = "AAAAAAAAAA";
    private static final String UPDATED_YAYIN_BILGISI = "BBBBBBBBBB";

    private static final String DEFAULT_YAYIN_YIL = "AAAAAAAAAA";
    private static final String UPDATED_YAYIN_YIL = "BBBBBBBBBB";

    private static final String DEFAULT_ELEKTRONIK_ERISIM = "AAAAAAAAAA";
    private static final String UPDATED_ELEKTRONIK_ERISIM = "BBBBBBBBBB";

    private static final String DEFAULT_ANAHTAR_KELIMELER = "AAAAAAAAAA";
    private static final String UPDATED_ANAHTAR_KELIMELER = "BBBBBBBBBB";

    @Autowired
    private YayinlarRepository yayinlarRepository;

    @Autowired
    private YayinlarService yayinlarService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restYayinlarMockMvc;

    private Yayinlar yayinlar;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final YayinlarResource yayinlarResource = new YayinlarResource(yayinlarService);
        this.restYayinlarMockMvc = MockMvcBuilders.standaloneSetup(yayinlarResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Yayinlar createEntity(EntityManager em) {
        Yayinlar yayinlar = new Yayinlar()
            .yayinTuru(DEFAULT_YAYIN_TURU)
            .yazarAdi(DEFAULT_YAZAR_ADI)
            .isbn(DEFAULT_ISBN)
            .sayfaSayisi(DEFAULT_SAYFA_SAYISI)
            .yayinBilgisi(DEFAULT_YAYIN_BILGISI)
            .yayinYil(DEFAULT_YAYIN_YIL)
            .elektronikErisim(DEFAULT_ELEKTRONIK_ERISIM)
            .anahtarKelimeler(DEFAULT_ANAHTAR_KELIMELER);
        return yayinlar;
    }

    @Before
    public void initTest() {
        yayinlar = createEntity(em);
    }

    @Test
    @Transactional
    public void createYayinlar() throws Exception {
        int databaseSizeBeforeCreate = yayinlarRepository.findAll().size();

        // Create the Yayinlar
        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isCreated());

        // Validate the Yayinlar in the database
        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeCreate + 1);
        Yayinlar testYayinlar = yayinlarList.get(yayinlarList.size() - 1);
        assertThat(testYayinlar.getYayinTuru()).isEqualTo(DEFAULT_YAYIN_TURU);
        assertThat(testYayinlar.getYazarAdi()).isEqualTo(DEFAULT_YAZAR_ADI);
        assertThat(testYayinlar.getIsbn()).isEqualTo(DEFAULT_ISBN);
        assertThat(testYayinlar.getSayfaSayisi()).isEqualTo(DEFAULT_SAYFA_SAYISI);
        assertThat(testYayinlar.getYayinBilgisi()).isEqualTo(DEFAULT_YAYIN_BILGISI);
        assertThat(testYayinlar.getYayinYil()).isEqualTo(DEFAULT_YAYIN_YIL);
        assertThat(testYayinlar.getElektronikErisim()).isEqualTo(DEFAULT_ELEKTRONIK_ERISIM);
        assertThat(testYayinlar.getAnahtarKelimeler()).isEqualTo(DEFAULT_ANAHTAR_KELIMELER);
    }

    @Test
    @Transactional
    public void createYayinlarWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = yayinlarRepository.findAll().size();

        // Create the Yayinlar with an existing ID
        yayinlar.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        // Validate the Yayinlar in the database
        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkYayinTuruIsRequired() throws Exception {
        int databaseSizeBeforeTest = yayinlarRepository.findAll().size();
        // set the field null
        yayinlar.setYayinTuru(null);

        // Create the Yayinlar, which fails.

        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYazarAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = yayinlarRepository.findAll().size();
        // set the field null
        yayinlar.setYazarAdi(null);

        // Create the Yayinlar, which fails.

        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSayfaSayisiIsRequired() throws Exception {
        int databaseSizeBeforeTest = yayinlarRepository.findAll().size();
        // set the field null
        yayinlar.setSayfaSayisi(null);

        // Create the Yayinlar, which fails.

        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYayinBilgisiIsRequired() throws Exception {
        int databaseSizeBeforeTest = yayinlarRepository.findAll().size();
        // set the field null
        yayinlar.setYayinBilgisi(null);

        // Create the Yayinlar, which fails.

        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYayinYilIsRequired() throws Exception {
        int databaseSizeBeforeTest = yayinlarRepository.findAll().size();
        // set the field null
        yayinlar.setYayinYil(null);

        // Create the Yayinlar, which fails.

        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkElektronikErisimIsRequired() throws Exception {
        int databaseSizeBeforeTest = yayinlarRepository.findAll().size();
        // set the field null
        yayinlar.setElektronikErisim(null);

        // Create the Yayinlar, which fails.

        restYayinlarMockMvc.perform(post("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllYayinlars() throws Exception {
        // Initialize the database
        yayinlarRepository.saveAndFlush(yayinlar);

        // Get all the yayinlarList
        restYayinlarMockMvc.perform(get("/api/yayinlars?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(yayinlar.getId().intValue())))
            .andExpect(jsonPath("$.[*].yayinTuru").value(hasItem(DEFAULT_YAYIN_TURU.toString())))
            .andExpect(jsonPath("$.[*].yazarAdi").value(hasItem(DEFAULT_YAZAR_ADI.toString())))
            .andExpect(jsonPath("$.[*].isbn").value(hasItem(DEFAULT_ISBN.toString())))
            .andExpect(jsonPath("$.[*].sayfaSayisi").value(hasItem(DEFAULT_SAYFA_SAYISI)))
            .andExpect(jsonPath("$.[*].yayinBilgisi").value(hasItem(DEFAULT_YAYIN_BILGISI.toString())))
            .andExpect(jsonPath("$.[*].yayinYil").value(hasItem(DEFAULT_YAYIN_YIL.toString())))
            .andExpect(jsonPath("$.[*].elektronikErisim").value(hasItem(DEFAULT_ELEKTRONIK_ERISIM.toString())))
            .andExpect(jsonPath("$.[*].anahtarKelimeler").value(hasItem(DEFAULT_ANAHTAR_KELIMELER.toString())));
    }
    
    @Test
    @Transactional
    public void getYayinlar() throws Exception {
        // Initialize the database
        yayinlarRepository.saveAndFlush(yayinlar);

        // Get the yayinlar
        restYayinlarMockMvc.perform(get("/api/yayinlars/{id}", yayinlar.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(yayinlar.getId().intValue()))
            .andExpect(jsonPath("$.yayinTuru").value(DEFAULT_YAYIN_TURU.toString()))
            .andExpect(jsonPath("$.yazarAdi").value(DEFAULT_YAZAR_ADI.toString()))
            .andExpect(jsonPath("$.isbn").value(DEFAULT_ISBN.toString()))
            .andExpect(jsonPath("$.sayfaSayisi").value(DEFAULT_SAYFA_SAYISI))
            .andExpect(jsonPath("$.yayinBilgisi").value(DEFAULT_YAYIN_BILGISI.toString()))
            .andExpect(jsonPath("$.yayinYil").value(DEFAULT_YAYIN_YIL.toString()))
            .andExpect(jsonPath("$.elektronikErisim").value(DEFAULT_ELEKTRONIK_ERISIM.toString()))
            .andExpect(jsonPath("$.anahtarKelimeler").value(DEFAULT_ANAHTAR_KELIMELER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingYayinlar() throws Exception {
        // Get the yayinlar
        restYayinlarMockMvc.perform(get("/api/yayinlars/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateYayinlar() throws Exception {
        // Initialize the database
        yayinlarService.save(yayinlar);

        int databaseSizeBeforeUpdate = yayinlarRepository.findAll().size();

        // Update the yayinlar
        Yayinlar updatedYayinlar = yayinlarRepository.findById(yayinlar.getId()).get();
        // Disconnect from session so that the updates on updatedYayinlar are not directly saved in db
        em.detach(updatedYayinlar);
        updatedYayinlar
            .yayinTuru(UPDATED_YAYIN_TURU)
            .yazarAdi(UPDATED_YAZAR_ADI)
            .isbn(UPDATED_ISBN)
            .sayfaSayisi(UPDATED_SAYFA_SAYISI)
            .yayinBilgisi(UPDATED_YAYIN_BILGISI)
            .yayinYil(UPDATED_YAYIN_YIL)
            .elektronikErisim(UPDATED_ELEKTRONIK_ERISIM)
            .anahtarKelimeler(UPDATED_ANAHTAR_KELIMELER);

        restYayinlarMockMvc.perform(put("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedYayinlar)))
            .andExpect(status().isOk());

        // Validate the Yayinlar in the database
        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeUpdate);
        Yayinlar testYayinlar = yayinlarList.get(yayinlarList.size() - 1);
        assertThat(testYayinlar.getYayinTuru()).isEqualTo(UPDATED_YAYIN_TURU);
        assertThat(testYayinlar.getYazarAdi()).isEqualTo(UPDATED_YAZAR_ADI);
        assertThat(testYayinlar.getIsbn()).isEqualTo(UPDATED_ISBN);
        assertThat(testYayinlar.getSayfaSayisi()).isEqualTo(UPDATED_SAYFA_SAYISI);
        assertThat(testYayinlar.getYayinBilgisi()).isEqualTo(UPDATED_YAYIN_BILGISI);
        assertThat(testYayinlar.getYayinYil()).isEqualTo(UPDATED_YAYIN_YIL);
        assertThat(testYayinlar.getElektronikErisim()).isEqualTo(UPDATED_ELEKTRONIK_ERISIM);
        assertThat(testYayinlar.getAnahtarKelimeler()).isEqualTo(UPDATED_ANAHTAR_KELIMELER);
    }

    @Test
    @Transactional
    public void updateNonExistingYayinlar() throws Exception {
        int databaseSizeBeforeUpdate = yayinlarRepository.findAll().size();

        // Create the Yayinlar

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restYayinlarMockMvc.perform(put("/api/yayinlars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(yayinlar)))
            .andExpect(status().isBadRequest());

        // Validate the Yayinlar in the database
        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteYayinlar() throws Exception {
        // Initialize the database
        yayinlarService.save(yayinlar);

        int databaseSizeBeforeDelete = yayinlarRepository.findAll().size();

        // Get the yayinlar
        restYayinlarMockMvc.perform(delete("/api/yayinlars/{id}", yayinlar.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Yayinlar> yayinlarList = yayinlarRepository.findAll();
        assertThat(yayinlarList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Yayinlar.class);
        Yayinlar yayinlar1 = new Yayinlar();
        yayinlar1.setId(1L);
        Yayinlar yayinlar2 = new Yayinlar();
        yayinlar2.setId(yayinlar1.getId());
        assertThat(yayinlar1).isEqualTo(yayinlar2);
        yayinlar2.setId(2L);
        assertThat(yayinlar1).isNotEqualTo(yayinlar2);
        yayinlar1.setId(null);
        assertThat(yayinlar1).isNotEqualTo(yayinlar2);
    }
}
