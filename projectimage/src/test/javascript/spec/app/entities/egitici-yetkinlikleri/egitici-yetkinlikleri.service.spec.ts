/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { EgiticiYetkinlikleriService } from 'app/entities/egitici-yetkinlikleri/egitici-yetkinlikleri.service';
import { IEgiticiYetkinlikleri, EgiticiYetkinlikleri, KurumTipi } from 'app/shared/model/egitici-yetkinlikleri.model';

describe('Service Tests', () => {
    describe('EgiticiYetkinlikleri Service', () => {
        let injector: TestBed;
        let service: EgiticiYetkinlikleriService;
        let httpMock: HttpTestingController;
        let elemDefault: IEgiticiYetkinlikleri;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(EgiticiYetkinlikleriService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new EgiticiYetkinlikleri(0, KurumTipi.UNIVERSITE, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a EgiticiYetkinlikleri', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new EgiticiYetkinlikleri(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a EgiticiYetkinlikleri', async () => {
                const returnedFromService = Object.assign(
                    {
                        kurumTipi: 'BBBBBB',
                        universiteAdi: 'BBBBBB',
                        kurumAdi: 'BBBBBB',
                        fakulte: 'BBBBBB',
                        bolum: 'BBBBBB',
                        konuDersAdi: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of EgiticiYetkinlikleri', async () => {
                const returnedFromService = Object.assign(
                    {
                        kurumTipi: 'BBBBBB',
                        universiteAdi: 'BBBBBB',
                        kurumAdi: 'BBBBBB',
                        fakulte: 'BBBBBB',
                        bolum: 'BBBBBB',
                        konuDersAdi: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a EgiticiYetkinlikleri', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
