/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { EgiticiYetkinlikleriUpdateComponent } from 'app/entities/egitici-yetkinlikleri/egitici-yetkinlikleri-update.component';
import { EgiticiYetkinlikleriService } from 'app/entities/egitici-yetkinlikleri/egitici-yetkinlikleri.service';
import { EgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';

describe('Component Tests', () => {
    describe('EgiticiYetkinlikleri Management Update Component', () => {
        let comp: EgiticiYetkinlikleriUpdateComponent;
        let fixture: ComponentFixture<EgiticiYetkinlikleriUpdateComponent>;
        let service: EgiticiYetkinlikleriService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [EgiticiYetkinlikleriUpdateComponent]
            })
                .overrideTemplate(EgiticiYetkinlikleriUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EgiticiYetkinlikleriUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EgiticiYetkinlikleriService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new EgiticiYetkinlikleri(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.egiticiYetkinlikleri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new EgiticiYetkinlikleri();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.egiticiYetkinlikleri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
