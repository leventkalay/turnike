/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { EgiticiYetkinlikleriDetailComponent } from 'app/entities/egitici-yetkinlikleri/egitici-yetkinlikleri-detail.component';
import { EgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';

describe('Component Tests', () => {
    describe('EgiticiYetkinlikleri Management Detail Component', () => {
        let comp: EgiticiYetkinlikleriDetailComponent;
        let fixture: ComponentFixture<EgiticiYetkinlikleriDetailComponent>;
        const route = ({ data: of({ egiticiYetkinlikleri: new EgiticiYetkinlikleri(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [EgiticiYetkinlikleriDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(EgiticiYetkinlikleriDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EgiticiYetkinlikleriDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.egiticiYetkinlikleri).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
