/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { IstectrubeleriUpdateComponent } from 'app/entities/istectrubeleri/istectrubeleri-update.component';
import { IstectrubeleriService } from 'app/entities/istectrubeleri/istectrubeleri.service';
import { Istectrubeleri } from 'app/shared/model/istectrubeleri.model';

describe('Component Tests', () => {
    describe('Istectrubeleri Management Update Component', () => {
        let comp: IstectrubeleriUpdateComponent;
        let fixture: ComponentFixture<IstectrubeleriUpdateComponent>;
        let service: IstectrubeleriService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [IstectrubeleriUpdateComponent]
            })
                .overrideTemplate(IstectrubeleriUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(IstectrubeleriUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IstectrubeleriService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Istectrubeleri(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.istectrubeleri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Istectrubeleri();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.istectrubeleri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
