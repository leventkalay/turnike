/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { IstectrubeleriDetailComponent } from 'app/entities/istectrubeleri/istectrubeleri-detail.component';
import { Istectrubeleri } from 'app/shared/model/istectrubeleri.model';

describe('Component Tests', () => {
    describe('Istectrubeleri Management Detail Component', () => {
        let comp: IstectrubeleriDetailComponent;
        let fixture: ComponentFixture<IstectrubeleriDetailComponent>;
        const route = ({ data: of({ istectrubeleri: new Istectrubeleri(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [IstectrubeleriDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(IstectrubeleriDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(IstectrubeleriDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.istectrubeleri).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
