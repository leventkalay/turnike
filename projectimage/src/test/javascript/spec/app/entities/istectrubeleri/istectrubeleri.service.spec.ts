/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { IstectrubeleriService } from 'app/entities/istectrubeleri/istectrubeleri.service';
import { IIstectrubeleri, Istectrubeleri, CalismaAlani, CalismaSekli, Aylar, Ulke, Sehir } from 'app/shared/model/istectrubeleri.model';

describe('Service Tests', () => {
    describe('Istectrubeleri Service', () => {
        let injector: TestBed;
        let service: IstectrubeleriService;
        let httpMock: HttpTestingController;
        let elemDefault: IIstectrubeleri;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(IstectrubeleriService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new Istectrubeleri(
                0,
                CalismaAlani.KAMU,
                CalismaSekli.TAM_ZAMANLI,
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                Aylar.OCAK,
                0,
                Aylar.OCAK,
                0,
                'AAAAAAA',
                Ulke.TURKIYE,
                Sehir.ADANA,
                'AAAAAAA'
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Istectrubeleri', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new Istectrubeleri(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Istectrubeleri', async () => {
                const returnedFromService = Object.assign(
                    {
                        calismaAlani: 'BBBBBB',
                        calismaSekli: 'BBBBBB',
                        kurumAdi: 'BBBBBB',
                        gorevBirimi: 'BBBBBB',
                        unvan: 'BBBBBB',
                        baslangicAyi: 'BBBBBB',
                        baslangicYili: 1,
                        bitisAyi: 'BBBBBB',
                        bitisYili: 1,
                        gorevTanimi: 'BBBBBB',
                        ulke: 'BBBBBB',
                        sehir: 'BBBBBB',
                        yararliBilgier: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Istectrubeleri', async () => {
                const returnedFromService = Object.assign(
                    {
                        calismaAlani: 'BBBBBB',
                        calismaSekli: 'BBBBBB',
                        kurumAdi: 'BBBBBB',
                        gorevBirimi: 'BBBBBB',
                        unvan: 'BBBBBB',
                        baslangicAyi: 'BBBBBB',
                        baslangicYili: 1,
                        bitisAyi: 'BBBBBB',
                        bitisYili: 1,
                        gorevTanimi: 'BBBBBB',
                        ulke: 'BBBBBB',
                        sehir: 'BBBBBB',
                        yararliBilgier: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Istectrubeleri', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
