/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { UzmanlikAlanlariService } from 'app/entities/uzmanlik-alanlari/uzmanlik-alanlari.service';
import { IUzmanlikAlanlari, UzmanlikAlanlari, DetayBilgiler } from 'app/shared/model/uzmanlik-alanlari.model';

describe('Service Tests', () => {
    describe('UzmanlikAlanlari Service', () => {
        let injector: TestBed;
        let service: UzmanlikAlanlariService;
        let httpMock: HttpTestingController;
        let elemDefault: IUzmanlikAlanlari;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(UzmanlikAlanlariService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new UzmanlikAlanlari(0, DetayBilgiler.ALTYAPI_YATIRIMLARI, 'AAAAAAA');
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a UzmanlikAlanlari', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new UzmanlikAlanlari(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a UzmanlikAlanlari', async () => {
                const returnedFromService = Object.assign(
                    {
                        detayBilgiler: 'BBBBBB',
                        aciklama: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of UzmanlikAlanlari', async () => {
                const returnedFromService = Object.assign(
                    {
                        detayBilgiler: 'BBBBBB',
                        aciklama: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a UzmanlikAlanlari', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
