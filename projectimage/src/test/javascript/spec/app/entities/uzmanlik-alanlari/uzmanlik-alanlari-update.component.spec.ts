/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { UzmanlikAlanlariUpdateComponent } from 'app/entities/uzmanlik-alanlari/uzmanlik-alanlari-update.component';
import { UzmanlikAlanlariService } from 'app/entities/uzmanlik-alanlari/uzmanlik-alanlari.service';
import { UzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';

describe('Component Tests', () => {
    describe('UzmanlikAlanlari Management Update Component', () => {
        let comp: UzmanlikAlanlariUpdateComponent;
        let fixture: ComponentFixture<UzmanlikAlanlariUpdateComponent>;
        let service: UzmanlikAlanlariService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [UzmanlikAlanlariUpdateComponent]
            })
                .overrideTemplate(UzmanlikAlanlariUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(UzmanlikAlanlariUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UzmanlikAlanlariService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new UzmanlikAlanlari(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.uzmanlikAlanlari = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new UzmanlikAlanlari();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.uzmanlikAlanlari = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
