/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { UzmanlikAlanlariDetailComponent } from 'app/entities/uzmanlik-alanlari/uzmanlik-alanlari-detail.component';
import { UzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';

describe('Component Tests', () => {
    describe('UzmanlikAlanlari Management Detail Component', () => {
        let comp: UzmanlikAlanlariDetailComponent;
        let fixture: ComponentFixture<UzmanlikAlanlariDetailComponent>;
        const route = ({ data: of({ uzmanlikAlanlari: new UzmanlikAlanlari(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [UzmanlikAlanlariDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(UzmanlikAlanlariDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(UzmanlikAlanlariDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.uzmanlikAlanlari).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
