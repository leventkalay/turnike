/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { YayinlarDetailComponent } from 'app/entities/yayinlar/yayinlar-detail.component';
import { Yayinlar } from 'app/shared/model/yayinlar.model';

describe('Component Tests', () => {
    describe('Yayinlar Management Detail Component', () => {
        let comp: YayinlarDetailComponent;
        let fixture: ComponentFixture<YayinlarDetailComponent>;
        const route = ({ data: of({ yayinlar: new Yayinlar(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [YayinlarDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(YayinlarDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(YayinlarDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.yayinlar).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
