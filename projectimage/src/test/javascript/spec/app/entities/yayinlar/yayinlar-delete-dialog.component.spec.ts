/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectimageTestModule } from '../../../test.module';
import { YayinlarDeleteDialogComponent } from 'app/entities/yayinlar/yayinlar-delete-dialog.component';
import { YayinlarService } from 'app/entities/yayinlar/yayinlar.service';

describe('Component Tests', () => {
    describe('Yayinlar Management Delete Component', () => {
        let comp: YayinlarDeleteDialogComponent;
        let fixture: ComponentFixture<YayinlarDeleteDialogComponent>;
        let service: YayinlarService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [YayinlarDeleteDialogComponent]
            })
                .overrideTemplate(YayinlarDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(YayinlarDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(YayinlarService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
