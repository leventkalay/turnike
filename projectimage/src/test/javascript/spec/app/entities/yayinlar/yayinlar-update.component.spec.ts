/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { YayinlarUpdateComponent } from 'app/entities/yayinlar/yayinlar-update.component';
import { YayinlarService } from 'app/entities/yayinlar/yayinlar.service';
import { Yayinlar } from 'app/shared/model/yayinlar.model';

describe('Component Tests', () => {
    describe('Yayinlar Management Update Component', () => {
        let comp: YayinlarUpdateComponent;
        let fixture: ComponentFixture<YayinlarUpdateComponent>;
        let service: YayinlarService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [YayinlarUpdateComponent]
            })
                .overrideTemplate(YayinlarUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(YayinlarUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(YayinlarService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Yayinlar(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.yayinlar = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Yayinlar();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.yayinlar = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
