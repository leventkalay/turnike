/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { OgrenimBilgileriDetailComponent } from 'app/entities/ogrenim-bilgileri/ogrenim-bilgileri-detail.component';
import { OgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';

describe('Component Tests', () => {
    describe('OgrenimBilgileri Management Detail Component', () => {
        let comp: OgrenimBilgileriDetailComponent;
        let fixture: ComponentFixture<OgrenimBilgileriDetailComponent>;
        const route = ({ data: of({ ogrenimBilgileri: new OgrenimBilgileri(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [OgrenimBilgileriDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(OgrenimBilgileriDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OgrenimBilgileriDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.ogrenimBilgileri).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
