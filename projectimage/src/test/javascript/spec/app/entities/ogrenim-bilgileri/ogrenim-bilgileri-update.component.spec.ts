/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { OgrenimBilgileriUpdateComponent } from 'app/entities/ogrenim-bilgileri/ogrenim-bilgileri-update.component';
import { OgrenimBilgileriService } from 'app/entities/ogrenim-bilgileri/ogrenim-bilgileri.service';
import { OgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';

describe('Component Tests', () => {
    describe('OgrenimBilgileri Management Update Component', () => {
        let comp: OgrenimBilgileriUpdateComponent;
        let fixture: ComponentFixture<OgrenimBilgileriUpdateComponent>;
        let service: OgrenimBilgileriService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [OgrenimBilgileriUpdateComponent]
            })
                .overrideTemplate(OgrenimBilgileriUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(OgrenimBilgileriUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OgrenimBilgileriService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new OgrenimBilgileri(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.ogrenimBilgileri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new OgrenimBilgileri();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.ogrenimBilgileri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
