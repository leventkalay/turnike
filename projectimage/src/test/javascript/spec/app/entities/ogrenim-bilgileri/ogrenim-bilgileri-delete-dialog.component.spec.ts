/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectimageTestModule } from '../../../test.module';
import { OgrenimBilgileriDeleteDialogComponent } from 'app/entities/ogrenim-bilgileri/ogrenim-bilgileri-delete-dialog.component';
import { OgrenimBilgileriService } from 'app/entities/ogrenim-bilgileri/ogrenim-bilgileri.service';

describe('Component Tests', () => {
    describe('OgrenimBilgileri Management Delete Component', () => {
        let comp: OgrenimBilgileriDeleteDialogComponent;
        let fixture: ComponentFixture<OgrenimBilgileriDeleteDialogComponent>;
        let service: OgrenimBilgileriService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [OgrenimBilgileriDeleteDialogComponent]
            })
                .overrideTemplate(OgrenimBilgileriDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OgrenimBilgileriDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OgrenimBilgileriService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
