/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { OgrenimBilgileriService } from 'app/entities/ogrenim-bilgileri/ogrenim-bilgileri.service';
import { IOgrenimBilgileri, OgrenimBilgileri, OgrenimDurumu, Diller, Ulke, Sehir } from 'app/shared/model/ogrenim-bilgileri.model';

describe('Service Tests', () => {
    describe('OgrenimBilgileri Service', () => {
        let injector: TestBed;
        let service: OgrenimBilgileriService;
        let httpMock: HttpTestingController;
        let elemDefault: IOgrenimBilgileri;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(OgrenimBilgileriService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new OgrenimBilgileri(0, OgrenimDurumu.ILKOKUL, 'AAAAAAA', 0, Diller.TURKCE, Ulke.TURKIYE, Sehir.ADANA);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a OgrenimBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new OgrenimBilgileri(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a OgrenimBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        ogrenimDurumu: 'BBBBBB',
                        okul: 'BBBBBB',
                        mezuniyetYili: 1,
                        ogrenimDili: 'BBBBBB',
                        ulke: 'BBBBBB',
                        sehir: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of OgrenimBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        ogrenimDurumu: 'BBBBBB',
                        okul: 'BBBBBB',
                        mezuniyetYili: 1,
                        ogrenimDili: 'BBBBBB',
                        ulke: 'BBBBBB',
                        sehir: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a OgrenimBilgileri', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
