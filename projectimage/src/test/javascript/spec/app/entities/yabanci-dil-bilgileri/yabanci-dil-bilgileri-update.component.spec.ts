/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { YabanciDilBilgileriUpdateComponent } from 'app/entities/yabanci-dil-bilgileri/yabanci-dil-bilgileri-update.component';
import { YabanciDilBilgileriService } from 'app/entities/yabanci-dil-bilgileri/yabanci-dil-bilgileri.service';
import { YabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';

describe('Component Tests', () => {
    describe('YabanciDilBilgileri Management Update Component', () => {
        let comp: YabanciDilBilgileriUpdateComponent;
        let fixture: ComponentFixture<YabanciDilBilgileriUpdateComponent>;
        let service: YabanciDilBilgileriService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [YabanciDilBilgileriUpdateComponent]
            })
                .overrideTemplate(YabanciDilBilgileriUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(YabanciDilBilgileriUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(YabanciDilBilgileriService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new YabanciDilBilgileri(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.yabanciDilBilgileri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new YabanciDilBilgileri();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.yabanciDilBilgileri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
