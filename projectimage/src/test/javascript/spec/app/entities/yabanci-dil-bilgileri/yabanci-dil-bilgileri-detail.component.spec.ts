/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { YabanciDilBilgileriDetailComponent } from 'app/entities/yabanci-dil-bilgileri/yabanci-dil-bilgileri-detail.component';
import { YabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';

describe('Component Tests', () => {
    describe('YabanciDilBilgileri Management Detail Component', () => {
        let comp: YabanciDilBilgileriDetailComponent;
        let fixture: ComponentFixture<YabanciDilBilgileriDetailComponent>;
        const route = ({ data: of({ yabanciDilBilgileri: new YabanciDilBilgileri(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [YabanciDilBilgileriDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(YabanciDilBilgileriDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(YabanciDilBilgileriDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.yabanciDilBilgileri).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
