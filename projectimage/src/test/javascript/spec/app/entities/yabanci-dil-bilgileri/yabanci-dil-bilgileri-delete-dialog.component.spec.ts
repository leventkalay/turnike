/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectimageTestModule } from '../../../test.module';
import { YabanciDilBilgileriDeleteDialogComponent } from 'app/entities/yabanci-dil-bilgileri/yabanci-dil-bilgileri-delete-dialog.component';
import { YabanciDilBilgileriService } from 'app/entities/yabanci-dil-bilgileri/yabanci-dil-bilgileri.service';

describe('Component Tests', () => {
    describe('YabanciDilBilgileri Management Delete Component', () => {
        let comp: YabanciDilBilgileriDeleteDialogComponent;
        let fixture: ComponentFixture<YabanciDilBilgileriDeleteDialogComponent>;
        let service: YabanciDilBilgileriService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [YabanciDilBilgileriDeleteDialogComponent]
            })
                .overrideTemplate(YabanciDilBilgileriDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(YabanciDilBilgileriDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(YabanciDilBilgileriService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
