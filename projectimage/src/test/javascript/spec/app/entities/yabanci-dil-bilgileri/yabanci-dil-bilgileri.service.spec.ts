/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { YabanciDilBilgileriService } from 'app/entities/yabanci-dil-bilgileri/yabanci-dil-bilgileri.service';
import { IYabanciDilBilgileri, YabanciDilBilgileri, Diller, BelgeTuru } from 'app/shared/model/yabanci-dil-bilgileri.model';

describe('Service Tests', () => {
    describe('YabanciDilBilgileri Service', () => {
        let injector: TestBed;
        let service: YabanciDilBilgileriService;
        let httpMock: HttpTestingController;
        let elemDefault: IYabanciDilBilgileri;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(YabanciDilBilgileriService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new YabanciDilBilgileri(0, Diller.TURKCE, BelgeTuru.CAE, currentDate, 0);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        belgeTarihi: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a YabanciDilBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        belgeTarihi: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        belgeTarihi: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new YabanciDilBilgileri(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a YabanciDilBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        dil: 'BBBBBB',
                        belgeTuru: 'BBBBBB',
                        belgeTarihi: currentDate.format(DATE_FORMAT),
                        puan: 1
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        belgeTarihi: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of YabanciDilBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        dil: 'BBBBBB',
                        belgeTuru: 'BBBBBB',
                        belgeTarihi: currentDate.format(DATE_FORMAT),
                        puan: 1
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        belgeTarihi: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a YabanciDilBilgileri', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
