/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { OzelIlgiAlanlariUpdateComponent } from 'app/entities/ozel-ilgi-alanlari/ozel-ilgi-alanlari-update.component';
import { OzelIlgiAlanlariService } from 'app/entities/ozel-ilgi-alanlari/ozel-ilgi-alanlari.service';
import { OzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';

describe('Component Tests', () => {
    describe('OzelIlgiAlanlari Management Update Component', () => {
        let comp: OzelIlgiAlanlariUpdateComponent;
        let fixture: ComponentFixture<OzelIlgiAlanlariUpdateComponent>;
        let service: OzelIlgiAlanlariService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [OzelIlgiAlanlariUpdateComponent]
            })
                .overrideTemplate(OzelIlgiAlanlariUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(OzelIlgiAlanlariUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OzelIlgiAlanlariService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new OzelIlgiAlanlari(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.ozelIlgiAlanlari = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new OzelIlgiAlanlari();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.ozelIlgiAlanlari = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
