/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { OzelIlgiAlanlariDetailComponent } from 'app/entities/ozel-ilgi-alanlari/ozel-ilgi-alanlari-detail.component';
import { OzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';

describe('Component Tests', () => {
    describe('OzelIlgiAlanlari Management Detail Component', () => {
        let comp: OzelIlgiAlanlariDetailComponent;
        let fixture: ComponentFixture<OzelIlgiAlanlariDetailComponent>;
        const route = ({ data: of({ ozelIlgiAlanlari: new OzelIlgiAlanlari(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [OzelIlgiAlanlariDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(OzelIlgiAlanlariDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OzelIlgiAlanlariDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.ozelIlgiAlanlari).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
