/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectimageTestModule } from '../../../test.module';
import { OzelIlgiAlanlariDeleteDialogComponent } from 'app/entities/ozel-ilgi-alanlari/ozel-ilgi-alanlari-delete-dialog.component';
import { OzelIlgiAlanlariService } from 'app/entities/ozel-ilgi-alanlari/ozel-ilgi-alanlari.service';

describe('Component Tests', () => {
    describe('OzelIlgiAlanlari Management Delete Component', () => {
        let comp: OzelIlgiAlanlariDeleteDialogComponent;
        let fixture: ComponentFixture<OzelIlgiAlanlariDeleteDialogComponent>;
        let service: OzelIlgiAlanlariService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [OzelIlgiAlanlariDeleteDialogComponent]
            })
                .overrideTemplate(OzelIlgiAlanlariDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(OzelIlgiAlanlariDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OzelIlgiAlanlariService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
