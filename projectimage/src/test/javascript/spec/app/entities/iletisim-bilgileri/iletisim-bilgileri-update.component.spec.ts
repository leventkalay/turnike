/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { IletisimBilgileriUpdateComponent } from 'app/entities/iletisim-bilgileri/iletisim-bilgileri-update.component';
import { IletisimBilgileriService } from 'app/entities/iletisim-bilgileri/iletisim-bilgileri.service';
import { IletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';

describe('Component Tests', () => {
    describe('IletisimBilgileri Management Update Component', () => {
        let comp: IletisimBilgileriUpdateComponent;
        let fixture: ComponentFixture<IletisimBilgileriUpdateComponent>;
        let service: IletisimBilgileriService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [IletisimBilgileriUpdateComponent]
            })
                .overrideTemplate(IletisimBilgileriUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(IletisimBilgileriUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IletisimBilgileriService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new IletisimBilgileri(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.iletisimBilgileri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new IletisimBilgileri();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.iletisimBilgileri = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
