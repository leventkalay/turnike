/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { IletisimBilgileriDetailComponent } from 'app/entities/iletisim-bilgileri/iletisim-bilgileri-detail.component';
import { IletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';

describe('Component Tests', () => {
    describe('IletisimBilgileri Management Detail Component', () => {
        let comp: IletisimBilgileriDetailComponent;
        let fixture: ComponentFixture<IletisimBilgileriDetailComponent>;
        const route = ({ data: of({ iletisimBilgileri: new IletisimBilgileri(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [IletisimBilgileriDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(IletisimBilgileriDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(IletisimBilgileriDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.iletisimBilgileri).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
