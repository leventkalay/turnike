/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { IletisimBilgileriService } from 'app/entities/iletisim-bilgileri/iletisim-bilgileri.service';
import { IIletisimBilgileri, IletisimBilgileri, Yakinlik } from 'app/shared/model/iletisim-bilgileri.model';

describe('Service Tests', () => {
    describe('IletisimBilgileri Service', () => {
        let injector: TestBed;
        let service: IletisimBilgileriService;
        let httpMock: HttpTestingController;
        let elemDefault: IIletisimBilgileri;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(IletisimBilgileriService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new IletisimBilgileri(
                0,
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                Yakinlik.BABA,
                'AAAAAAA'
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a IletisimBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new IletisimBilgileri(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a IletisimBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        adres: 'BBBBBB',
                        cepTelefonu: 'BBBBBB',
                        cepTelefonuAlt: 'BBBBBB',
                        evTelefonu: 'BBBBBB',
                        dahiliTelefon: 'BBBBBB',
                        eposta: 'BBBBBB',
                        acilAdSoyad: 'BBBBBB',
                        yakinlik: 'BBBBBB',
                        acilTelefon: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of IletisimBilgileri', async () => {
                const returnedFromService = Object.assign(
                    {
                        adres: 'BBBBBB',
                        cepTelefonu: 'BBBBBB',
                        cepTelefonuAlt: 'BBBBBB',
                        evTelefonu: 'BBBBBB',
                        dahiliTelefon: 'BBBBBB',
                        eposta: 'BBBBBB',
                        acilAdSoyad: 'BBBBBB',
                        yakinlik: 'BBBBBB',
                        acilTelefon: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a IletisimBilgileri', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
