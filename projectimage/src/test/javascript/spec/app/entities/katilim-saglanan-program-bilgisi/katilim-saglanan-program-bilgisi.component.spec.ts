/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { ProjectimageTestModule } from '../../../test.module';
import { KatilimSaglananProgramBilgisiComponent } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi.component';
import { KatilimSaglananProgramBilgisiService } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi.service';
import { KatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';

describe('Component Tests', () => {
    describe('KatilimSaglananProgramBilgisi Management Component', () => {
        let comp: KatilimSaglananProgramBilgisiComponent;
        let fixture: ComponentFixture<KatilimSaglananProgramBilgisiComponent>;
        let service: KatilimSaglananProgramBilgisiService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [KatilimSaglananProgramBilgisiComponent],
                providers: [
                    {
                        provide: ActivatedRoute,
                        useValue: {
                            data: {
                                subscribe: (fn: (value: Data) => void) =>
                                    fn({
                                        pagingParams: {
                                            predicate: 'id',
                                            reverse: false,
                                            page: 0
                                        }
                                    })
                            }
                        }
                    }
                ]
            })
                .overrideTemplate(KatilimSaglananProgramBilgisiComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(KatilimSaglananProgramBilgisiComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KatilimSaglananProgramBilgisiService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new KatilimSaglananProgramBilgisi(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.katilimSaglananProgramBilgisis[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });

        it('should load a page', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new KatilimSaglananProgramBilgisi(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.loadPage(1);

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.katilimSaglananProgramBilgisis[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });

        it('should not load a page is the page is the same as the previous page', () => {
            spyOn(service, 'query').and.callThrough();

            // WHEN
            comp.loadPage(0);

            // THEN
            expect(service.query).toHaveBeenCalledTimes(0);
        });

        it('should re-initialize the page', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new KatilimSaglananProgramBilgisi(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.loadPage(1);
            comp.clear();

            // THEN
            expect(comp.page).toEqual(0);
            expect(service.query).toHaveBeenCalledTimes(2);
            expect(comp.katilimSaglananProgramBilgisis[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
        it('should calculate the sort attribute for an id', () => {
            // WHEN
            const result = comp.sort();

            // THEN
            expect(result).toEqual(['id,desc']);
        });

        it('should calculate the sort attribute for a non-id attribute', () => {
            // GIVEN
            comp.predicate = 'name';

            // WHEN
            const result = comp.sort();

            // THEN
            expect(result).toEqual(['name,desc', 'id']);
        });
    });
});
