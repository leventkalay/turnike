/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { KatilimSaglananProgramBilgisiDetailComponent } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi-detail.component';
import { KatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';

describe('Component Tests', () => {
    describe('KatilimSaglananProgramBilgisi Management Detail Component', () => {
        let comp: KatilimSaglananProgramBilgisiDetailComponent;
        let fixture: ComponentFixture<KatilimSaglananProgramBilgisiDetailComponent>;
        const route = ({ data: of({ katilimSaglananProgramBilgisi: new KatilimSaglananProgramBilgisi(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [KatilimSaglananProgramBilgisiDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(KatilimSaglananProgramBilgisiDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(KatilimSaglananProgramBilgisiDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.katilimSaglananProgramBilgisi).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
