/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { KatilimSaglananProgramBilgisiService } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi.service';
import {
    IKatilimSaglananProgramBilgisi,
    KatilimSaglananProgramBilgisi,
    ProgramTuru,
    ProgramGorevi,
    Ulke,
    Sehir,
    BelgeTuru
} from 'app/shared/model/katilim-saglanan-program-bilgisi.model';

describe('Service Tests', () => {
    describe('KatilimSaglananProgramBilgisi Service', () => {
        let injector: TestBed;
        let service: KatilimSaglananProgramBilgisiService;
        let httpMock: HttpTestingController;
        let elemDefault: IKatilimSaglananProgramBilgisi;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(KatilimSaglananProgramBilgisiService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new KatilimSaglananProgramBilgisi(
                0,
                ProgramTuru.SEMINER,
                ProgramGorevi.KATILIMCI,
                'AAAAAAA',
                'AAAAAAA',
                0,
                0,
                'AAAAAAA',
                Ulke.TURKIYE,
                Sehir.ADANA,
                BelgeTuru.CAE,
                'AAAAAAA'
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a KatilimSaglananProgramBilgisi', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new KatilimSaglananProgramBilgisi(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a KatilimSaglananProgramBilgisi', async () => {
                const returnedFromService = Object.assign(
                    {
                        programTuru: 'BBBBBB',
                        programGorevi: 'BBBBBB',
                        programAdi: 'BBBBBB',
                        programKonusu: 'BBBBBB',
                        baslangicYili: 1,
                        bitisYili: 1,
                        duzenleyenKurum: 'BBBBBB',
                        ulke: 'BBBBBB',
                        sehir: 'BBBBBB',
                        belgeTuru: 'BBBBBB',
                        anahtarKelimeler: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of KatilimSaglananProgramBilgisi', async () => {
                const returnedFromService = Object.assign(
                    {
                        programTuru: 'BBBBBB',
                        programGorevi: 'BBBBBB',
                        programAdi: 'BBBBBB',
                        programKonusu: 'BBBBBB',
                        baslangicYili: 1,
                        bitisYili: 1,
                        duzenleyenKurum: 'BBBBBB',
                        ulke: 'BBBBBB',
                        sehir: 'BBBBBB',
                        belgeTuru: 'BBBBBB',
                        anahtarKelimeler: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a KatilimSaglananProgramBilgisi', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
