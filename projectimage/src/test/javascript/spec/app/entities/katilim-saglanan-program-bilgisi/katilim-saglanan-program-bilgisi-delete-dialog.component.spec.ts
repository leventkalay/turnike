/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectimageTestModule } from '../../../test.module';
import { KatilimSaglananProgramBilgisiDeleteDialogComponent } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi-delete-dialog.component';
import { KatilimSaglananProgramBilgisiService } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi.service';

describe('Component Tests', () => {
    describe('KatilimSaglananProgramBilgisi Management Delete Component', () => {
        let comp: KatilimSaglananProgramBilgisiDeleteDialogComponent;
        let fixture: ComponentFixture<KatilimSaglananProgramBilgisiDeleteDialogComponent>;
        let service: KatilimSaglananProgramBilgisiService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [KatilimSaglananProgramBilgisiDeleteDialogComponent]
            })
                .overrideTemplate(KatilimSaglananProgramBilgisiDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(KatilimSaglananProgramBilgisiDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KatilimSaglananProgramBilgisiService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
