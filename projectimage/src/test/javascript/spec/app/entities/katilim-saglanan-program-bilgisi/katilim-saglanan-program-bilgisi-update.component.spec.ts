/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { KatilimSaglananProgramBilgisiUpdateComponent } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi-update.component';
import { KatilimSaglananProgramBilgisiService } from 'app/entities/katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi.service';
import { KatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';

describe('Component Tests', () => {
    describe('KatilimSaglananProgramBilgisi Management Update Component', () => {
        let comp: KatilimSaglananProgramBilgisiUpdateComponent;
        let fixture: ComponentFixture<KatilimSaglananProgramBilgisiUpdateComponent>;
        let service: KatilimSaglananProgramBilgisiService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [KatilimSaglananProgramBilgisiUpdateComponent]
            })
                .overrideTemplate(KatilimSaglananProgramBilgisiUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(KatilimSaglananProgramBilgisiUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KatilimSaglananProgramBilgisiService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new KatilimSaglananProgramBilgisi(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.katilimSaglananProgramBilgisi = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new KatilimSaglananProgramBilgisi();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.katilimSaglananProgramBilgisi = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
