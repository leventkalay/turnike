/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectimageTestModule } from '../../../test.module';
import { KisiselBilgilerDeleteDialogComponent } from 'app/entities/kisisel-bilgiler/kisisel-bilgiler-delete-dialog.component';
import { KisiselBilgilerService } from 'app/entities/kisisel-bilgiler/kisisel-bilgiler.service';

describe('Component Tests', () => {
    describe('KisiselBilgiler Management Delete Component', () => {
        let comp: KisiselBilgilerDeleteDialogComponent;
        let fixture: ComponentFixture<KisiselBilgilerDeleteDialogComponent>;
        let service: KisiselBilgilerService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [KisiselBilgilerDeleteDialogComponent]
            })
                .overrideTemplate(KisiselBilgilerDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(KisiselBilgilerDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KisiselBilgilerService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
