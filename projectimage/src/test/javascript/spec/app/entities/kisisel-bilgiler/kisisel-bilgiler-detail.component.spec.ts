/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { KisiselBilgilerDetailComponent } from 'app/entities/kisisel-bilgiler/kisisel-bilgiler-detail.component';
import { KisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';

describe('Component Tests', () => {
    describe('KisiselBilgiler Management Detail Component', () => {
        let comp: KisiselBilgilerDetailComponent;
        let fixture: ComponentFixture<KisiselBilgilerDetailComponent>;
        const route = ({ data: of({ kisiselBilgiler: new KisiselBilgiler(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [KisiselBilgilerDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(KisiselBilgilerDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(KisiselBilgilerDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.kisiselBilgiler).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
