/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ProjectimageTestModule } from '../../../test.module';
import { KisiselBilgilerUpdateComponent } from 'app/entities/kisisel-bilgiler/kisisel-bilgiler-update.component';
import { KisiselBilgilerService } from 'app/entities/kisisel-bilgiler/kisisel-bilgiler.service';
import { KisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';

describe('Component Tests', () => {
    describe('KisiselBilgiler Management Update Component', () => {
        let comp: KisiselBilgilerUpdateComponent;
        let fixture: ComponentFixture<KisiselBilgilerUpdateComponent>;
        let service: KisiselBilgilerService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ProjectimageTestModule],
                declarations: [KisiselBilgilerUpdateComponent]
            })
                .overrideTemplate(KisiselBilgilerUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(KisiselBilgilerUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KisiselBilgilerService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new KisiselBilgiler(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.kisiselBilgiler = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new KisiselBilgiler();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.kisiselBilgiler = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
