/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { KisiselBilgilerService } from 'app/entities/kisisel-bilgiler/kisisel-bilgiler.service';
import {
    IKisiselBilgiler,
    KisiselBilgiler,
    MedeniDurum,
    Cinsiyet,
    IstihdamSekli,
    Askerlik,
    KanGrubu
} from 'app/shared/model/kisisel-bilgiler.model';

describe('Service Tests', () => {
    describe('KisiselBilgiler Service', () => {
        let injector: TestBed;
        let service: KisiselBilgilerService;
        let httpMock: HttpTestingController;
        let elemDefault: IKisiselBilgiler;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(KisiselBilgilerService);
            httpMock = injector.get(HttpTestingController);

            elemDefault = new KisiselBilgiler(
                0,
                'AAAAAAA',
                'AAAAAAA',
                0,
                'AAAAAAA',
                'AAAAAAA',
                MedeniDurum.EVLI,
                Cinsiyet.Erkek,
                IstihdamSekli.SK_657,
                Askerlik.YAPTI,
                KanGrubu.BILINMIYOR,
                'image/png',
                'AAAAAAA',
                'image/png',
                'AAAAAAA',
                'AAAAAAA'
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign({}, elemDefault);
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a KisiselBilgiler', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .create(new KisiselBilgiler(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a KisiselBilgiler', async () => {
                const returnedFromService = Object.assign(
                    {
                        ad: 'BBBBBB',
                        soyad: 'BBBBBB',
                        pirimGunSayisi: 1,
                        sgkNo: 'BBBBBB',
                        sicilNo: 'BBBBBB',
                        medeniDurum: 'BBBBBB',
                        cinsiyet: 'BBBBBB',
                        istihdamSekli: 'BBBBBB',
                        askerlik: 'BBBBBB',
                        kangrubu: 'BBBBBB',
                        image: 'BBBBBB',
                        any: 'BBBBBB',
                        text: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign({}, returnedFromService);
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of KisiselBilgiler', async () => {
                const returnedFromService = Object.assign(
                    {
                        ad: 'BBBBBB',
                        soyad: 'BBBBBB',
                        pirimGunSayisi: 1,
                        sgkNo: 'BBBBBB',
                        sicilNo: 'BBBBBB',
                        medeniDurum: 'BBBBBB',
                        cinsiyet: 'BBBBBB',
                        istihdamSekli: 'BBBBBB',
                        askerlik: 'BBBBBB',
                        kangrubu: 'BBBBBB',
                        image: 'BBBBBB',
                        any: 'BBBBBB',
                        text: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign({}, returnedFromService);
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a KisiselBilgiler', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
