import { NgModule } from '@angular/core';

import { ProjectimageSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [ProjectimageSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [ProjectimageSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class ProjectimageSharedCommonModule {}
