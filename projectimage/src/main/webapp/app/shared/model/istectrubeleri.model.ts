export const enum CalismaAlani {
    KAMU = 'KAMU',
    OZEL = 'OZEL',
    ULUSLARARASI = 'ULUSLARARASI'
}

export const enum CalismaSekli {
    TAM_ZAMANLI = 'TAM_ZAMANLI',
    YARI_ZAMANLI = 'YARI_ZAMANLI',
    STAJ = 'STAJ'
}

export const enum Aylar {
    OCAK = 'OCAK',
    SUBAT = 'SUBAT',
    MART = 'MART',
    NISAN = 'NISAN',
    MAYIS = 'MAYIS',
    HAZIRAN = 'HAZIRAN',
    TEMMUZ = 'TEMMUZ',
    AGUSTOS = 'AGUSTOS',
    EYLUL = 'EYLUL',
    EKIM = 'EKIM',
    KASIM = 'KASIM',
    ARALIK = 'ARALIK'
}

export const enum Ulke {
    TURKIYE = 'TURKIYE',
    INGILTERE = 'INGILTERE',
    ALMANYA = 'ALMANYA',
    JAPONYA = 'JAPONYA'
}

export const enum Sehir {
    ADANA = 'ADANA',
    ADIYAMAN = 'ADIYAMAN',
    AFYON = 'AFYON',
    AGRI = 'AGRI',
    AMASYA = 'AMASYA',
    ANKARA = 'ANKARA',
    ANTALYA = 'ANTALYA',
    ARTVIN = 'ARTVIN',
    AYDIN = 'AYDIN',
    BALIKESIR = 'BALIKESIR',
    ISTANBUL = 'ISTANBUL',
    IZMIR = 'IZMIR'
}

export interface IIstectrubeleri {
    id?: number;
    calismaAlani?: CalismaAlani;
    calismaSekli?: CalismaSekli;
    kurumAdi?: string;
    gorevBirimi?: string;
    unvan?: string;
    baslangicAyi?: Aylar;
    baslangicYili?: number;
    bitisAyi?: Aylar;
    bitisYili?: number;
    gorevTanimi?: string;
    ulke?: Ulke;
    sehir?: Sehir;
    yararliBilgier?: string;
}

export class Istectrubeleri implements IIstectrubeleri {
    constructor(
        public id?: number,
        public calismaAlani?: CalismaAlani,
        public calismaSekli?: CalismaSekli,
        public kurumAdi?: string,
        public gorevBirimi?: string,
        public unvan?: string,
        public baslangicAyi?: Aylar,
        public baslangicYili?: number,
        public bitisAyi?: Aylar,
        public bitisYili?: number,
        public gorevTanimi?: string,
        public ulke?: Ulke,
        public sehir?: Sehir,
        public yararliBilgier?: string
    ) {}
}
