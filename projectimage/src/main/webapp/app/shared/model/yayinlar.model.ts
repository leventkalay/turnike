export const enum YayinTuru {
    OZEL = 'OZEL',
    KAMU = 'KAMU',
    DIGER = 'DIGER'
}

export interface IYayinlar {
    id?: number;
    yayinTuru?: YayinTuru;
    yazarAdi?: string;
    isbn?: string;
    sayfaSayisi?: number;
    yayinBilgisi?: string;
    yayinYil?: string;
    elektronikErisim?: string;
    anahtarKelimeler?: string;
}

export class Yayinlar implements IYayinlar {
    constructor(
        public id?: number,
        public yayinTuru?: YayinTuru,
        public yazarAdi?: string,
        public isbn?: string,
        public sayfaSayisi?: number,
        public yayinBilgisi?: string,
        public yayinYil?: string,
        public elektronikErisim?: string,
        public anahtarKelimeler?: string
    ) {}
}
