export const enum OgrenimDurumu {
    ILKOKUL = 'ILKOKUL',
    ORTAOKUL = 'ORTAOKUL',
    LISE = 'LISE',
    ONLISANS = 'ONLISANS',
    LISANS = 'LISANS',
    LISANSUSTU = 'LISANSUSTU',
    DOKTORA = 'DOKTORA'
}

export const enum Diller {
    TURKCE = 'TURKCE',
    INGILIZCE = 'INGILIZCE',
    FRANSIZCA = 'FRANSIZCA',
    ALMANCA = 'ALMANCA',
    JAPONCA = 'JAPONCA'
}

export const enum Ulke {
    TURKIYE = 'TURKIYE',
    INGILTERE = 'INGILTERE',
    ALMANYA = 'ALMANYA',
    JAPONYA = 'JAPONYA'
}

export const enum Sehir {
    ADANA = 'ADANA',
    ADIYAMAN = 'ADIYAMAN',
    AFYON = 'AFYON',
    AGRI = 'AGRI',
    AMASYA = 'AMASYA',
    ANKARA = 'ANKARA',
    ANTALYA = 'ANTALYA',
    ARTVIN = 'ARTVIN',
    AYDIN = 'AYDIN',
    BALIKESIR = 'BALIKESIR',
    ISTANBUL = 'ISTANBUL',
    IZMIR = 'IZMIR'
}

export interface IOgrenimBilgileri {
    id?: number;
    ogrenimDurumu?: OgrenimDurumu;
    okul?: string;
    mezuniyetYili?: number;
    ogrenimDili?: Diller;
    ulke?: Ulke;
    sehir?: Sehir;
}

export class OgrenimBilgileri implements IOgrenimBilgileri {
    constructor(
        public id?: number,
        public ogrenimDurumu?: OgrenimDurumu,
        public okul?: string,
        public mezuniyetYili?: number,
        public ogrenimDili?: Diller,
        public ulke?: Ulke,
        public sehir?: Sehir
    ) {}
}
