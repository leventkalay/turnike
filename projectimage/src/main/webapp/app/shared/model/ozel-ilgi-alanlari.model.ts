export interface IOzelIlgiAlanlari {
    id?: number;
    ozelIlgiAlanlari?: string;
}

export class OzelIlgiAlanlari implements IOzelIlgiAlanlari {
    constructor(public id?: number, public ozelIlgiAlanlari?: string) {}
}
