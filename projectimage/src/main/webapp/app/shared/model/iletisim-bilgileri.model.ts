export const enum Yakinlik {
    BABA = 'BABA',
    ANNE = 'ANNE',
    KARDES = 'KARDES',
    KIZI = 'KIZI',
    OGLU = 'OGLU',
    ESI = 'ESI',
    DIGER = 'DIGER'
}

export interface IIletisimBilgileri {
    id?: number;
    adres?: string;
    cepTelefonu?: string;
    cepTelefonuAlt?: string;
    evTelefonu?: string;
    dahiliTelefon?: string;
    eposta?: string;
    acilAdSoyad?: string;
    yakinlik?: Yakinlik;
    acilTelefon?: string;
}

export class IletisimBilgileri implements IIletisimBilgileri {
    constructor(
        public id?: number,
        public adres?: string,
        public cepTelefonu?: string,
        public cepTelefonuAlt?: string,
        public evTelefonu?: string,
        public dahiliTelefon?: string,
        public eposta?: string,
        public acilAdSoyad?: string,
        public yakinlik?: Yakinlik,
        public acilTelefon?: string
    ) {}
}
