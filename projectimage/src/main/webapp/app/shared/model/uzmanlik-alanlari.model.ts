export const enum DetayBilgiler {
    ALTYAPI_YATIRIMLARI = 'ALTYAPI_YATIRIMLARI',
    ARGE = 'ARGE',
    BILGI_VE_ILETISIM_TEKONOLOJILERI = 'BILGI_VE_ILETISIM_TEKONOLOJILERI',
    BUTCE_HAZIRLIK_VE_UYGULAMA_SURECLERI = 'BUTCE_HAZIRLIK_VE_UYGULAMA_SURECLERI',
    CEVRE = 'CEVRE',
    EGITIM = 'EGITIM',
    ENERJI = 'ENERJI',
    FON_YONETIMI = 'FON_YONETIMI',
    GENCLIKVESPOR = 'GENCLIKVESPOR'
}

export interface IUzmanlikAlanlari {
    id?: number;
    detayBilgiler?: DetayBilgiler;
    aciklama?: string;
}

export class UzmanlikAlanlari implements IUzmanlikAlanlari {
    constructor(public id?: number, public detayBilgiler?: DetayBilgiler, public aciklama?: string) {}
}
