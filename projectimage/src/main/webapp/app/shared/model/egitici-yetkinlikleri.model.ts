export const enum KurumTipi {
    UNIVERSITE = 'UNIVERSITE',
    DIGER = 'DIGER'
}

export interface IEgiticiYetkinlikleri {
    id?: number;
    kurumTipi?: KurumTipi;
    universiteAdi?: string;
    kurumAdi?: string;
    fakulte?: string;
    bolum?: string;
    konuDersAdi?: string;
}

export class EgiticiYetkinlikleri implements IEgiticiYetkinlikleri {
    constructor(
        public id?: number,
        public kurumTipi?: KurumTipi,
        public universiteAdi?: string,
        public kurumAdi?: string,
        public fakulte?: string,
        public bolum?: string,
        public konuDersAdi?: string
    ) {}
}
