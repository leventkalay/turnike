export const enum ProjeCiktisi {
    PROJE_OZETI = 'PROJE_OZETI',
    PROJE_RAPORU = 'PROJE_RAPORU',
    DIGER = 'DIGER'
}

export interface IProje {
    id?: number;
    projeAdi?: string;
    gorevi?: string;
    konusu?: string;
    amaci?: string;
    yil?: number;
    yurutenKurum?: string;
    projeCiktisi?: ProjeCiktisi;
    anahtarKelimeler?: string;
}

export class Proje implements IProje {
    constructor(
        public id?: number,
        public projeAdi?: string,
        public gorevi?: string,
        public konusu?: string,
        public amaci?: string,
        public yil?: number,
        public yurutenKurum?: string,
        public projeCiktisi?: ProjeCiktisi,
        public anahtarKelimeler?: string
    ) {}
}
