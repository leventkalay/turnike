export const enum ProgramTuru {
    SEMINER = 'SEMINER',
    KURS = 'KURS',
    TOPLANTI = 'TOPLANTI',
    CALISTAY = 'CALISTAY',
    KONFERANS = 'KONFERANS',
    KONGRE = 'KONGRE',
    PANEL = 'PANEL',
    OZEL_IHTISAS_KOMISYONU = 'OZEL_IHTISAS_KOMISYONU',
    DIGER = 'DIGER'
}

export const enum ProgramGorevi {
    KATILIMCI = 'KATILIMCI',
    KURSIYER = 'KURSIYER',
    KONUSMACI = 'KONUSMACI',
    EGITICI = 'EGITICI',
    MODERATOR = 'MODERATOR',
    PANELIST = 'PANELIST',
    SUNUCU = 'SUNUCU',
    ORGANIZASYON_VE_PLANLAMA = 'ORGANIZASYON_VE_PLANLAMA',
    DIGER = 'DIGER'
}

export const enum Ulke {
    TURKIYE = 'TURKIYE',
    INGILTERE = 'INGILTERE',
    ALMANYA = 'ALMANYA',
    JAPONYA = 'JAPONYA'
}

export const enum Sehir {
    ADANA = 'ADANA',
    ADIYAMAN = 'ADIYAMAN',
    AFYON = 'AFYON',
    AGRI = 'AGRI',
    AMASYA = 'AMASYA',
    ANKARA = 'ANKARA',
    ANTALYA = 'ANTALYA',
    ARTVIN = 'ARTVIN',
    AYDIN = 'AYDIN',
    BALIKESIR = 'BALIKESIR',
    ISTANBUL = 'ISTANBUL',
    IZMIR = 'IZMIR'
}

export const enum BelgeTuru {
    CAE = 'CAE',
    CPE = 'CPE',
    FCE = 'FCE',
    IELTS = 'IELTS',
    KPDS = 'KPDS',
    TOEFL = 'TOEFL',
    UDS = 'UDS',
    YDS = 'YDS'
}

export interface IKatilimSaglananProgramBilgisi {
    id?: number;
    programTuru?: ProgramTuru;
    programGorevi?: ProgramGorevi;
    programAdi?: string;
    programKonusu?: string;
    baslangicYili?: number;
    bitisYili?: number;
    duzenleyenKurum?: string;
    ulke?: Ulke;
    sehir?: Sehir;
    belgeTuru?: BelgeTuru;
    anahtarKelimeler?: string;
}

export class KatilimSaglananProgramBilgisi implements IKatilimSaglananProgramBilgisi {
    constructor(
        public id?: number,
        public programTuru?: ProgramTuru,
        public programGorevi?: ProgramGorevi,
        public programAdi?: string,
        public programKonusu?: string,
        public baslangicYili?: number,
        public bitisYili?: number,
        public duzenleyenKurum?: string,
        public ulke?: Ulke,
        public sehir?: Sehir,
        public belgeTuru?: BelgeTuru,
        public anahtarKelimeler?: string
    ) {}
}
