import { IUser } from 'app/core/user/user.model';

export const enum MedeniDurum {
    EVLI = 'EVLI',
    BEKAR = 'BEKAR'
}

export const enum Cinsiyet {
    Erkek = 'Erkek',
    KADIN = 'KADIN'
}

export const enum IstihdamSekli {
    SK_657 = 'SK_657',
    KHK_696 = 'KHK_696'
}

export const enum Askerlik {
    YAPTI = 'YAPTI',
    YAPMADI = 'YAPMADI',
    MUAF = 'MUAF',
    TECILLI = 'TECILLI'
}

export const enum KanGrubu {
    BILINMIYOR = 'BILINMIYOR',
    APozitif = 'APozitif',
    ANegatif = 'ANegatif',
    ABPozitif = 'ABPozitif',
    ABNegatif = 'ABNegatif',
    BPozitif = 'BPozitif',
    BNegatif = 'BNegatif',
    SifirPozitif = 'SifirPozitif',
    SifirNegatif = 'SifirNegatif'
}

export interface IKisiselBilgiler {
    id?: number;
    ad?: string;
    soyad?: string;
    pirimGunSayisi?: number;
    sgkNo?: string;
    sicilNo?: string;
    medeniDurum?: MedeniDurum;
    cinsiyet?: Cinsiyet;
    istihdamSekli?: IstihdamSekli;
    askerlik?: Askerlik;
    kangrubu?: KanGrubu;
    imageContentType?: string;
    image?: any;
    anyContentType?: string;
    any?: any;
    text?: any;
    user?: IUser;
}

export class KisiselBilgiler implements IKisiselBilgiler {
    constructor(
        public id?: number,
        public ad?: string,
        public soyad?: string,
        public pirimGunSayisi?: number,
        public sgkNo?: string,
        public sicilNo?: string,
        public medeniDurum?: MedeniDurum,
        public cinsiyet?: Cinsiyet,
        public istihdamSekli?: IstihdamSekli,
        public askerlik?: Askerlik,
        public kangrubu?: KanGrubu,
        public imageContentType?: string,
        public image?: any,
        public anyContentType?: string,
        public any?: any,
        public text?: any,
        public user?: IUser
    ) {}
}
