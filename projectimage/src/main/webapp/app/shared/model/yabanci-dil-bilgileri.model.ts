import { Moment } from 'moment';

export const enum Diller {
    TURKCE = 'TURKCE',
    INGILIZCE = 'INGILIZCE',
    FRANSIZCA = 'FRANSIZCA',
    ALMANCA = 'ALMANCA',
    JAPONCA = 'JAPONCA'
}

export const enum BelgeTuru {
    CAE = 'CAE',
    CPE = 'CPE',
    FCE = 'FCE',
    IELTS = 'IELTS',
    KPDS = 'KPDS',
    TOEFL = 'TOEFL',
    UDS = 'UDS',
    YDS = 'YDS'
}

export interface IYabanciDilBilgileri {
    id?: number;
    dil?: Diller;
    belgeTuru?: BelgeTuru;
    belgeTarihi?: Moment;
    puan?: number;
}

export class YabanciDilBilgileri implements IYabanciDilBilgileri {
    constructor(public id?: number, public dil?: Diller, public belgeTuru?: BelgeTuru, public belgeTarihi?: Moment, public puan?: number) {}
}
