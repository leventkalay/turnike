import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IKisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';
import { KisiselBilgilerService } from './kisisel-bilgiler.service';

@Component({
    selector: 'jhi-kisisel-bilgiler-delete-dialog',
    templateUrl: './kisisel-bilgiler-delete-dialog.component.html'
})
export class KisiselBilgilerDeleteDialogComponent {
    kisiselBilgiler: IKisiselBilgiler;

    constructor(
        private kisiselBilgilerService: KisiselBilgilerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.kisiselBilgilerService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'kisiselBilgilerListModification',
                content: 'Deleted an kisiselBilgiler'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-kisisel-bilgiler-delete-popup',
    template: ''
})
export class KisiselBilgilerDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ kisiselBilgiler }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(KisiselBilgilerDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.kisiselBilgiler = kisiselBilgiler;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
