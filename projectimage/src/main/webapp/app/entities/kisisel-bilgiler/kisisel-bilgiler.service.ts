import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IKisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';

type EntityResponseType = HttpResponse<IKisiselBilgiler>;
type EntityArrayResponseType = HttpResponse<IKisiselBilgiler[]>;

@Injectable({ providedIn: 'root' })
export class KisiselBilgilerService {
    public resourceUrl = SERVER_API_URL + 'api/kisisel-bilgilers';

    constructor(private http: HttpClient) {}

    create(kisiselBilgiler: IKisiselBilgiler): Observable<EntityResponseType> {
        return this.http.post<IKisiselBilgiler>(this.resourceUrl, kisiselBilgiler, { observe: 'response' });
    }

    update(kisiselBilgiler: IKisiselBilgiler): Observable<EntityResponseType> {
        return this.http.put<IKisiselBilgiler>(this.resourceUrl, kisiselBilgiler, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IKisiselBilgiler>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IKisiselBilgiler[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
