import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IKisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';
import { KisiselBilgilerService } from './kisisel-bilgiler.service';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-kisisel-bilgiler-update',
    templateUrl: './kisisel-bilgiler-update.component.html'
})
export class KisiselBilgilerUpdateComponent implements OnInit {
    kisiselBilgiler: IKisiselBilgiler;
    isSaving: boolean;

    users: IUser[];

    constructor(
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private kisiselBilgilerService: KisiselBilgilerService,
        private userService: UserService,
        private elementRef: ElementRef,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ kisiselBilgiler }) => {
            this.kisiselBilgiler = kisiselBilgiler;
        });
        this.userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.kisiselBilgiler, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.kisiselBilgiler.id !== undefined) {
            this.subscribeToSaveResponse(this.kisiselBilgilerService.update(this.kisiselBilgiler));
        } else {
            this.subscribeToSaveResponse(this.kisiselBilgilerService.create(this.kisiselBilgiler));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IKisiselBilgiler>>) {
        result.subscribe((res: HttpResponse<IKisiselBilgiler>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }
}
