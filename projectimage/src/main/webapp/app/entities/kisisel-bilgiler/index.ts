export * from './kisisel-bilgiler.service';
export * from './kisisel-bilgiler-update.component';
export * from './kisisel-bilgiler-delete-dialog.component';
export * from './kisisel-bilgiler-detail.component';
export * from './kisisel-bilgiler.component';
export * from './kisisel-bilgiler.route';
