import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { KisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';
import { KisiselBilgilerService } from './kisisel-bilgiler.service';
import { KisiselBilgilerComponent } from './kisisel-bilgiler.component';
import { KisiselBilgilerDetailComponent } from './kisisel-bilgiler-detail.component';
import { KisiselBilgilerUpdateComponent } from './kisisel-bilgiler-update.component';
import { KisiselBilgilerDeletePopupComponent } from './kisisel-bilgiler-delete-dialog.component';
import { IKisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';

@Injectable({ providedIn: 'root' })
export class KisiselBilgilerResolve implements Resolve<IKisiselBilgiler> {
    constructor(private service: KisiselBilgilerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<KisiselBilgiler> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<KisiselBilgiler>) => response.ok),
                map((kisiselBilgiler: HttpResponse<KisiselBilgiler>) => kisiselBilgiler.body)
            );
        }
        return of(new KisiselBilgiler());
    }
}

export const kisiselBilgilerRoute: Routes = [
    {
        path: 'kisisel-bilgiler',
        component: KisiselBilgilerComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'KisiselBilgilers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'kisisel-bilgiler/:id/view',
        component: KisiselBilgilerDetailComponent,
        resolve: {
            kisiselBilgiler: KisiselBilgilerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KisiselBilgilers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'kisisel-bilgiler/new',
        component: KisiselBilgilerUpdateComponent,
        resolve: {
            kisiselBilgiler: KisiselBilgilerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KisiselBilgilers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'kisisel-bilgiler/:id/edit',
        component: KisiselBilgilerUpdateComponent,
        resolve: {
            kisiselBilgiler: KisiselBilgilerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KisiselBilgilers'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const kisiselBilgilerPopupRoute: Routes = [
    {
        path: 'kisisel-bilgiler/:id/delete',
        component: KisiselBilgilerDeletePopupComponent,
        resolve: {
            kisiselBilgiler: KisiselBilgilerResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KisiselBilgilers'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
