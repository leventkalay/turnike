import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IKisiselBilgiler } from 'app/shared/model/kisisel-bilgiler.model';

@Component({
    selector: 'jhi-kisisel-bilgiler-detail',
    templateUrl: './kisisel-bilgiler-detail.component.html'
})
export class KisiselBilgilerDetailComponent implements OnInit {
    kisiselBilgiler: IKisiselBilgiler;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ kisiselBilgiler }) => {
            this.kisiselBilgiler = kisiselBilgiler;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
