import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import { ProjectimageAdminModule } from 'app/admin/admin.module';
import {
    KisiselBilgilerComponent,
    KisiselBilgilerDetailComponent,
    KisiselBilgilerUpdateComponent,
    KisiselBilgilerDeletePopupComponent,
    KisiselBilgilerDeleteDialogComponent,
    kisiselBilgilerRoute,
    kisiselBilgilerPopupRoute
} from './';

const ENTITY_STATES = [...kisiselBilgilerRoute, ...kisiselBilgilerPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, ProjectimageAdminModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        KisiselBilgilerComponent,
        KisiselBilgilerDetailComponent,
        KisiselBilgilerUpdateComponent,
        KisiselBilgilerDeleteDialogComponent,
        KisiselBilgilerDeletePopupComponent
    ],
    entryComponents: [
        KisiselBilgilerComponent,
        KisiselBilgilerUpdateComponent,
        KisiselBilgilerDeleteDialogComponent,
        KisiselBilgilerDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageKisiselBilgilerModule {}
