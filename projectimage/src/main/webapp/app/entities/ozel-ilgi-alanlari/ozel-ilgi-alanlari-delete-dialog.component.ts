import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';
import { OzelIlgiAlanlariService } from './ozel-ilgi-alanlari.service';

@Component({
    selector: 'jhi-ozel-ilgi-alanlari-delete-dialog',
    templateUrl: './ozel-ilgi-alanlari-delete-dialog.component.html'
})
export class OzelIlgiAlanlariDeleteDialogComponent {
    ozelIlgiAlanlari: IOzelIlgiAlanlari;

    constructor(
        private ozelIlgiAlanlariService: OzelIlgiAlanlariService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ozelIlgiAlanlariService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'ozelIlgiAlanlariListModification',
                content: 'Deleted an ozelIlgiAlanlari'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ozel-ilgi-alanlari-delete-popup',
    template: ''
})
export class OzelIlgiAlanlariDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ozelIlgiAlanlari }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OzelIlgiAlanlariDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.ozelIlgiAlanlari = ozelIlgiAlanlari;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
