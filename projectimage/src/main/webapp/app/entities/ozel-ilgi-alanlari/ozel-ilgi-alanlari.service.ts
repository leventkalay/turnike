import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';

type EntityResponseType = HttpResponse<IOzelIlgiAlanlari>;
type EntityArrayResponseType = HttpResponse<IOzelIlgiAlanlari[]>;

@Injectable({ providedIn: 'root' })
export class OzelIlgiAlanlariService {
    public resourceUrl = SERVER_API_URL + 'api/ozel-ilgi-alanlaris';

    constructor(private http: HttpClient) {}

    create(ozelIlgiAlanlari: IOzelIlgiAlanlari): Observable<EntityResponseType> {
        return this.http.post<IOzelIlgiAlanlari>(this.resourceUrl, ozelIlgiAlanlari, { observe: 'response' });
    }

    update(ozelIlgiAlanlari: IOzelIlgiAlanlari): Observable<EntityResponseType> {
        return this.http.put<IOzelIlgiAlanlari>(this.resourceUrl, ozelIlgiAlanlari, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IOzelIlgiAlanlari>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOzelIlgiAlanlari[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
