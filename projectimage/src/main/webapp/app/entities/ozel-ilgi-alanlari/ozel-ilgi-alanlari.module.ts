import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    OzelIlgiAlanlariComponent,
    OzelIlgiAlanlariDetailComponent,
    OzelIlgiAlanlariUpdateComponent,
    OzelIlgiAlanlariDeletePopupComponent,
    OzelIlgiAlanlariDeleteDialogComponent,
    ozelIlgiAlanlariRoute,
    ozelIlgiAlanlariPopupRoute
} from './';

const ENTITY_STATES = [...ozelIlgiAlanlariRoute, ...ozelIlgiAlanlariPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        OzelIlgiAlanlariComponent,
        OzelIlgiAlanlariDetailComponent,
        OzelIlgiAlanlariUpdateComponent,
        OzelIlgiAlanlariDeleteDialogComponent,
        OzelIlgiAlanlariDeletePopupComponent
    ],
    entryComponents: [
        OzelIlgiAlanlariComponent,
        OzelIlgiAlanlariUpdateComponent,
        OzelIlgiAlanlariDeleteDialogComponent,
        OzelIlgiAlanlariDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageOzelIlgiAlanlariModule {}
