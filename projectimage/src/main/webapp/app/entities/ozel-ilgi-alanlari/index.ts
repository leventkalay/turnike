export * from './ozel-ilgi-alanlari.service';
export * from './ozel-ilgi-alanlari-update.component';
export * from './ozel-ilgi-alanlari-delete-dialog.component';
export * from './ozel-ilgi-alanlari-detail.component';
export * from './ozel-ilgi-alanlari.component';
export * from './ozel-ilgi-alanlari.route';
