import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';

@Component({
    selector: 'jhi-ozel-ilgi-alanlari-detail',
    templateUrl: './ozel-ilgi-alanlari-detail.component.html'
})
export class OzelIlgiAlanlariDetailComponent implements OnInit {
    ozelIlgiAlanlari: IOzelIlgiAlanlari;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ozelIlgiAlanlari }) => {
            this.ozelIlgiAlanlari = ozelIlgiAlanlari;
        });
    }

    previousState() {
        window.history.back();
    }
}
