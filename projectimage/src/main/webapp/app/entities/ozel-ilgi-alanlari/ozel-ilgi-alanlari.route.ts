import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { OzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';
import { OzelIlgiAlanlariService } from './ozel-ilgi-alanlari.service';
import { OzelIlgiAlanlariComponent } from './ozel-ilgi-alanlari.component';
import { OzelIlgiAlanlariDetailComponent } from './ozel-ilgi-alanlari-detail.component';
import { OzelIlgiAlanlariUpdateComponent } from './ozel-ilgi-alanlari-update.component';
import { OzelIlgiAlanlariDeletePopupComponent } from './ozel-ilgi-alanlari-delete-dialog.component';
import { IOzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';

@Injectable({ providedIn: 'root' })
export class OzelIlgiAlanlariResolve implements Resolve<IOzelIlgiAlanlari> {
    constructor(private service: OzelIlgiAlanlariService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OzelIlgiAlanlari> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<OzelIlgiAlanlari>) => response.ok),
                map((ozelIlgiAlanlari: HttpResponse<OzelIlgiAlanlari>) => ozelIlgiAlanlari.body)
            );
        }
        return of(new OzelIlgiAlanlari());
    }
}

export const ozelIlgiAlanlariRoute: Routes = [
    {
        path: 'ozel-ilgi-alanlari',
        component: OzelIlgiAlanlariComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'OzelIlgiAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ozel-ilgi-alanlari/:id/view',
        component: OzelIlgiAlanlariDetailComponent,
        resolve: {
            ozelIlgiAlanlari: OzelIlgiAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OzelIlgiAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ozel-ilgi-alanlari/new',
        component: OzelIlgiAlanlariUpdateComponent,
        resolve: {
            ozelIlgiAlanlari: OzelIlgiAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OzelIlgiAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ozel-ilgi-alanlari/:id/edit',
        component: OzelIlgiAlanlariUpdateComponent,
        resolve: {
            ozelIlgiAlanlari: OzelIlgiAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OzelIlgiAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ozelIlgiAlanlariPopupRoute: Routes = [
    {
        path: 'ozel-ilgi-alanlari/:id/delete',
        component: OzelIlgiAlanlariDeletePopupComponent,
        resolve: {
            ozelIlgiAlanlari: OzelIlgiAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OzelIlgiAlanlaris'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
