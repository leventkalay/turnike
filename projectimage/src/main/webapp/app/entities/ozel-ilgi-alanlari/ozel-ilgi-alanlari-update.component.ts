import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IOzelIlgiAlanlari } from 'app/shared/model/ozel-ilgi-alanlari.model';
import { OzelIlgiAlanlariService } from './ozel-ilgi-alanlari.service';

@Component({
    selector: 'jhi-ozel-ilgi-alanlari-update',
    templateUrl: './ozel-ilgi-alanlari-update.component.html'
})
export class OzelIlgiAlanlariUpdateComponent implements OnInit {
    ozelIlgiAlanlari: IOzelIlgiAlanlari;
    isSaving: boolean;

    constructor(private ozelIlgiAlanlariService: OzelIlgiAlanlariService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ ozelIlgiAlanlari }) => {
            this.ozelIlgiAlanlari = ozelIlgiAlanlari;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.ozelIlgiAlanlari.id !== undefined) {
            this.subscribeToSaveResponse(this.ozelIlgiAlanlariService.update(this.ozelIlgiAlanlari));
        } else {
            this.subscribeToSaveResponse(this.ozelIlgiAlanlariService.create(this.ozelIlgiAlanlari));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IOzelIlgiAlanlari>>) {
        result.subscribe((res: HttpResponse<IOzelIlgiAlanlari>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
