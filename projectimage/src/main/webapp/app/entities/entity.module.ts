import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ProjectimageKisiselBilgilerModule } from './kisisel-bilgiler/kisisel-bilgiler.module';
import { ProjectimageIletisimBilgileriModule } from './iletisim-bilgileri/iletisim-bilgileri.module';
import { ProjectimageOgrenimBilgileriModule } from './ogrenim-bilgileri/ogrenim-bilgileri.module';
import { ProjectimageYabanciDilBilgileriModule } from './yabanci-dil-bilgileri/yabanci-dil-bilgileri.module';
import { ProjectimageIstectrubeleriModule } from './istectrubeleri/istectrubeleri.module';
import { ProjectimageUzmanlikAlanlariModule } from './uzmanlik-alanlari/uzmanlik-alanlari.module';
import { ProjectimageEgiticiYetkinlikleriModule } from './egitici-yetkinlikleri/egitici-yetkinlikleri.module';
import { ProjectimageYayinlarModule } from './yayinlar/yayinlar.module';
import { ProjectimageProjeModule } from './proje/proje.module';
import { ProjectimageKatilimSaglananProgramBilgisiModule } from './katilim-saglanan-program-bilgisi/katilim-saglanan-program-bilgisi.module';
import { ProjectimageOzelIlgiAlanlariModule } from './ozel-ilgi-alanlari/ozel-ilgi-alanlari.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        ProjectimageKisiselBilgilerModule,
        ProjectimageIletisimBilgileriModule,
        ProjectimageOgrenimBilgileriModule,
        ProjectimageYabanciDilBilgileriModule,
        ProjectimageIstectrubeleriModule,
        ProjectimageUzmanlikAlanlariModule,
        ProjectimageEgiticiYetkinlikleriModule,
        ProjectimageYayinlarModule,
        ProjectimageProjeModule,
        ProjectimageKatilimSaglananProgramBilgisiModule,
        ProjectimageOzelIlgiAlanlariModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageEntityModule {}
