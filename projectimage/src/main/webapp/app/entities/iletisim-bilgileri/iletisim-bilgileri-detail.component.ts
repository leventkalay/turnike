import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';

@Component({
    selector: 'jhi-iletisim-bilgileri-detail',
    templateUrl: './iletisim-bilgileri-detail.component.html'
})
export class IletisimBilgileriDetailComponent implements OnInit {
    iletisimBilgileri: IIletisimBilgileri;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ iletisimBilgileri }) => {
            this.iletisimBilgileri = iletisimBilgileri;
        });
    }

    previousState() {
        window.history.back();
    }
}
