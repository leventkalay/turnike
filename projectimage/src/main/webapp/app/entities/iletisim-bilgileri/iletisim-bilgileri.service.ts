import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IIletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';

type EntityResponseType = HttpResponse<IIletisimBilgileri>;
type EntityArrayResponseType = HttpResponse<IIletisimBilgileri[]>;

@Injectable({ providedIn: 'root' })
export class IletisimBilgileriService {
    public resourceUrl = SERVER_API_URL + 'api/iletisim-bilgileris';

    constructor(private http: HttpClient) {}

    create(iletisimBilgileri: IIletisimBilgileri): Observable<EntityResponseType> {
        return this.http.post<IIletisimBilgileri>(this.resourceUrl, iletisimBilgileri, { observe: 'response' });
    }

    update(iletisimBilgileri: IIletisimBilgileri): Observable<EntityResponseType> {
        return this.http.put<IIletisimBilgileri>(this.resourceUrl, iletisimBilgileri, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IIletisimBilgileri>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IIletisimBilgileri[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
