import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IIletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';
import { IletisimBilgileriService } from './iletisim-bilgileri.service';

@Component({
    selector: 'jhi-iletisim-bilgileri-delete-dialog',
    templateUrl: './iletisim-bilgileri-delete-dialog.component.html'
})
export class IletisimBilgileriDeleteDialogComponent {
    iletisimBilgileri: IIletisimBilgileri;

    constructor(
        private iletisimBilgileriService: IletisimBilgileriService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.iletisimBilgileriService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'iletisimBilgileriListModification',
                content: 'Deleted an iletisimBilgileri'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-iletisim-bilgileri-delete-popup',
    template: ''
})
export class IletisimBilgileriDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ iletisimBilgileri }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(IletisimBilgileriDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.iletisimBilgileri = iletisimBilgileri;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
