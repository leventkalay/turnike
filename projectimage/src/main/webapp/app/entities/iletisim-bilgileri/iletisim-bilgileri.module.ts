import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    IletisimBilgileriComponent,
    IletisimBilgileriDetailComponent,
    IletisimBilgileriUpdateComponent,
    IletisimBilgileriDeletePopupComponent,
    IletisimBilgileriDeleteDialogComponent,
    iletisimBilgileriRoute,
    iletisimBilgileriPopupRoute
} from './';

const ENTITY_STATES = [...iletisimBilgileriRoute, ...iletisimBilgileriPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        IletisimBilgileriComponent,
        IletisimBilgileriDetailComponent,
        IletisimBilgileriUpdateComponent,
        IletisimBilgileriDeleteDialogComponent,
        IletisimBilgileriDeletePopupComponent
    ],
    entryComponents: [
        IletisimBilgileriComponent,
        IletisimBilgileriUpdateComponent,
        IletisimBilgileriDeleteDialogComponent,
        IletisimBilgileriDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageIletisimBilgileriModule {}
