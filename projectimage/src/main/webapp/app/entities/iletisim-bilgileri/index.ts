export * from './iletisim-bilgileri.service';
export * from './iletisim-bilgileri-update.component';
export * from './iletisim-bilgileri-delete-dialog.component';
export * from './iletisim-bilgileri-detail.component';
export * from './iletisim-bilgileri.component';
export * from './iletisim-bilgileri.route';
