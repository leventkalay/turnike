import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IIletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';
import { IletisimBilgileriService } from './iletisim-bilgileri.service';

@Component({
    selector: 'jhi-iletisim-bilgileri-update',
    templateUrl: './iletisim-bilgileri-update.component.html'
})
export class IletisimBilgileriUpdateComponent implements OnInit {
    iletisimBilgileri: IIletisimBilgileri;
    isSaving: boolean;

    constructor(private iletisimBilgileriService: IletisimBilgileriService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ iletisimBilgileri }) => {
            this.iletisimBilgileri = iletisimBilgileri;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.iletisimBilgileri.id !== undefined) {
            this.subscribeToSaveResponse(this.iletisimBilgileriService.update(this.iletisimBilgileri));
        } else {
            this.subscribeToSaveResponse(this.iletisimBilgileriService.create(this.iletisimBilgileri));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IIletisimBilgileri>>) {
        result.subscribe((res: HttpResponse<IIletisimBilgileri>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
