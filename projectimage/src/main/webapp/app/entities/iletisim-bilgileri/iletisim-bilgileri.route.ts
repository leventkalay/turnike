import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';
import { IletisimBilgileriService } from './iletisim-bilgileri.service';
import { IletisimBilgileriComponent } from './iletisim-bilgileri.component';
import { IletisimBilgileriDetailComponent } from './iletisim-bilgileri-detail.component';
import { IletisimBilgileriUpdateComponent } from './iletisim-bilgileri-update.component';
import { IletisimBilgileriDeletePopupComponent } from './iletisim-bilgileri-delete-dialog.component';
import { IIletisimBilgileri } from 'app/shared/model/iletisim-bilgileri.model';

@Injectable({ providedIn: 'root' })
export class IletisimBilgileriResolve implements Resolve<IIletisimBilgileri> {
    constructor(private service: IletisimBilgileriService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IletisimBilgileri> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<IletisimBilgileri>) => response.ok),
                map((iletisimBilgileri: HttpResponse<IletisimBilgileri>) => iletisimBilgileri.body)
            );
        }
        return of(new IletisimBilgileri());
    }
}

export const iletisimBilgileriRoute: Routes = [
    {
        path: 'iletisim-bilgileri',
        component: IletisimBilgileriComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'IletisimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'iletisim-bilgileri/:id/view',
        component: IletisimBilgileriDetailComponent,
        resolve: {
            iletisimBilgileri: IletisimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'IletisimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'iletisim-bilgileri/new',
        component: IletisimBilgileriUpdateComponent,
        resolve: {
            iletisimBilgileri: IletisimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'IletisimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'iletisim-bilgileri/:id/edit',
        component: IletisimBilgileriUpdateComponent,
        resolve: {
            iletisimBilgileri: IletisimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'IletisimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const iletisimBilgileriPopupRoute: Routes = [
    {
        path: 'iletisim-bilgileri/:id/delete',
        component: IletisimBilgileriDeletePopupComponent,
        resolve: {
            iletisimBilgileri: IletisimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'IletisimBilgileris'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
