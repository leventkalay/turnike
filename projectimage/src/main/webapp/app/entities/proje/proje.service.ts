import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProje } from 'app/shared/model/proje.model';

type EntityResponseType = HttpResponse<IProje>;
type EntityArrayResponseType = HttpResponse<IProje[]>;

@Injectable({ providedIn: 'root' })
export class ProjeService {
    public resourceUrl = SERVER_API_URL + 'api/projes';

    constructor(private http: HttpClient) {}

    create(proje: IProje): Observable<EntityResponseType> {
        return this.http.post<IProje>(this.resourceUrl, proje, { observe: 'response' });
    }

    update(proje: IProje): Observable<EntityResponseType> {
        return this.http.put<IProje>(this.resourceUrl, proje, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IProje>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IProje[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
