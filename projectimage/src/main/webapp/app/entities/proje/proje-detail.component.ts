import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProje } from 'app/shared/model/proje.model';

@Component({
    selector: 'jhi-proje-detail',
    templateUrl: './proje-detail.component.html'
})
export class ProjeDetailComponent implements OnInit {
    proje: IProje;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ proje }) => {
            this.proje = proje;
        });
    }

    previousState() {
        window.history.back();
    }
}
