import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    ProjeComponent,
    ProjeDetailComponent,
    ProjeUpdateComponent,
    ProjeDeletePopupComponent,
    ProjeDeleteDialogComponent,
    projeRoute,
    projePopupRoute
} from './';

const ENTITY_STATES = [...projeRoute, ...projePopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [ProjeComponent, ProjeDetailComponent, ProjeUpdateComponent, ProjeDeleteDialogComponent, ProjeDeletePopupComponent],
    entryComponents: [ProjeComponent, ProjeUpdateComponent, ProjeDeleteDialogComponent, ProjeDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageProjeModule {}
