import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    IstectrubeleriComponent,
    IstectrubeleriDetailComponent,
    IstectrubeleriUpdateComponent,
    IstectrubeleriDeletePopupComponent,
    IstectrubeleriDeleteDialogComponent,
    istectrubeleriRoute,
    istectrubeleriPopupRoute
} from './';

const ENTITY_STATES = [...istectrubeleriRoute, ...istectrubeleriPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        IstectrubeleriComponent,
        IstectrubeleriDetailComponent,
        IstectrubeleriUpdateComponent,
        IstectrubeleriDeleteDialogComponent,
        IstectrubeleriDeletePopupComponent
    ],
    entryComponents: [
        IstectrubeleriComponent,
        IstectrubeleriUpdateComponent,
        IstectrubeleriDeleteDialogComponent,
        IstectrubeleriDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageIstectrubeleriModule {}
