import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IIstectrubeleri } from 'app/shared/model/istectrubeleri.model';
import { IstectrubeleriService } from './istectrubeleri.service';

@Component({
    selector: 'jhi-istectrubeleri-delete-dialog',
    templateUrl: './istectrubeleri-delete-dialog.component.html'
})
export class IstectrubeleriDeleteDialogComponent {
    istectrubeleri: IIstectrubeleri;

    constructor(
        private istectrubeleriService: IstectrubeleriService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.istectrubeleriService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'istectrubeleriListModification',
                content: 'Deleted an istectrubeleri'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-istectrubeleri-delete-popup',
    template: ''
})
export class IstectrubeleriDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ istectrubeleri }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(IstectrubeleriDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.istectrubeleri = istectrubeleri;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
