import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IIstectrubeleri } from 'app/shared/model/istectrubeleri.model';
import { IstectrubeleriService } from './istectrubeleri.service';

@Component({
    selector: 'jhi-istectrubeleri-update',
    templateUrl: './istectrubeleri-update.component.html'
})
export class IstectrubeleriUpdateComponent implements OnInit {
    istectrubeleri: IIstectrubeleri;
    isSaving: boolean;

    constructor(private istectrubeleriService: IstectrubeleriService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ istectrubeleri }) => {
            this.istectrubeleri = istectrubeleri;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.istectrubeleri.id !== undefined) {
            this.subscribeToSaveResponse(this.istectrubeleriService.update(this.istectrubeleri));
        } else {
            this.subscribeToSaveResponse(this.istectrubeleriService.create(this.istectrubeleri));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IIstectrubeleri>>) {
        result.subscribe((res: HttpResponse<IIstectrubeleri>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
