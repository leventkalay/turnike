import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIstectrubeleri } from 'app/shared/model/istectrubeleri.model';

@Component({
    selector: 'jhi-istectrubeleri-detail',
    templateUrl: './istectrubeleri-detail.component.html'
})
export class IstectrubeleriDetailComponent implements OnInit {
    istectrubeleri: IIstectrubeleri;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ istectrubeleri }) => {
            this.istectrubeleri = istectrubeleri;
        });
    }

    previousState() {
        window.history.back();
    }
}
