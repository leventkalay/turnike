export * from './istectrubeleri.service';
export * from './istectrubeleri-update.component';
export * from './istectrubeleri-delete-dialog.component';
export * from './istectrubeleri-detail.component';
export * from './istectrubeleri.component';
export * from './istectrubeleri.route';
