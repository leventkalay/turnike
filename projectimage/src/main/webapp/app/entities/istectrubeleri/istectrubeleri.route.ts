import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Istectrubeleri } from 'app/shared/model/istectrubeleri.model';
import { IstectrubeleriService } from './istectrubeleri.service';
import { IstectrubeleriComponent } from './istectrubeleri.component';
import { IstectrubeleriDetailComponent } from './istectrubeleri-detail.component';
import { IstectrubeleriUpdateComponent } from './istectrubeleri-update.component';
import { IstectrubeleriDeletePopupComponent } from './istectrubeleri-delete-dialog.component';
import { IIstectrubeleri } from 'app/shared/model/istectrubeleri.model';

@Injectable({ providedIn: 'root' })
export class IstectrubeleriResolve implements Resolve<IIstectrubeleri> {
    constructor(private service: IstectrubeleriService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Istectrubeleri> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Istectrubeleri>) => response.ok),
                map((istectrubeleri: HttpResponse<Istectrubeleri>) => istectrubeleri.body)
            );
        }
        return of(new Istectrubeleri());
    }
}

export const istectrubeleriRoute: Routes = [
    {
        path: 'istectrubeleri',
        component: IstectrubeleriComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'Istectrubeleris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'istectrubeleri/:id/view',
        component: IstectrubeleriDetailComponent,
        resolve: {
            istectrubeleri: IstectrubeleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Istectrubeleris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'istectrubeleri/new',
        component: IstectrubeleriUpdateComponent,
        resolve: {
            istectrubeleri: IstectrubeleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Istectrubeleris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'istectrubeleri/:id/edit',
        component: IstectrubeleriUpdateComponent,
        resolve: {
            istectrubeleri: IstectrubeleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Istectrubeleris'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const istectrubeleriPopupRoute: Routes = [
    {
        path: 'istectrubeleri/:id/delete',
        component: IstectrubeleriDeletePopupComponent,
        resolve: {
            istectrubeleri: IstectrubeleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Istectrubeleris'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
