import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';

type EntityResponseType = HttpResponse<IUzmanlikAlanlari>;
type EntityArrayResponseType = HttpResponse<IUzmanlikAlanlari[]>;

@Injectable({ providedIn: 'root' })
export class UzmanlikAlanlariService {
    public resourceUrl = SERVER_API_URL + 'api/uzmanlik-alanlaris';

    constructor(private http: HttpClient) {}

    create(uzmanlikAlanlari: IUzmanlikAlanlari): Observable<EntityResponseType> {
        return this.http.post<IUzmanlikAlanlari>(this.resourceUrl, uzmanlikAlanlari, { observe: 'response' });
    }

    update(uzmanlikAlanlari: IUzmanlikAlanlari): Observable<EntityResponseType> {
        return this.http.put<IUzmanlikAlanlari>(this.resourceUrl, uzmanlikAlanlari, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IUzmanlikAlanlari>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IUzmanlikAlanlari[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
