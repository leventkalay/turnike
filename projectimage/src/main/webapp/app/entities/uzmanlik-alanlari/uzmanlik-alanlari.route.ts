import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';
import { UzmanlikAlanlariService } from './uzmanlik-alanlari.service';
import { UzmanlikAlanlariComponent } from './uzmanlik-alanlari.component';
import { UzmanlikAlanlariDetailComponent } from './uzmanlik-alanlari-detail.component';
import { UzmanlikAlanlariUpdateComponent } from './uzmanlik-alanlari-update.component';
import { UzmanlikAlanlariDeletePopupComponent } from './uzmanlik-alanlari-delete-dialog.component';
import { IUzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';

@Injectable({ providedIn: 'root' })
export class UzmanlikAlanlariResolve implements Resolve<IUzmanlikAlanlari> {
    constructor(private service: UzmanlikAlanlariService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UzmanlikAlanlari> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<UzmanlikAlanlari>) => response.ok),
                map((uzmanlikAlanlari: HttpResponse<UzmanlikAlanlari>) => uzmanlikAlanlari.body)
            );
        }
        return of(new UzmanlikAlanlari());
    }
}

export const uzmanlikAlanlariRoute: Routes = [
    {
        path: 'uzmanlik-alanlari',
        component: UzmanlikAlanlariComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'UzmanlikAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'uzmanlik-alanlari/:id/view',
        component: UzmanlikAlanlariDetailComponent,
        resolve: {
            uzmanlikAlanlari: UzmanlikAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UzmanlikAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'uzmanlik-alanlari/new',
        component: UzmanlikAlanlariUpdateComponent,
        resolve: {
            uzmanlikAlanlari: UzmanlikAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UzmanlikAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'uzmanlik-alanlari/:id/edit',
        component: UzmanlikAlanlariUpdateComponent,
        resolve: {
            uzmanlikAlanlari: UzmanlikAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UzmanlikAlanlaris'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const uzmanlikAlanlariPopupRoute: Routes = [
    {
        path: 'uzmanlik-alanlari/:id/delete',
        component: UzmanlikAlanlariDeletePopupComponent,
        resolve: {
            uzmanlikAlanlari: UzmanlikAlanlariResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'UzmanlikAlanlaris'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
