import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    UzmanlikAlanlariComponent,
    UzmanlikAlanlariDetailComponent,
    UzmanlikAlanlariUpdateComponent,
    UzmanlikAlanlariDeletePopupComponent,
    UzmanlikAlanlariDeleteDialogComponent,
    uzmanlikAlanlariRoute,
    uzmanlikAlanlariPopupRoute
} from './';

const ENTITY_STATES = [...uzmanlikAlanlariRoute, ...uzmanlikAlanlariPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        UzmanlikAlanlariComponent,
        UzmanlikAlanlariDetailComponent,
        UzmanlikAlanlariUpdateComponent,
        UzmanlikAlanlariDeleteDialogComponent,
        UzmanlikAlanlariDeletePopupComponent
    ],
    entryComponents: [
        UzmanlikAlanlariComponent,
        UzmanlikAlanlariUpdateComponent,
        UzmanlikAlanlariDeleteDialogComponent,
        UzmanlikAlanlariDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageUzmanlikAlanlariModule {}
