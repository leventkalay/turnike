import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';
import { UzmanlikAlanlariService } from './uzmanlik-alanlari.service';

@Component({
    selector: 'jhi-uzmanlik-alanlari-delete-dialog',
    templateUrl: './uzmanlik-alanlari-delete-dialog.component.html'
})
export class UzmanlikAlanlariDeleteDialogComponent {
    uzmanlikAlanlari: IUzmanlikAlanlari;

    constructor(
        private uzmanlikAlanlariService: UzmanlikAlanlariService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.uzmanlikAlanlariService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'uzmanlikAlanlariListModification',
                content: 'Deleted an uzmanlikAlanlari'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-uzmanlik-alanlari-delete-popup',
    template: ''
})
export class UzmanlikAlanlariDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ uzmanlikAlanlari }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(UzmanlikAlanlariDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.uzmanlikAlanlari = uzmanlikAlanlari;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
