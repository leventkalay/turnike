import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';

@Component({
    selector: 'jhi-uzmanlik-alanlari-detail',
    templateUrl: './uzmanlik-alanlari-detail.component.html'
})
export class UzmanlikAlanlariDetailComponent implements OnInit {
    uzmanlikAlanlari: IUzmanlikAlanlari;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ uzmanlikAlanlari }) => {
            this.uzmanlikAlanlari = uzmanlikAlanlari;
        });
    }

    previousState() {
        window.history.back();
    }
}
