export * from './uzmanlik-alanlari.service';
export * from './uzmanlik-alanlari-update.component';
export * from './uzmanlik-alanlari-delete-dialog.component';
export * from './uzmanlik-alanlari-detail.component';
export * from './uzmanlik-alanlari.component';
export * from './uzmanlik-alanlari.route';
