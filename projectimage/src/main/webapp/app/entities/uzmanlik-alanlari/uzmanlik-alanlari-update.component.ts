import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IUzmanlikAlanlari } from 'app/shared/model/uzmanlik-alanlari.model';
import { UzmanlikAlanlariService } from './uzmanlik-alanlari.service';

@Component({
    selector: 'jhi-uzmanlik-alanlari-update',
    templateUrl: './uzmanlik-alanlari-update.component.html'
})
export class UzmanlikAlanlariUpdateComponent implements OnInit {
    uzmanlikAlanlari: IUzmanlikAlanlari;
    isSaving: boolean;

    constructor(private uzmanlikAlanlariService: UzmanlikAlanlariService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ uzmanlikAlanlari }) => {
            this.uzmanlikAlanlari = uzmanlikAlanlari;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.uzmanlikAlanlari.id !== undefined) {
            this.subscribeToSaveResponse(this.uzmanlikAlanlariService.update(this.uzmanlikAlanlari));
        } else {
            this.subscribeToSaveResponse(this.uzmanlikAlanlariService.create(this.uzmanlikAlanlari));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IUzmanlikAlanlari>>) {
        result.subscribe((res: HttpResponse<IUzmanlikAlanlari>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
