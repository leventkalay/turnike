import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { YabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';
import { YabanciDilBilgileriService } from './yabanci-dil-bilgileri.service';
import { YabanciDilBilgileriComponent } from './yabanci-dil-bilgileri.component';
import { YabanciDilBilgileriDetailComponent } from './yabanci-dil-bilgileri-detail.component';
import { YabanciDilBilgileriUpdateComponent } from './yabanci-dil-bilgileri-update.component';
import { YabanciDilBilgileriDeletePopupComponent } from './yabanci-dil-bilgileri-delete-dialog.component';
import { IYabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';

@Injectable({ providedIn: 'root' })
export class YabanciDilBilgileriResolve implements Resolve<IYabanciDilBilgileri> {
    constructor(private service: YabanciDilBilgileriService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<YabanciDilBilgileri> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<YabanciDilBilgileri>) => response.ok),
                map((yabanciDilBilgileri: HttpResponse<YabanciDilBilgileri>) => yabanciDilBilgileri.body)
            );
        }
        return of(new YabanciDilBilgileri());
    }
}

export const yabanciDilBilgileriRoute: Routes = [
    {
        path: 'yabanci-dil-bilgileri',
        component: YabanciDilBilgileriComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'YabanciDilBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'yabanci-dil-bilgileri/:id/view',
        component: YabanciDilBilgileriDetailComponent,
        resolve: {
            yabanciDilBilgileri: YabanciDilBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'YabanciDilBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'yabanci-dil-bilgileri/new',
        component: YabanciDilBilgileriUpdateComponent,
        resolve: {
            yabanciDilBilgileri: YabanciDilBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'YabanciDilBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'yabanci-dil-bilgileri/:id/edit',
        component: YabanciDilBilgileriUpdateComponent,
        resolve: {
            yabanciDilBilgileri: YabanciDilBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'YabanciDilBilgileris'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const yabanciDilBilgileriPopupRoute: Routes = [
    {
        path: 'yabanci-dil-bilgileri/:id/delete',
        component: YabanciDilBilgileriDeletePopupComponent,
        resolve: {
            yabanciDilBilgileri: YabanciDilBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'YabanciDilBilgileris'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
