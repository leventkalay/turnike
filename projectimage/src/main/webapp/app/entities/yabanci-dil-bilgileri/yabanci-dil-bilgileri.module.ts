import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    YabanciDilBilgileriComponent,
    YabanciDilBilgileriDetailComponent,
    YabanciDilBilgileriUpdateComponent,
    YabanciDilBilgileriDeletePopupComponent,
    YabanciDilBilgileriDeleteDialogComponent,
    yabanciDilBilgileriRoute,
    yabanciDilBilgileriPopupRoute
} from './';

const ENTITY_STATES = [...yabanciDilBilgileriRoute, ...yabanciDilBilgileriPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        YabanciDilBilgileriComponent,
        YabanciDilBilgileriDetailComponent,
        YabanciDilBilgileriUpdateComponent,
        YabanciDilBilgileriDeleteDialogComponent,
        YabanciDilBilgileriDeletePopupComponent
    ],
    entryComponents: [
        YabanciDilBilgileriComponent,
        YabanciDilBilgileriUpdateComponent,
        YabanciDilBilgileriDeleteDialogComponent,
        YabanciDilBilgileriDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageYabanciDilBilgileriModule {}
