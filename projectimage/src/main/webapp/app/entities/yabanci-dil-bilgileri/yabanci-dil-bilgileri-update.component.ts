import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { IYabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';
import { YabanciDilBilgileriService } from './yabanci-dil-bilgileri.service';

@Component({
    selector: 'jhi-yabanci-dil-bilgileri-update',
    templateUrl: './yabanci-dil-bilgileri-update.component.html'
})
export class YabanciDilBilgileriUpdateComponent implements OnInit {
    yabanciDilBilgileri: IYabanciDilBilgileri;
    isSaving: boolean;
    belgeTarihiDp: any;

    constructor(private yabanciDilBilgileriService: YabanciDilBilgileriService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ yabanciDilBilgileri }) => {
            this.yabanciDilBilgileri = yabanciDilBilgileri;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.yabanciDilBilgileri.id !== undefined) {
            this.subscribeToSaveResponse(this.yabanciDilBilgileriService.update(this.yabanciDilBilgileri));
        } else {
            this.subscribeToSaveResponse(this.yabanciDilBilgileriService.create(this.yabanciDilBilgileri));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IYabanciDilBilgileri>>) {
        result.subscribe((res: HttpResponse<IYabanciDilBilgileri>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
