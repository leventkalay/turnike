export * from './yabanci-dil-bilgileri.service';
export * from './yabanci-dil-bilgileri-update.component';
export * from './yabanci-dil-bilgileri-delete-dialog.component';
export * from './yabanci-dil-bilgileri-detail.component';
export * from './yabanci-dil-bilgileri.component';
export * from './yabanci-dil-bilgileri.route';
