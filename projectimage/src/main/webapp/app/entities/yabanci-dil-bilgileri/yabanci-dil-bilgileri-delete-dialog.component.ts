import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IYabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';
import { YabanciDilBilgileriService } from './yabanci-dil-bilgileri.service';

@Component({
    selector: 'jhi-yabanci-dil-bilgileri-delete-dialog',
    templateUrl: './yabanci-dil-bilgileri-delete-dialog.component.html'
})
export class YabanciDilBilgileriDeleteDialogComponent {
    yabanciDilBilgileri: IYabanciDilBilgileri;

    constructor(
        private yabanciDilBilgileriService: YabanciDilBilgileriService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.yabanciDilBilgileriService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'yabanciDilBilgileriListModification',
                content: 'Deleted an yabanciDilBilgileri'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-yabanci-dil-bilgileri-delete-popup',
    template: ''
})
export class YabanciDilBilgileriDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ yabanciDilBilgileri }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(YabanciDilBilgileriDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.yabanciDilBilgileri = yabanciDilBilgileri;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
