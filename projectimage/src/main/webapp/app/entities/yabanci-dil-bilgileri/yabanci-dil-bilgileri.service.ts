import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IYabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';

type EntityResponseType = HttpResponse<IYabanciDilBilgileri>;
type EntityArrayResponseType = HttpResponse<IYabanciDilBilgileri[]>;

@Injectable({ providedIn: 'root' })
export class YabanciDilBilgileriService {
    public resourceUrl = SERVER_API_URL + 'api/yabanci-dil-bilgileris';

    constructor(private http: HttpClient) {}

    create(yabanciDilBilgileri: IYabanciDilBilgileri): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(yabanciDilBilgileri);
        return this.http
            .post<IYabanciDilBilgileri>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(yabanciDilBilgileri: IYabanciDilBilgileri): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(yabanciDilBilgileri);
        return this.http
            .put<IYabanciDilBilgileri>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IYabanciDilBilgileri>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IYabanciDilBilgileri[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(yabanciDilBilgileri: IYabanciDilBilgileri): IYabanciDilBilgileri {
        const copy: IYabanciDilBilgileri = Object.assign({}, yabanciDilBilgileri, {
            belgeTarihi:
                yabanciDilBilgileri.belgeTarihi != null && yabanciDilBilgileri.belgeTarihi.isValid()
                    ? yabanciDilBilgileri.belgeTarihi.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.belgeTarihi = res.body.belgeTarihi != null ? moment(res.body.belgeTarihi) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((yabanciDilBilgileri: IYabanciDilBilgileri) => {
                yabanciDilBilgileri.belgeTarihi = yabanciDilBilgileri.belgeTarihi != null ? moment(yabanciDilBilgileri.belgeTarihi) : null;
            });
        }
        return res;
    }
}
