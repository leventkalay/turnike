import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IYabanciDilBilgileri } from 'app/shared/model/yabanci-dil-bilgileri.model';

@Component({
    selector: 'jhi-yabanci-dil-bilgileri-detail',
    templateUrl: './yabanci-dil-bilgileri-detail.component.html'
})
export class YabanciDilBilgileriDetailComponent implements OnInit {
    yabanciDilBilgileri: IYabanciDilBilgileri;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ yabanciDilBilgileri }) => {
            this.yabanciDilBilgileri = yabanciDilBilgileri;
        });
    }

    previousState() {
        window.history.back();
    }
}
