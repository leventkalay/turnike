import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';

type EntityResponseType = HttpResponse<IEgiticiYetkinlikleri>;
type EntityArrayResponseType = HttpResponse<IEgiticiYetkinlikleri[]>;

@Injectable({ providedIn: 'root' })
export class EgiticiYetkinlikleriService {
    public resourceUrl = SERVER_API_URL + 'api/egitici-yetkinlikleris';

    constructor(private http: HttpClient) {}

    create(egiticiYetkinlikleri: IEgiticiYetkinlikleri): Observable<EntityResponseType> {
        return this.http.post<IEgiticiYetkinlikleri>(this.resourceUrl, egiticiYetkinlikleri, { observe: 'response' });
    }

    update(egiticiYetkinlikleri: IEgiticiYetkinlikleri): Observable<EntityResponseType> {
        return this.http.put<IEgiticiYetkinlikleri>(this.resourceUrl, egiticiYetkinlikleri, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IEgiticiYetkinlikleri>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IEgiticiYetkinlikleri[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
