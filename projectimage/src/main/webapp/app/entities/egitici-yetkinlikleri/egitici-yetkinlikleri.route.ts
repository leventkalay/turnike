import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { EgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';
import { EgiticiYetkinlikleriService } from './egitici-yetkinlikleri.service';
import { EgiticiYetkinlikleriComponent } from './egitici-yetkinlikleri.component';
import { EgiticiYetkinlikleriDetailComponent } from './egitici-yetkinlikleri-detail.component';
import { EgiticiYetkinlikleriUpdateComponent } from './egitici-yetkinlikleri-update.component';
import { EgiticiYetkinlikleriDeletePopupComponent } from './egitici-yetkinlikleri-delete-dialog.component';
import { IEgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';

@Injectable({ providedIn: 'root' })
export class EgiticiYetkinlikleriResolve implements Resolve<IEgiticiYetkinlikleri> {
    constructor(private service: EgiticiYetkinlikleriService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<EgiticiYetkinlikleri> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<EgiticiYetkinlikleri>) => response.ok),
                map((egiticiYetkinlikleri: HttpResponse<EgiticiYetkinlikleri>) => egiticiYetkinlikleri.body)
            );
        }
        return of(new EgiticiYetkinlikleri());
    }
}

export const egiticiYetkinlikleriRoute: Routes = [
    {
        path: 'egitici-yetkinlikleri',
        component: EgiticiYetkinlikleriComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'EgiticiYetkinlikleris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'egitici-yetkinlikleri/:id/view',
        component: EgiticiYetkinlikleriDetailComponent,
        resolve: {
            egiticiYetkinlikleri: EgiticiYetkinlikleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EgiticiYetkinlikleris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'egitici-yetkinlikleri/new',
        component: EgiticiYetkinlikleriUpdateComponent,
        resolve: {
            egiticiYetkinlikleri: EgiticiYetkinlikleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EgiticiYetkinlikleris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'egitici-yetkinlikleri/:id/edit',
        component: EgiticiYetkinlikleriUpdateComponent,
        resolve: {
            egiticiYetkinlikleri: EgiticiYetkinlikleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EgiticiYetkinlikleris'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const egiticiYetkinlikleriPopupRoute: Routes = [
    {
        path: 'egitici-yetkinlikleri/:id/delete',
        component: EgiticiYetkinlikleriDeletePopupComponent,
        resolve: {
            egiticiYetkinlikleri: EgiticiYetkinlikleriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EgiticiYetkinlikleris'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
