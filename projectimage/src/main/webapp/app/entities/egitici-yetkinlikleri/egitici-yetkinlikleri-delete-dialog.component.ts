import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';
import { EgiticiYetkinlikleriService } from './egitici-yetkinlikleri.service';

@Component({
    selector: 'jhi-egitici-yetkinlikleri-delete-dialog',
    templateUrl: './egitici-yetkinlikleri-delete-dialog.component.html'
})
export class EgiticiYetkinlikleriDeleteDialogComponent {
    egiticiYetkinlikleri: IEgiticiYetkinlikleri;

    constructor(
        private egiticiYetkinlikleriService: EgiticiYetkinlikleriService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.egiticiYetkinlikleriService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'egiticiYetkinlikleriListModification',
                content: 'Deleted an egiticiYetkinlikleri'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-egitici-yetkinlikleri-delete-popup',
    template: ''
})
export class EgiticiYetkinlikleriDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ egiticiYetkinlikleri }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(EgiticiYetkinlikleriDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.egiticiYetkinlikleri = egiticiYetkinlikleri;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
