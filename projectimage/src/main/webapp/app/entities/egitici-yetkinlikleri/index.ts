export * from './egitici-yetkinlikleri.service';
export * from './egitici-yetkinlikleri-update.component';
export * from './egitici-yetkinlikleri-delete-dialog.component';
export * from './egitici-yetkinlikleri-detail.component';
export * from './egitici-yetkinlikleri.component';
export * from './egitici-yetkinlikleri.route';
