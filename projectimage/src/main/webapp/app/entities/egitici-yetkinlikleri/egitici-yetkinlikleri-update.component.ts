import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IEgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';
import { EgiticiYetkinlikleriService } from './egitici-yetkinlikleri.service';

@Component({
    selector: 'jhi-egitici-yetkinlikleri-update',
    templateUrl: './egitici-yetkinlikleri-update.component.html'
})
export class EgiticiYetkinlikleriUpdateComponent implements OnInit {
    egiticiYetkinlikleri: IEgiticiYetkinlikleri;
    isSaving: boolean;

    constructor(private egiticiYetkinlikleriService: EgiticiYetkinlikleriService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ egiticiYetkinlikleri }) => {
            this.egiticiYetkinlikleri = egiticiYetkinlikleri;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.egiticiYetkinlikleri.id !== undefined) {
            this.subscribeToSaveResponse(this.egiticiYetkinlikleriService.update(this.egiticiYetkinlikleri));
        } else {
            this.subscribeToSaveResponse(this.egiticiYetkinlikleriService.create(this.egiticiYetkinlikleri));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IEgiticiYetkinlikleri>>) {
        result.subscribe(
            (res: HttpResponse<IEgiticiYetkinlikleri>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
