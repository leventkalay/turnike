import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    EgiticiYetkinlikleriComponent,
    EgiticiYetkinlikleriDetailComponent,
    EgiticiYetkinlikleriUpdateComponent,
    EgiticiYetkinlikleriDeletePopupComponent,
    EgiticiYetkinlikleriDeleteDialogComponent,
    egiticiYetkinlikleriRoute,
    egiticiYetkinlikleriPopupRoute
} from './';

const ENTITY_STATES = [...egiticiYetkinlikleriRoute, ...egiticiYetkinlikleriPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        EgiticiYetkinlikleriComponent,
        EgiticiYetkinlikleriDetailComponent,
        EgiticiYetkinlikleriUpdateComponent,
        EgiticiYetkinlikleriDeleteDialogComponent,
        EgiticiYetkinlikleriDeletePopupComponent
    ],
    entryComponents: [
        EgiticiYetkinlikleriComponent,
        EgiticiYetkinlikleriUpdateComponent,
        EgiticiYetkinlikleriDeleteDialogComponent,
        EgiticiYetkinlikleriDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageEgiticiYetkinlikleriModule {}
