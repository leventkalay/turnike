import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEgiticiYetkinlikleri } from 'app/shared/model/egitici-yetkinlikleri.model';

@Component({
    selector: 'jhi-egitici-yetkinlikleri-detail',
    templateUrl: './egitici-yetkinlikleri-detail.component.html'
})
export class EgiticiYetkinlikleriDetailComponent implements OnInit {
    egiticiYetkinlikleri: IEgiticiYetkinlikleri;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ egiticiYetkinlikleri }) => {
            this.egiticiYetkinlikleri = egiticiYetkinlikleri;
        });
    }

    previousState() {
        window.history.back();
    }
}
