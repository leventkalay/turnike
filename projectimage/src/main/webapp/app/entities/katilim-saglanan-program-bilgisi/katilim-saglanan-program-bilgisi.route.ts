import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { KatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';
import { KatilimSaglananProgramBilgisiService } from './katilim-saglanan-program-bilgisi.service';
import { KatilimSaglananProgramBilgisiComponent } from './katilim-saglanan-program-bilgisi.component';
import { KatilimSaglananProgramBilgisiDetailComponent } from './katilim-saglanan-program-bilgisi-detail.component';
import { KatilimSaglananProgramBilgisiUpdateComponent } from './katilim-saglanan-program-bilgisi-update.component';
import { KatilimSaglananProgramBilgisiDeletePopupComponent } from './katilim-saglanan-program-bilgisi-delete-dialog.component';
import { IKatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';

@Injectable({ providedIn: 'root' })
export class KatilimSaglananProgramBilgisiResolve implements Resolve<IKatilimSaglananProgramBilgisi> {
    constructor(private service: KatilimSaglananProgramBilgisiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<KatilimSaglananProgramBilgisi> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<KatilimSaglananProgramBilgisi>) => response.ok),
                map((katilimSaglananProgramBilgisi: HttpResponse<KatilimSaglananProgramBilgisi>) => katilimSaglananProgramBilgisi.body)
            );
        }
        return of(new KatilimSaglananProgramBilgisi());
    }
}

export const katilimSaglananProgramBilgisiRoute: Routes = [
    {
        path: 'katilim-saglanan-program-bilgisi',
        component: KatilimSaglananProgramBilgisiComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'KatilimSaglananProgramBilgisis'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'katilim-saglanan-program-bilgisi/:id/view',
        component: KatilimSaglananProgramBilgisiDetailComponent,
        resolve: {
            katilimSaglananProgramBilgisi: KatilimSaglananProgramBilgisiResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KatilimSaglananProgramBilgisis'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'katilim-saglanan-program-bilgisi/new',
        component: KatilimSaglananProgramBilgisiUpdateComponent,
        resolve: {
            katilimSaglananProgramBilgisi: KatilimSaglananProgramBilgisiResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KatilimSaglananProgramBilgisis'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'katilim-saglanan-program-bilgisi/:id/edit',
        component: KatilimSaglananProgramBilgisiUpdateComponent,
        resolve: {
            katilimSaglananProgramBilgisi: KatilimSaglananProgramBilgisiResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KatilimSaglananProgramBilgisis'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const katilimSaglananProgramBilgisiPopupRoute: Routes = [
    {
        path: 'katilim-saglanan-program-bilgisi/:id/delete',
        component: KatilimSaglananProgramBilgisiDeletePopupComponent,
        resolve: {
            katilimSaglananProgramBilgisi: KatilimSaglananProgramBilgisiResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'KatilimSaglananProgramBilgisis'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
