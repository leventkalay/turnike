import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    KatilimSaglananProgramBilgisiComponent,
    KatilimSaglananProgramBilgisiDetailComponent,
    KatilimSaglananProgramBilgisiUpdateComponent,
    KatilimSaglananProgramBilgisiDeletePopupComponent,
    KatilimSaglananProgramBilgisiDeleteDialogComponent,
    katilimSaglananProgramBilgisiRoute,
    katilimSaglananProgramBilgisiPopupRoute
} from './';

const ENTITY_STATES = [...katilimSaglananProgramBilgisiRoute, ...katilimSaglananProgramBilgisiPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        KatilimSaglananProgramBilgisiComponent,
        KatilimSaglananProgramBilgisiDetailComponent,
        KatilimSaglananProgramBilgisiUpdateComponent,
        KatilimSaglananProgramBilgisiDeleteDialogComponent,
        KatilimSaglananProgramBilgisiDeletePopupComponent
    ],
    entryComponents: [
        KatilimSaglananProgramBilgisiComponent,
        KatilimSaglananProgramBilgisiUpdateComponent,
        KatilimSaglananProgramBilgisiDeleteDialogComponent,
        KatilimSaglananProgramBilgisiDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageKatilimSaglananProgramBilgisiModule {}
