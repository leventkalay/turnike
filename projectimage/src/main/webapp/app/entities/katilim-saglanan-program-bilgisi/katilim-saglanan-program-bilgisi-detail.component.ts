import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';

@Component({
    selector: 'jhi-katilim-saglanan-program-bilgisi-detail',
    templateUrl: './katilim-saglanan-program-bilgisi-detail.component.html'
})
export class KatilimSaglananProgramBilgisiDetailComponent implements OnInit {
    katilimSaglananProgramBilgisi: IKatilimSaglananProgramBilgisi;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ katilimSaglananProgramBilgisi }) => {
            this.katilimSaglananProgramBilgisi = katilimSaglananProgramBilgisi;
        });
    }

    previousState() {
        window.history.back();
    }
}
