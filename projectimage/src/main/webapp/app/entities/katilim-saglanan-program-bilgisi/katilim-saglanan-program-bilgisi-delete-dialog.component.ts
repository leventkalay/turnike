import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IKatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';
import { KatilimSaglananProgramBilgisiService } from './katilim-saglanan-program-bilgisi.service';

@Component({
    selector: 'jhi-katilim-saglanan-program-bilgisi-delete-dialog',
    templateUrl: './katilim-saglanan-program-bilgisi-delete-dialog.component.html'
})
export class KatilimSaglananProgramBilgisiDeleteDialogComponent {
    katilimSaglananProgramBilgisi: IKatilimSaglananProgramBilgisi;

    constructor(
        private katilimSaglananProgramBilgisiService: KatilimSaglananProgramBilgisiService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.katilimSaglananProgramBilgisiService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'katilimSaglananProgramBilgisiListModification',
                content: 'Deleted an katilimSaglananProgramBilgisi'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-katilim-saglanan-program-bilgisi-delete-popup',
    template: ''
})
export class KatilimSaglananProgramBilgisiDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ katilimSaglananProgramBilgisi }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(KatilimSaglananProgramBilgisiDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.katilimSaglananProgramBilgisi = katilimSaglananProgramBilgisi;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
