import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IKatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';
import { KatilimSaglananProgramBilgisiService } from './katilim-saglanan-program-bilgisi.service';

@Component({
    selector: 'jhi-katilim-saglanan-program-bilgisi-update',
    templateUrl: './katilim-saglanan-program-bilgisi-update.component.html'
})
export class KatilimSaglananProgramBilgisiUpdateComponent implements OnInit {
    katilimSaglananProgramBilgisi: IKatilimSaglananProgramBilgisi;
    isSaving: boolean;

    constructor(
        private katilimSaglananProgramBilgisiService: KatilimSaglananProgramBilgisiService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ katilimSaglananProgramBilgisi }) => {
            this.katilimSaglananProgramBilgisi = katilimSaglananProgramBilgisi;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.katilimSaglananProgramBilgisi.id !== undefined) {
            this.subscribeToSaveResponse(this.katilimSaglananProgramBilgisiService.update(this.katilimSaglananProgramBilgisi));
        } else {
            this.subscribeToSaveResponse(this.katilimSaglananProgramBilgisiService.create(this.katilimSaglananProgramBilgisi));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IKatilimSaglananProgramBilgisi>>) {
        result.subscribe(
            (res: HttpResponse<IKatilimSaglananProgramBilgisi>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
