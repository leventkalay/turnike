import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IKatilimSaglananProgramBilgisi } from 'app/shared/model/katilim-saglanan-program-bilgisi.model';

type EntityResponseType = HttpResponse<IKatilimSaglananProgramBilgisi>;
type EntityArrayResponseType = HttpResponse<IKatilimSaglananProgramBilgisi[]>;

@Injectable({ providedIn: 'root' })
export class KatilimSaglananProgramBilgisiService {
    public resourceUrl = SERVER_API_URL + 'api/katilim-saglanan-program-bilgisis';

    constructor(private http: HttpClient) {}

    create(katilimSaglananProgramBilgisi: IKatilimSaglananProgramBilgisi): Observable<EntityResponseType> {
        return this.http.post<IKatilimSaglananProgramBilgisi>(this.resourceUrl, katilimSaglananProgramBilgisi, { observe: 'response' });
    }

    update(katilimSaglananProgramBilgisi: IKatilimSaglananProgramBilgisi): Observable<EntityResponseType> {
        return this.http.put<IKatilimSaglananProgramBilgisi>(this.resourceUrl, katilimSaglananProgramBilgisi, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IKatilimSaglananProgramBilgisi>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IKatilimSaglananProgramBilgisi[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
