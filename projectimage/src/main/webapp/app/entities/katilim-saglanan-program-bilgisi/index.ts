export * from './katilim-saglanan-program-bilgisi.service';
export * from './katilim-saglanan-program-bilgisi-update.component';
export * from './katilim-saglanan-program-bilgisi-delete-dialog.component';
export * from './katilim-saglanan-program-bilgisi-detail.component';
export * from './katilim-saglanan-program-bilgisi.component';
export * from './katilim-saglanan-program-bilgisi.route';
