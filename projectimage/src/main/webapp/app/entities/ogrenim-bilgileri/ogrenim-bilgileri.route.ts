import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { OgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';
import { OgrenimBilgileriService } from './ogrenim-bilgileri.service';
import { OgrenimBilgileriComponent } from './ogrenim-bilgileri.component';
import { OgrenimBilgileriDetailComponent } from './ogrenim-bilgileri-detail.component';
import { OgrenimBilgileriUpdateComponent } from './ogrenim-bilgileri-update.component';
import { OgrenimBilgileriDeletePopupComponent } from './ogrenim-bilgileri-delete-dialog.component';
import { IOgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';

@Injectable({ providedIn: 'root' })
export class OgrenimBilgileriResolve implements Resolve<IOgrenimBilgileri> {
    constructor(private service: OgrenimBilgileriService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OgrenimBilgileri> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<OgrenimBilgileri>) => response.ok),
                map((ogrenimBilgileri: HttpResponse<OgrenimBilgileri>) => ogrenimBilgileri.body)
            );
        }
        return of(new OgrenimBilgileri());
    }
}

export const ogrenimBilgileriRoute: Routes = [
    {
        path: 'ogrenim-bilgileri',
        component: OgrenimBilgileriComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'OgrenimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ogrenim-bilgileri/:id/view',
        component: OgrenimBilgileriDetailComponent,
        resolve: {
            ogrenimBilgileri: OgrenimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OgrenimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ogrenim-bilgileri/new',
        component: OgrenimBilgileriUpdateComponent,
        resolve: {
            ogrenimBilgileri: OgrenimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OgrenimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ogrenim-bilgileri/:id/edit',
        component: OgrenimBilgileriUpdateComponent,
        resolve: {
            ogrenimBilgileri: OgrenimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OgrenimBilgileris'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ogrenimBilgileriPopupRoute: Routes = [
    {
        path: 'ogrenim-bilgileri/:id/delete',
        component: OgrenimBilgileriDeletePopupComponent,
        resolve: {
            ogrenimBilgileri: OgrenimBilgileriResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'OgrenimBilgileris'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
