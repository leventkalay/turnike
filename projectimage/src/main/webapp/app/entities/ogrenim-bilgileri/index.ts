export * from './ogrenim-bilgileri.service';
export * from './ogrenim-bilgileri-update.component';
export * from './ogrenim-bilgileri-delete-dialog.component';
export * from './ogrenim-bilgileri-detail.component';
export * from './ogrenim-bilgileri.component';
export * from './ogrenim-bilgileri.route';
