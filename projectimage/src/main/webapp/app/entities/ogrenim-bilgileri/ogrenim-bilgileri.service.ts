import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';

type EntityResponseType = HttpResponse<IOgrenimBilgileri>;
type EntityArrayResponseType = HttpResponse<IOgrenimBilgileri[]>;

@Injectable({ providedIn: 'root' })
export class OgrenimBilgileriService {
    public resourceUrl = SERVER_API_URL + 'api/ogrenim-bilgileris';

    constructor(private http: HttpClient) {}

    create(ogrenimBilgileri: IOgrenimBilgileri): Observable<EntityResponseType> {
        return this.http.post<IOgrenimBilgileri>(this.resourceUrl, ogrenimBilgileri, { observe: 'response' });
    }

    update(ogrenimBilgileri: IOgrenimBilgileri): Observable<EntityResponseType> {
        return this.http.put<IOgrenimBilgileri>(this.resourceUrl, ogrenimBilgileri, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IOgrenimBilgileri>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOgrenimBilgileri[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
