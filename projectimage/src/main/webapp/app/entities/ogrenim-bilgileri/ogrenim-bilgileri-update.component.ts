import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IOgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';
import { OgrenimBilgileriService } from './ogrenim-bilgileri.service';

@Component({
    selector: 'jhi-ogrenim-bilgileri-update',
    templateUrl: './ogrenim-bilgileri-update.component.html'
})
export class OgrenimBilgileriUpdateComponent implements OnInit {
    ogrenimBilgileri: IOgrenimBilgileri;
    isSaving: boolean;

    constructor(private ogrenimBilgileriService: OgrenimBilgileriService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ ogrenimBilgileri }) => {
            this.ogrenimBilgileri = ogrenimBilgileri;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.ogrenimBilgileri.id !== undefined) {
            this.subscribeToSaveResponse(this.ogrenimBilgileriService.update(this.ogrenimBilgileri));
        } else {
            this.subscribeToSaveResponse(this.ogrenimBilgileriService.create(this.ogrenimBilgileri));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IOgrenimBilgileri>>) {
        result.subscribe((res: HttpResponse<IOgrenimBilgileri>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
