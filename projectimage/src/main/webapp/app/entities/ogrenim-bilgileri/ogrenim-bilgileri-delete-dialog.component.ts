import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';
import { OgrenimBilgileriService } from './ogrenim-bilgileri.service';

@Component({
    selector: 'jhi-ogrenim-bilgileri-delete-dialog',
    templateUrl: './ogrenim-bilgileri-delete-dialog.component.html'
})
export class OgrenimBilgileriDeleteDialogComponent {
    ogrenimBilgileri: IOgrenimBilgileri;

    constructor(
        private ogrenimBilgileriService: OgrenimBilgileriService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ogrenimBilgileriService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'ogrenimBilgileriListModification',
                content: 'Deleted an ogrenimBilgileri'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ogrenim-bilgileri-delete-popup',
    template: ''
})
export class OgrenimBilgileriDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ogrenimBilgileri }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OgrenimBilgileriDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.ogrenimBilgileri = ogrenimBilgileri;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
