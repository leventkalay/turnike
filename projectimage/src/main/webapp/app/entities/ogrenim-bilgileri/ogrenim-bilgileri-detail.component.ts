import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOgrenimBilgileri } from 'app/shared/model/ogrenim-bilgileri.model';

@Component({
    selector: 'jhi-ogrenim-bilgileri-detail',
    templateUrl: './ogrenim-bilgileri-detail.component.html'
})
export class OgrenimBilgileriDetailComponent implements OnInit {
    ogrenimBilgileri: IOgrenimBilgileri;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ ogrenimBilgileri }) => {
            this.ogrenimBilgileri = ogrenimBilgileri;
        });
    }

    previousState() {
        window.history.back();
    }
}
