import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    OgrenimBilgileriComponent,
    OgrenimBilgileriDetailComponent,
    OgrenimBilgileriUpdateComponent,
    OgrenimBilgileriDeletePopupComponent,
    OgrenimBilgileriDeleteDialogComponent,
    ogrenimBilgileriRoute,
    ogrenimBilgileriPopupRoute
} from './';

const ENTITY_STATES = [...ogrenimBilgileriRoute, ...ogrenimBilgileriPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        OgrenimBilgileriComponent,
        OgrenimBilgileriDetailComponent,
        OgrenimBilgileriUpdateComponent,
        OgrenimBilgileriDeleteDialogComponent,
        OgrenimBilgileriDeletePopupComponent
    ],
    entryComponents: [
        OgrenimBilgileriComponent,
        OgrenimBilgileriUpdateComponent,
        OgrenimBilgileriDeleteDialogComponent,
        OgrenimBilgileriDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageOgrenimBilgileriModule {}
