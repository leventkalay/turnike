import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IYayinlar } from 'app/shared/model/yayinlar.model';

type EntityResponseType = HttpResponse<IYayinlar>;
type EntityArrayResponseType = HttpResponse<IYayinlar[]>;

@Injectable({ providedIn: 'root' })
export class YayinlarService {
    public resourceUrl = SERVER_API_URL + 'api/yayinlars';

    constructor(private http: HttpClient) {}

    create(yayinlar: IYayinlar): Observable<EntityResponseType> {
        return this.http.post<IYayinlar>(this.resourceUrl, yayinlar, { observe: 'response' });
    }

    update(yayinlar: IYayinlar): Observable<EntityResponseType> {
        return this.http.put<IYayinlar>(this.resourceUrl, yayinlar, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IYayinlar>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IYayinlar[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
