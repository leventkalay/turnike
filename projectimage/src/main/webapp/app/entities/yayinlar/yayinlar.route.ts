import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Yayinlar } from 'app/shared/model/yayinlar.model';
import { YayinlarService } from './yayinlar.service';
import { YayinlarComponent } from './yayinlar.component';
import { YayinlarDetailComponent } from './yayinlar-detail.component';
import { YayinlarUpdateComponent } from './yayinlar-update.component';
import { YayinlarDeletePopupComponent } from './yayinlar-delete-dialog.component';
import { IYayinlar } from 'app/shared/model/yayinlar.model';

@Injectable({ providedIn: 'root' })
export class YayinlarResolve implements Resolve<IYayinlar> {
    constructor(private service: YayinlarService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Yayinlar> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Yayinlar>) => response.ok),
                map((yayinlar: HttpResponse<Yayinlar>) => yayinlar.body)
            );
        }
        return of(new Yayinlar());
    }
}

export const yayinlarRoute: Routes = [
    {
        path: 'yayinlar',
        component: YayinlarComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'Yayinlars'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'yayinlar/:id/view',
        component: YayinlarDetailComponent,
        resolve: {
            yayinlar: YayinlarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Yayinlars'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'yayinlar/new',
        component: YayinlarUpdateComponent,
        resolve: {
            yayinlar: YayinlarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Yayinlars'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'yayinlar/:id/edit',
        component: YayinlarUpdateComponent,
        resolve: {
            yayinlar: YayinlarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Yayinlars'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const yayinlarPopupRoute: Routes = [
    {
        path: 'yayinlar/:id/delete',
        component: YayinlarDeletePopupComponent,
        resolve: {
            yayinlar: YayinlarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Yayinlars'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
