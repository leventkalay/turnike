import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IYayinlar } from 'app/shared/model/yayinlar.model';
import { YayinlarService } from './yayinlar.service';

@Component({
    selector: 'jhi-yayinlar-update',
    templateUrl: './yayinlar-update.component.html'
})
export class YayinlarUpdateComponent implements OnInit {
    yayinlar: IYayinlar;
    isSaving: boolean;

    constructor(private yayinlarService: YayinlarService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ yayinlar }) => {
            this.yayinlar = yayinlar;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.yayinlar.id !== undefined) {
            this.subscribeToSaveResponse(this.yayinlarService.update(this.yayinlar));
        } else {
            this.subscribeToSaveResponse(this.yayinlarService.create(this.yayinlar));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IYayinlar>>) {
        result.subscribe((res: HttpResponse<IYayinlar>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
