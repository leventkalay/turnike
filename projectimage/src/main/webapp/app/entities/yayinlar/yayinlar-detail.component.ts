import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IYayinlar } from 'app/shared/model/yayinlar.model';

@Component({
    selector: 'jhi-yayinlar-detail',
    templateUrl: './yayinlar-detail.component.html'
})
export class YayinlarDetailComponent implements OnInit {
    yayinlar: IYayinlar;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ yayinlar }) => {
            this.yayinlar = yayinlar;
        });
    }

    previousState() {
        window.history.back();
    }
}
