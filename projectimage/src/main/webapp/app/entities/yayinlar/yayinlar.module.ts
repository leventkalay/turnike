import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectimageSharedModule } from 'app/shared';
import {
    YayinlarComponent,
    YayinlarDetailComponent,
    YayinlarUpdateComponent,
    YayinlarDeletePopupComponent,
    YayinlarDeleteDialogComponent,
    yayinlarRoute,
    yayinlarPopupRoute
} from './';

const ENTITY_STATES = [...yayinlarRoute, ...yayinlarPopupRoute];

@NgModule({
    imports: [ProjectimageSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        YayinlarComponent,
        YayinlarDetailComponent,
        YayinlarUpdateComponent,
        YayinlarDeleteDialogComponent,
        YayinlarDeletePopupComponent
    ],
    entryComponents: [YayinlarComponent, YayinlarUpdateComponent, YayinlarDeleteDialogComponent, YayinlarDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectimageYayinlarModule {}
