export * from './yayinlar.service';
export * from './yayinlar-update.component';
export * from './yayinlar-delete-dialog.component';
export * from './yayinlar-detail.component';
export * from './yayinlar.component';
export * from './yayinlar.route';
