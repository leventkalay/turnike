import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IYayinlar } from 'app/shared/model/yayinlar.model';
import { YayinlarService } from './yayinlar.service';

@Component({
    selector: 'jhi-yayinlar-delete-dialog',
    templateUrl: './yayinlar-delete-dialog.component.html'
})
export class YayinlarDeleteDialogComponent {
    yayinlar: IYayinlar;

    constructor(private yayinlarService: YayinlarService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.yayinlarService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'yayinlarListModification',
                content: 'Deleted an yayinlar'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-yayinlar-delete-popup',
    template: ''
})
export class YayinlarDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ yayinlar }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(YayinlarDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.yayinlar = yayinlar;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
