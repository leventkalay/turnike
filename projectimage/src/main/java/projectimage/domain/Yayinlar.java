package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.YayinTuru;

/**
 * A Yayinlar.
 */
@Entity
@Table(name = "yayinlar")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Yayinlar implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "yayin_turu", nullable = false)
    private YayinTuru yayinTuru;

    @NotNull
    @Column(name = "yazar_adi", nullable = false)
    private String yazarAdi;

    @Column(name = "isbn")
    private String isbn;

    @NotNull
    @Column(name = "sayfa_sayisi", nullable = false)
    private Integer sayfaSayisi;

    @NotNull
    @Column(name = "yayin_bilgisi", nullable = false)
    private String yayinBilgisi;

    @NotNull
    @Column(name = "yayin_yil", nullable = false)
    private String yayinYil;

    @NotNull
    @Column(name = "elektronik_erisim", nullable = false)
    private String elektronikErisim;

    @Column(name = "anahtar_kelimeler")
    private String anahtarKelimeler;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public YayinTuru getYayinTuru() {
        return yayinTuru;
    }

    public Yayinlar yayinTuru(YayinTuru yayinTuru) {
        this.yayinTuru = yayinTuru;
        return this;
    }

    public void setYayinTuru(YayinTuru yayinTuru) {
        this.yayinTuru = yayinTuru;
    }

    public String getYazarAdi() {
        return yazarAdi;
    }

    public Yayinlar yazarAdi(String yazarAdi) {
        this.yazarAdi = yazarAdi;
        return this;
    }

    public void setYazarAdi(String yazarAdi) {
        this.yazarAdi = yazarAdi;
    }

    public String getIsbn() {
        return isbn;
    }

    public Yayinlar isbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Integer getSayfaSayisi() {
        return sayfaSayisi;
    }

    public Yayinlar sayfaSayisi(Integer sayfaSayisi) {
        this.sayfaSayisi = sayfaSayisi;
        return this;
    }

    public void setSayfaSayisi(Integer sayfaSayisi) {
        this.sayfaSayisi = sayfaSayisi;
    }

    public String getYayinBilgisi() {
        return yayinBilgisi;
    }

    public Yayinlar yayinBilgisi(String yayinBilgisi) {
        this.yayinBilgisi = yayinBilgisi;
        return this;
    }

    public void setYayinBilgisi(String yayinBilgisi) {
        this.yayinBilgisi = yayinBilgisi;
    }

    public String getYayinYil() {
        return yayinYil;
    }

    public Yayinlar yayinYil(String yayinYil) {
        this.yayinYil = yayinYil;
        return this;
    }

    public void setYayinYil(String yayinYil) {
        this.yayinYil = yayinYil;
    }

    public String getElektronikErisim() {
        return elektronikErisim;
    }

    public Yayinlar elektronikErisim(String elektronikErisim) {
        this.elektronikErisim = elektronikErisim;
        return this;
    }

    public void setElektronikErisim(String elektronikErisim) {
        this.elektronikErisim = elektronikErisim;
    }

    public String getAnahtarKelimeler() {
        return anahtarKelimeler;
    }

    public Yayinlar anahtarKelimeler(String anahtarKelimeler) {
        this.anahtarKelimeler = anahtarKelimeler;
        return this;
    }

    public void setAnahtarKelimeler(String anahtarKelimeler) {
        this.anahtarKelimeler = anahtarKelimeler;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Yayinlar yayinlar = (Yayinlar) o;
        if (yayinlar.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), yayinlar.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Yayinlar{" +
            "id=" + getId() +
            ", yayinTuru='" + getYayinTuru() + "'" +
            ", yazarAdi='" + getYazarAdi() + "'" +
            ", isbn='" + getIsbn() + "'" +
            ", sayfaSayisi=" + getSayfaSayisi() +
            ", yayinBilgisi='" + getYayinBilgisi() + "'" +
            ", yayinYil='" + getYayinYil() + "'" +
            ", elektronikErisim='" + getElektronikErisim() + "'" +
            ", anahtarKelimeler='" + getAnahtarKelimeler() + "'" +
            "}";
    }
}
