package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.ProgramTuru;

import projectimage.domain.enumeration.ProgramGorevi;

import projectimage.domain.enumeration.Ulke;

import projectimage.domain.enumeration.Sehir;

import projectimage.domain.enumeration.BelgeTuru;

/**
 * A KatilimSaglananProgramBilgisi.
 */
@Entity
@Table(name = "katilim_saglanan_program_bilgisi")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class KatilimSaglananProgramBilgisi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "program_turu", nullable = false)
    private ProgramTuru programTuru;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "program_gorevi", nullable = false)
    private ProgramGorevi programGorevi;

    @NotNull
    @Column(name = "program_adi", nullable = false)
    private String programAdi;

    @Column(name = "program_konusu")
    private String programKonusu;

    @Column(name = "baslangic_yili")
    private Integer baslangicYili;

    @Column(name = "bitis_yili")
    private Integer bitisYili;

    @NotNull
    @Column(name = "duzenleyen_kurum", nullable = false)
    private String duzenleyenKurum;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ulke", nullable = false)
    private Ulke ulke;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sehir", nullable = false)
    private Sehir sehir;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "belge_turu", nullable = false)
    private BelgeTuru belgeTuru;

    @Column(name = "anahtar_kelimeler")
    private String anahtarKelimeler;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProgramTuru getProgramTuru() {
        return programTuru;
    }

    public KatilimSaglananProgramBilgisi programTuru(ProgramTuru programTuru) {
        this.programTuru = programTuru;
        return this;
    }

    public void setProgramTuru(ProgramTuru programTuru) {
        this.programTuru = programTuru;
    }

    public ProgramGorevi getProgramGorevi() {
        return programGorevi;
    }

    public KatilimSaglananProgramBilgisi programGorevi(ProgramGorevi programGorevi) {
        this.programGorevi = programGorevi;
        return this;
    }

    public void setProgramGorevi(ProgramGorevi programGorevi) {
        this.programGorevi = programGorevi;
    }

    public String getProgramAdi() {
        return programAdi;
    }

    public KatilimSaglananProgramBilgisi programAdi(String programAdi) {
        this.programAdi = programAdi;
        return this;
    }

    public void setProgramAdi(String programAdi) {
        this.programAdi = programAdi;
    }

    public String getProgramKonusu() {
        return programKonusu;
    }

    public KatilimSaglananProgramBilgisi programKonusu(String programKonusu) {
        this.programKonusu = programKonusu;
        return this;
    }

    public void setProgramKonusu(String programKonusu) {
        this.programKonusu = programKonusu;
    }

    public Integer getBaslangicYili() {
        return baslangicYili;
    }

    public KatilimSaglananProgramBilgisi baslangicYili(Integer baslangicYili) {
        this.baslangicYili = baslangicYili;
        return this;
    }

    public void setBaslangicYili(Integer baslangicYili) {
        this.baslangicYili = baslangicYili;
    }

    public Integer getBitisYili() {
        return bitisYili;
    }

    public KatilimSaglananProgramBilgisi bitisYili(Integer bitisYili) {
        this.bitisYili = bitisYili;
        return this;
    }

    public void setBitisYili(Integer bitisYili) {
        this.bitisYili = bitisYili;
    }

    public String getDuzenleyenKurum() {
        return duzenleyenKurum;
    }

    public KatilimSaglananProgramBilgisi duzenleyenKurum(String duzenleyenKurum) {
        this.duzenleyenKurum = duzenleyenKurum;
        return this;
    }

    public void setDuzenleyenKurum(String duzenleyenKurum) {
        this.duzenleyenKurum = duzenleyenKurum;
    }

    public Ulke getUlke() {
        return ulke;
    }

    public KatilimSaglananProgramBilgisi ulke(Ulke ulke) {
        this.ulke = ulke;
        return this;
    }

    public void setUlke(Ulke ulke) {
        this.ulke = ulke;
    }

    public Sehir getSehir() {
        return sehir;
    }

    public KatilimSaglananProgramBilgisi sehir(Sehir sehir) {
        this.sehir = sehir;
        return this;
    }

    public void setSehir(Sehir sehir) {
        this.sehir = sehir;
    }

    public BelgeTuru getBelgeTuru() {
        return belgeTuru;
    }

    public KatilimSaglananProgramBilgisi belgeTuru(BelgeTuru belgeTuru) {
        this.belgeTuru = belgeTuru;
        return this;
    }

    public void setBelgeTuru(BelgeTuru belgeTuru) {
        this.belgeTuru = belgeTuru;
    }

    public String getAnahtarKelimeler() {
        return anahtarKelimeler;
    }

    public KatilimSaglananProgramBilgisi anahtarKelimeler(String anahtarKelimeler) {
        this.anahtarKelimeler = anahtarKelimeler;
        return this;
    }

    public void setAnahtarKelimeler(String anahtarKelimeler) {
        this.anahtarKelimeler = anahtarKelimeler;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi = (KatilimSaglananProgramBilgisi) o;
        if (katilimSaglananProgramBilgisi.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), katilimSaglananProgramBilgisi.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KatilimSaglananProgramBilgisi{" +
            "id=" + getId() +
            ", programTuru='" + getProgramTuru() + "'" +
            ", programGorevi='" + getProgramGorevi() + "'" +
            ", programAdi='" + getProgramAdi() + "'" +
            ", programKonusu='" + getProgramKonusu() + "'" +
            ", baslangicYili=" + getBaslangicYili() +
            ", bitisYili=" + getBitisYili() +
            ", duzenleyenKurum='" + getDuzenleyenKurum() + "'" +
            ", ulke='" + getUlke() + "'" +
            ", sehir='" + getSehir() + "'" +
            ", belgeTuru='" + getBelgeTuru() + "'" +
            ", anahtarKelimeler='" + getAnahtarKelimeler() + "'" +
            "}";
    }
}
