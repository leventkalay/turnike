package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.KurumTipi;

/**
 * A EgiticiYetkinlikleri.
 */
@Entity
@Table(name = "egitici_yetkinlikleri")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EgiticiYetkinlikleri implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "kurum_tipi")
    private KurumTipi kurumTipi;

    @Column(name = "universite_adi")
    private String universiteAdi;

    @Column(name = "kurum_adi")
    private String kurumAdi;

    @Column(name = "fakulte")
    private String fakulte;

    @Column(name = "bolum")
    private String bolum;

    @Column(name = "konu_ders_adi")
    private String konuDersAdi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public KurumTipi getKurumTipi() {
        return kurumTipi;
    }

    public EgiticiYetkinlikleri kurumTipi(KurumTipi kurumTipi) {
        this.kurumTipi = kurumTipi;
        return this;
    }

    public void setKurumTipi(KurumTipi kurumTipi) {
        this.kurumTipi = kurumTipi;
    }

    public String getUniversiteAdi() {
        return universiteAdi;
    }

    public EgiticiYetkinlikleri universiteAdi(String universiteAdi) {
        this.universiteAdi = universiteAdi;
        return this;
    }

    public void setUniversiteAdi(String universiteAdi) {
        this.universiteAdi = universiteAdi;
    }

    public String getKurumAdi() {
        return kurumAdi;
    }

    public EgiticiYetkinlikleri kurumAdi(String kurumAdi) {
        this.kurumAdi = kurumAdi;
        return this;
    }

    public void setKurumAdi(String kurumAdi) {
        this.kurumAdi = kurumAdi;
    }

    public String getFakulte() {
        return fakulte;
    }

    public EgiticiYetkinlikleri fakulte(String fakulte) {
        this.fakulte = fakulte;
        return this;
    }

    public void setFakulte(String fakulte) {
        this.fakulte = fakulte;
    }

    public String getBolum() {
        return bolum;
    }

    public EgiticiYetkinlikleri bolum(String bolum) {
        this.bolum = bolum;
        return this;
    }

    public void setBolum(String bolum) {
        this.bolum = bolum;
    }

    public String getKonuDersAdi() {
        return konuDersAdi;
    }

    public EgiticiYetkinlikleri konuDersAdi(String konuDersAdi) {
        this.konuDersAdi = konuDersAdi;
        return this;
    }

    public void setKonuDersAdi(String konuDersAdi) {
        this.konuDersAdi = konuDersAdi;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EgiticiYetkinlikleri egiticiYetkinlikleri = (EgiticiYetkinlikleri) o;
        if (egiticiYetkinlikleri.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), egiticiYetkinlikleri.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EgiticiYetkinlikleri{" +
            "id=" + getId() +
            ", kurumTipi='" + getKurumTipi() + "'" +
            ", universiteAdi='" + getUniversiteAdi() + "'" +
            ", kurumAdi='" + getKurumAdi() + "'" +
            ", fakulte='" + getFakulte() + "'" +
            ", bolum='" + getBolum() + "'" +
            ", konuDersAdi='" + getKonuDersAdi() + "'" +
            "}";
    }
}
