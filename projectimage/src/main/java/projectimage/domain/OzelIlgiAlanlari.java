package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A OzelIlgiAlanlari.
 */
@Entity
@Table(name = "ozel_ilgi_alanlari")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OzelIlgiAlanlari implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "ozel_ilgi_alanlari")
    private String ozelIlgiAlanlari;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOzelIlgiAlanlari() {
        return ozelIlgiAlanlari;
    }

    public OzelIlgiAlanlari ozelIlgiAlanlari(String ozelIlgiAlanlari) {
        this.ozelIlgiAlanlari = ozelIlgiAlanlari;
        return this;
    }

    public void setOzelIlgiAlanlari(String ozelIlgiAlanlari) {
        this.ozelIlgiAlanlari = ozelIlgiAlanlari;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OzelIlgiAlanlari ozelIlgiAlanlari = (OzelIlgiAlanlari) o;
        if (ozelIlgiAlanlari.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ozelIlgiAlanlari.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OzelIlgiAlanlari{" +
            "id=" + getId() +
            ", ozelIlgiAlanlari='" + getOzelIlgiAlanlari() + "'" +
            "}";
    }
}
