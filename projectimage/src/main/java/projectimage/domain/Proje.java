package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.ProjeCiktisi;

/**
 * A Proje.
 */
@Entity
@Table(name = "proje")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Proje implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "proje_adi", nullable = false)
    private String projeAdi;

    @NotNull
    @Column(name = "gorevi", nullable = false)
    private String gorevi;

    @NotNull
    @Column(name = "konusu", nullable = false)
    private String konusu;

    @NotNull
    @Column(name = "amaci", nullable = false)
    private String amaci;

    @NotNull
    @Column(name = "yil", nullable = false)
    private Integer yil;

    @NotNull
    @Column(name = "yuruten_kurum", nullable = false)
    private String yurutenKurum;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "proje_ciktisi", nullable = false)
    private ProjeCiktisi projeCiktisi;

    @Column(name = "anahtar_kelimeler")
    private String anahtarKelimeler;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjeAdi() {
        return projeAdi;
    }

    public Proje projeAdi(String projeAdi) {
        this.projeAdi = projeAdi;
        return this;
    }

    public void setProjeAdi(String projeAdi) {
        this.projeAdi = projeAdi;
    }

    public String getGorevi() {
        return gorevi;
    }

    public Proje gorevi(String gorevi) {
        this.gorevi = gorevi;
        return this;
    }

    public void setGorevi(String gorevi) {
        this.gorevi = gorevi;
    }

    public String getKonusu() {
        return konusu;
    }

    public Proje konusu(String konusu) {
        this.konusu = konusu;
        return this;
    }

    public void setKonusu(String konusu) {
        this.konusu = konusu;
    }

    public String getAmaci() {
        return amaci;
    }

    public Proje amaci(String amaci) {
        this.amaci = amaci;
        return this;
    }

    public void setAmaci(String amaci) {
        this.amaci = amaci;
    }

    public Integer getYil() {
        return yil;
    }

    public Proje yil(Integer yil) {
        this.yil = yil;
        return this;
    }

    public void setYil(Integer yil) {
        this.yil = yil;
    }

    public String getYurutenKurum() {
        return yurutenKurum;
    }

    public Proje yurutenKurum(String yurutenKurum) {
        this.yurutenKurum = yurutenKurum;
        return this;
    }

    public void setYurutenKurum(String yurutenKurum) {
        this.yurutenKurum = yurutenKurum;
    }

    public ProjeCiktisi getProjeCiktisi() {
        return projeCiktisi;
    }

    public Proje projeCiktisi(ProjeCiktisi projeCiktisi) {
        this.projeCiktisi = projeCiktisi;
        return this;
    }

    public void setProjeCiktisi(ProjeCiktisi projeCiktisi) {
        this.projeCiktisi = projeCiktisi;
    }

    public String getAnahtarKelimeler() {
        return anahtarKelimeler;
    }

    public Proje anahtarKelimeler(String anahtarKelimeler) {
        this.anahtarKelimeler = anahtarKelimeler;
        return this;
    }

    public void setAnahtarKelimeler(String anahtarKelimeler) {
        this.anahtarKelimeler = anahtarKelimeler;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Proje proje = (Proje) o;
        if (proje.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), proje.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Proje{" +
            "id=" + getId() +
            ", projeAdi='" + getProjeAdi() + "'" +
            ", gorevi='" + getGorevi() + "'" +
            ", konusu='" + getKonusu() + "'" +
            ", amaci='" + getAmaci() + "'" +
            ", yil=" + getYil() +
            ", yurutenKurum='" + getYurutenKurum() + "'" +
            ", projeCiktisi='" + getProjeCiktisi() + "'" +
            ", anahtarKelimeler='" + getAnahtarKelimeler() + "'" +
            "}";
    }
}
