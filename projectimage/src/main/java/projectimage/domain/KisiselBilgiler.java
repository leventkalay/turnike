package projectimage.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.MedeniDurum;

import projectimage.domain.enumeration.Cinsiyet;

import projectimage.domain.enumeration.IstihdamSekli;

import projectimage.domain.enumeration.Askerlik;

import projectimage.domain.enumeration.KanGrubu;

/**
 * A KisiselBilgiler.
 */
@Entity
@Table(name = "kisisel_bilgiler")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class KisiselBilgiler implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "ad", nullable = false)
    private String ad;

    @NotNull
    @Column(name = "soyad", nullable = false)
    private String soyad;

    @Column(name = "pirim_gun_sayisi")
    private Integer pirimGunSayisi;

    @NotNull
    @Column(name = "sgk_no", nullable = false)
    private String sgkNo;

    @NotNull
    @Column(name = "sicil_no", nullable = false)
    private String sicilNo;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "medeni_durum", nullable = false)
    private MedeniDurum medeniDurum;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "cinsiyet", nullable = false)
    private Cinsiyet cinsiyet;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "istihdam_sekli", nullable = false)
    private IstihdamSekli istihdamSekli;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "askerlik", nullable = false)
    private Askerlik askerlik;

    @Enumerated(EnumType.STRING)
    @Column(name = "kangrubu")
    private KanGrubu kangrubu;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @Lob
    @Column(name = "jhi_any")
    private byte[] any;

    @Column(name = "jhi_any_content_type")
    private String anyContentType;

    @Lob
    @Column(name = "text")
    private String text;

    @ManyToOne
    @JsonIgnoreProperties("")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAd() {
        return ad;
    }

    public KisiselBilgiler ad(String ad) {
        this.ad = ad;
        return this;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public KisiselBilgiler soyad(String soyad) {
        this.soyad = soyad;
        return this;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public Integer getPirimGunSayisi() {
        return pirimGunSayisi;
    }

    public KisiselBilgiler pirimGunSayisi(Integer pirimGunSayisi) {
        this.pirimGunSayisi = pirimGunSayisi;
        return this;
    }

    public void setPirimGunSayisi(Integer pirimGunSayisi) {
        this.pirimGunSayisi = pirimGunSayisi;
    }

    public String getSgkNo() {
        return sgkNo;
    }

    public KisiselBilgiler sgkNo(String sgkNo) {
        this.sgkNo = sgkNo;
        return this;
    }

    public void setSgkNo(String sgkNo) {
        this.sgkNo = sgkNo;
    }

    public String getSicilNo() {
        return sicilNo;
    }

    public KisiselBilgiler sicilNo(String sicilNo) {
        this.sicilNo = sicilNo;
        return this;
    }

    public void setSicilNo(String sicilNo) {
        this.sicilNo = sicilNo;
    }

    public MedeniDurum getMedeniDurum() {
        return medeniDurum;
    }

    public KisiselBilgiler medeniDurum(MedeniDurum medeniDurum) {
        this.medeniDurum = medeniDurum;
        return this;
    }

    public void setMedeniDurum(MedeniDurum medeniDurum) {
        this.medeniDurum = medeniDurum;
    }

    public Cinsiyet getCinsiyet() {
        return cinsiyet;
    }

    public KisiselBilgiler cinsiyet(Cinsiyet cinsiyet) {
        this.cinsiyet = cinsiyet;
        return this;
    }

    public void setCinsiyet(Cinsiyet cinsiyet) {
        this.cinsiyet = cinsiyet;
    }

    public IstihdamSekli getIstihdamSekli() {
        return istihdamSekli;
    }

    public KisiselBilgiler istihdamSekli(IstihdamSekli istihdamSekli) {
        this.istihdamSekli = istihdamSekli;
        return this;
    }

    public void setIstihdamSekli(IstihdamSekli istihdamSekli) {
        this.istihdamSekli = istihdamSekli;
    }

    public Askerlik getAskerlik() {
        return askerlik;
    }

    public KisiselBilgiler askerlik(Askerlik askerlik) {
        this.askerlik = askerlik;
        return this;
    }

    public void setAskerlik(Askerlik askerlik) {
        this.askerlik = askerlik;
    }

    public KanGrubu getKangrubu() {
        return kangrubu;
    }

    public KisiselBilgiler kangrubu(KanGrubu kangrubu) {
        this.kangrubu = kangrubu;
        return this;
    }

    public void setKangrubu(KanGrubu kangrubu) {
        this.kangrubu = kangrubu;
    }

    public byte[] getImage() {
        return image;
    }

    public KisiselBilgiler image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public KisiselBilgiler imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public byte[] getAny() {
        return any;
    }

    public KisiselBilgiler any(byte[] any) {
        this.any = any;
        return this;
    }

    public void setAny(byte[] any) {
        this.any = any;
    }

    public String getAnyContentType() {
        return anyContentType;
    }

    public KisiselBilgiler anyContentType(String anyContentType) {
        this.anyContentType = anyContentType;
        return this;
    }

    public void setAnyContentType(String anyContentType) {
        this.anyContentType = anyContentType;
    }

    public String getText() {
        return text;
    }

    public KisiselBilgiler text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public KisiselBilgiler user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KisiselBilgiler kisiselBilgiler = (KisiselBilgiler) o;
        if (kisiselBilgiler.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), kisiselBilgiler.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KisiselBilgiler{" +
            "id=" + getId() +
            ", ad='" + getAd() + "'" +
            ", soyad='" + getSoyad() + "'" +
            ", pirimGunSayisi=" + getPirimGunSayisi() +
            ", sgkNo='" + getSgkNo() + "'" +
            ", sicilNo='" + getSicilNo() + "'" +
            ", medeniDurum='" + getMedeniDurum() + "'" +
            ", cinsiyet='" + getCinsiyet() + "'" +
            ", istihdamSekli='" + getIstihdamSekli() + "'" +
            ", askerlik='" + getAskerlik() + "'" +
            ", kangrubu='" + getKangrubu() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", any='" + getAny() + "'" +
            ", anyContentType='" + getAnyContentType() + "'" +
            ", text='" + getText() + "'" +
            "}";
    }
}
