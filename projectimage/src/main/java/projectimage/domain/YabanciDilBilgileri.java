package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import projectimage.domain.enumeration.Diller;

import projectimage.domain.enumeration.BelgeTuru;

/**
 * A YabanciDilBilgileri.
 */
@Entity
@Table(name = "yabanci_dil_bilgileri")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class YabanciDilBilgileri implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "dil")
    private Diller dil;

    @Enumerated(EnumType.STRING)
    @Column(name = "belge_turu")
    private BelgeTuru belgeTuru;

    @NotNull
    @Column(name = "belge_tarihi", nullable = false)
    private LocalDate belgeTarihi;

    @NotNull
    @Column(name = "puan", nullable = false)
    private Double puan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Diller getDil() {
        return dil;
    }

    public YabanciDilBilgileri dil(Diller dil) {
        this.dil = dil;
        return this;
    }

    public void setDil(Diller dil) {
        this.dil = dil;
    }

    public BelgeTuru getBelgeTuru() {
        return belgeTuru;
    }

    public YabanciDilBilgileri belgeTuru(BelgeTuru belgeTuru) {
        this.belgeTuru = belgeTuru;
        return this;
    }

    public void setBelgeTuru(BelgeTuru belgeTuru) {
        this.belgeTuru = belgeTuru;
    }

    public LocalDate getBelgeTarihi() {
        return belgeTarihi;
    }

    public YabanciDilBilgileri belgeTarihi(LocalDate belgeTarihi) {
        this.belgeTarihi = belgeTarihi;
        return this;
    }

    public void setBelgeTarihi(LocalDate belgeTarihi) {
        this.belgeTarihi = belgeTarihi;
    }

    public Double getPuan() {
        return puan;
    }

    public YabanciDilBilgileri puan(Double puan) {
        this.puan = puan;
        return this;
    }

    public void setPuan(Double puan) {
        this.puan = puan;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        YabanciDilBilgileri yabanciDilBilgileri = (YabanciDilBilgileri) o;
        if (yabanciDilBilgileri.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), yabanciDilBilgileri.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "YabanciDilBilgileri{" +
            "id=" + getId() +
            ", dil='" + getDil() + "'" +
            ", belgeTuru='" + getBelgeTuru() + "'" +
            ", belgeTarihi='" + getBelgeTarihi() + "'" +
            ", puan=" + getPuan() +
            "}";
    }
}
