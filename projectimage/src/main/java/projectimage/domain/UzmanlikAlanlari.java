package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.DetayBilgiler;

/**
 * A UzmanlikAlanlari.
 */
@Entity
@Table(name = "uzmanlik_alanlari")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UzmanlikAlanlari implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "detay_bilgiler", nullable = false)
    private DetayBilgiler detayBilgiler;

    @NotNull
    @Column(name = "aciklama", nullable = false)
    private String aciklama;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DetayBilgiler getDetayBilgiler() {
        return detayBilgiler;
    }

    public UzmanlikAlanlari detayBilgiler(DetayBilgiler detayBilgiler) {
        this.detayBilgiler = detayBilgiler;
        return this;
    }

    public void setDetayBilgiler(DetayBilgiler detayBilgiler) {
        this.detayBilgiler = detayBilgiler;
    }

    public String getAciklama() {
        return aciklama;
    }

    public UzmanlikAlanlari aciklama(String aciklama) {
        this.aciklama = aciklama;
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UzmanlikAlanlari uzmanlikAlanlari = (UzmanlikAlanlari) o;
        if (uzmanlikAlanlari.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), uzmanlikAlanlari.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UzmanlikAlanlari{" +
            "id=" + getId() +
            ", detayBilgiler='" + getDetayBilgiler() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            "}";
    }
}
