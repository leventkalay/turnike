package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.OgrenimDurumu;

import projectimage.domain.enumeration.Diller;

import projectimage.domain.enumeration.Ulke;

import projectimage.domain.enumeration.Sehir;

/**
 * A OgrenimBilgileri.
 */
@Entity
@Table(name = "ogrenim_bilgileri")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OgrenimBilgileri implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ogrenim_durumu", nullable = false)
    private OgrenimDurumu ogrenimDurumu;

    @NotNull
    @Column(name = "okul", nullable = false)
    private String okul;

    @Column(name = "mezuniyet_yili")
    private Integer mezuniyetYili;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ogrenim_dili", nullable = false)
    private Diller ogrenimDili;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ulke", nullable = false)
    private Ulke ulke;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sehir", nullable = false)
    private Sehir sehir;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OgrenimDurumu getOgrenimDurumu() {
        return ogrenimDurumu;
    }

    public OgrenimBilgileri ogrenimDurumu(OgrenimDurumu ogrenimDurumu) {
        this.ogrenimDurumu = ogrenimDurumu;
        return this;
    }

    public void setOgrenimDurumu(OgrenimDurumu ogrenimDurumu) {
        this.ogrenimDurumu = ogrenimDurumu;
    }

    public String getOkul() {
        return okul;
    }

    public OgrenimBilgileri okul(String okul) {
        this.okul = okul;
        return this;
    }

    public void setOkul(String okul) {
        this.okul = okul;
    }

    public Integer getMezuniyetYili() {
        return mezuniyetYili;
    }

    public OgrenimBilgileri mezuniyetYili(Integer mezuniyetYili) {
        this.mezuniyetYili = mezuniyetYili;
        return this;
    }

    public void setMezuniyetYili(Integer mezuniyetYili) {
        this.mezuniyetYili = mezuniyetYili;
    }

    public Diller getOgrenimDili() {
        return ogrenimDili;
    }

    public OgrenimBilgileri ogrenimDili(Diller ogrenimDili) {
        this.ogrenimDili = ogrenimDili;
        return this;
    }

    public void setOgrenimDili(Diller ogrenimDili) {
        this.ogrenimDili = ogrenimDili;
    }

    public Ulke getUlke() {
        return ulke;
    }

    public OgrenimBilgileri ulke(Ulke ulke) {
        this.ulke = ulke;
        return this;
    }

    public void setUlke(Ulke ulke) {
        this.ulke = ulke;
    }

    public Sehir getSehir() {
        return sehir;
    }

    public OgrenimBilgileri sehir(Sehir sehir) {
        this.sehir = sehir;
        return this;
    }

    public void setSehir(Sehir sehir) {
        this.sehir = sehir;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OgrenimBilgileri ogrenimBilgileri = (OgrenimBilgileri) o;
        if (ogrenimBilgileri.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ogrenimBilgileri.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OgrenimBilgileri{" +
            "id=" + getId() +
            ", ogrenimDurumu='" + getOgrenimDurumu() + "'" +
            ", okul='" + getOkul() + "'" +
            ", mezuniyetYili=" + getMezuniyetYili() +
            ", ogrenimDili='" + getOgrenimDili() + "'" +
            ", ulke='" + getUlke() + "'" +
            ", sehir='" + getSehir() + "'" +
            "}";
    }
}
