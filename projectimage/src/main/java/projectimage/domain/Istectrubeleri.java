package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.CalismaAlani;

import projectimage.domain.enumeration.CalismaSekli;

import projectimage.domain.enumeration.Aylar;

import projectimage.domain.enumeration.Ulke;

import projectimage.domain.enumeration.Sehir;

/**
 * A Istectrubeleri.
 */
@Entity
@Table(name = "istectrubeleri")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Istectrubeleri implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "calisma_alani", nullable = false)
    private CalismaAlani calismaAlani;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "calisma_sekli", nullable = false)
    private CalismaSekli calismaSekli;

    @NotNull
    @Column(name = "kurum_adi", nullable = false)
    private String kurumAdi;

    @NotNull
    @Column(name = "gorev_birimi", nullable = false)
    private String gorevBirimi;

    @NotNull
    @Column(name = "unvan", nullable = false)
    private String unvan;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "baslangic_ayi", nullable = false)
    private Aylar baslangicAyi;

    @NotNull
    @Column(name = "baslangic_yili", nullable = false)
    private Integer baslangicYili;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "bitis_ayi", nullable = false)
    private Aylar bitisAyi;

    @NotNull
    @Column(name = "bitis_yili", nullable = false)
    private Integer bitisYili;

    @Column(name = "gorev_tanimi")
    private String gorevTanimi;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "ulke", nullable = false)
    private Ulke ulke;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sehir", nullable = false)
    private Sehir sehir;

    @Column(name = "yararli_bilgier")
    private String yararliBilgier;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalismaAlani getCalismaAlani() {
        return calismaAlani;
    }

    public Istectrubeleri calismaAlani(CalismaAlani calismaAlani) {
        this.calismaAlani = calismaAlani;
        return this;
    }

    public void setCalismaAlani(CalismaAlani calismaAlani) {
        this.calismaAlani = calismaAlani;
    }

    public CalismaSekli getCalismaSekli() {
        return calismaSekli;
    }

    public Istectrubeleri calismaSekli(CalismaSekli calismaSekli) {
        this.calismaSekli = calismaSekli;
        return this;
    }

    public void setCalismaSekli(CalismaSekli calismaSekli) {
        this.calismaSekli = calismaSekli;
    }

    public String getKurumAdi() {
        return kurumAdi;
    }

    public Istectrubeleri kurumAdi(String kurumAdi) {
        this.kurumAdi = kurumAdi;
        return this;
    }

    public void setKurumAdi(String kurumAdi) {
        this.kurumAdi = kurumAdi;
    }

    public String getGorevBirimi() {
        return gorevBirimi;
    }

    public Istectrubeleri gorevBirimi(String gorevBirimi) {
        this.gorevBirimi = gorevBirimi;
        return this;
    }

    public void setGorevBirimi(String gorevBirimi) {
        this.gorevBirimi = gorevBirimi;
    }

    public String getUnvan() {
        return unvan;
    }

    public Istectrubeleri unvan(String unvan) {
        this.unvan = unvan;
        return this;
    }

    public void setUnvan(String unvan) {
        this.unvan = unvan;
    }

    public Aylar getBaslangicAyi() {
        return baslangicAyi;
    }

    public Istectrubeleri baslangicAyi(Aylar baslangicAyi) {
        this.baslangicAyi = baslangicAyi;
        return this;
    }

    public void setBaslangicAyi(Aylar baslangicAyi) {
        this.baslangicAyi = baslangicAyi;
    }

    public Integer getBaslangicYili() {
        return baslangicYili;
    }

    public Istectrubeleri baslangicYili(Integer baslangicYili) {
        this.baslangicYili = baslangicYili;
        return this;
    }

    public void setBaslangicYili(Integer baslangicYili) {
        this.baslangicYili = baslangicYili;
    }

    public Aylar getBitisAyi() {
        return bitisAyi;
    }

    public Istectrubeleri bitisAyi(Aylar bitisAyi) {
        this.bitisAyi = bitisAyi;
        return this;
    }

    public void setBitisAyi(Aylar bitisAyi) {
        this.bitisAyi = bitisAyi;
    }

    public Integer getBitisYili() {
        return bitisYili;
    }

    public Istectrubeleri bitisYili(Integer bitisYili) {
        this.bitisYili = bitisYili;
        return this;
    }

    public void setBitisYili(Integer bitisYili) {
        this.bitisYili = bitisYili;
    }

    public String getGorevTanimi() {
        return gorevTanimi;
    }

    public Istectrubeleri gorevTanimi(String gorevTanimi) {
        this.gorevTanimi = gorevTanimi;
        return this;
    }

    public void setGorevTanimi(String gorevTanimi) {
        this.gorevTanimi = gorevTanimi;
    }

    public Ulke getUlke() {
        return ulke;
    }

    public Istectrubeleri ulke(Ulke ulke) {
        this.ulke = ulke;
        return this;
    }

    public void setUlke(Ulke ulke) {
        this.ulke = ulke;
    }

    public Sehir getSehir() {
        return sehir;
    }

    public Istectrubeleri sehir(Sehir sehir) {
        this.sehir = sehir;
        return this;
    }

    public void setSehir(Sehir sehir) {
        this.sehir = sehir;
    }

    public String getYararliBilgier() {
        return yararliBilgier;
    }

    public Istectrubeleri yararliBilgier(String yararliBilgier) {
        this.yararliBilgier = yararliBilgier;
        return this;
    }

    public void setYararliBilgier(String yararliBilgier) {
        this.yararliBilgier = yararliBilgier;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Istectrubeleri istectrubeleri = (Istectrubeleri) o;
        if (istectrubeleri.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), istectrubeleri.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Istectrubeleri{" +
            "id=" + getId() +
            ", calismaAlani='" + getCalismaAlani() + "'" +
            ", calismaSekli='" + getCalismaSekli() + "'" +
            ", kurumAdi='" + getKurumAdi() + "'" +
            ", gorevBirimi='" + getGorevBirimi() + "'" +
            ", unvan='" + getUnvan() + "'" +
            ", baslangicAyi='" + getBaslangicAyi() + "'" +
            ", baslangicYili=" + getBaslangicYili() +
            ", bitisAyi='" + getBitisAyi() + "'" +
            ", bitisYili=" + getBitisYili() +
            ", gorevTanimi='" + getGorevTanimi() + "'" +
            ", ulke='" + getUlke() + "'" +
            ", sehir='" + getSehir() + "'" +
            ", yararliBilgier='" + getYararliBilgier() + "'" +
            "}";
    }
}
