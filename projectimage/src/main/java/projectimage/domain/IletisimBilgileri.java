package projectimage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import projectimage.domain.enumeration.Yakinlik;

/**
 * A IletisimBilgileri.
 */
@Entity
@Table(name = "iletisim_bilgileri")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class IletisimBilgileri implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "adres")
    private String adres;

    @Column(name = "cep_telefonu")
    private String cepTelefonu;

    @Column(name = "cep_telefonu_alt")
    private String cepTelefonuAlt;

    @Column(name = "ev_telefonu")
    private String evTelefonu;

    @Column(name = "dahili_telefon")
    private String dahiliTelefon;

    @NotNull
    @Column(name = "eposta", nullable = false)
    private String eposta;

    @Column(name = "acil_ad_soyad")
    private String acilAdSoyad;

    @Enumerated(EnumType.STRING)
    @Column(name = "yakinlik")
    private Yakinlik yakinlik;

    @Column(name = "acil_telefon")
    private String acilTelefon;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdres() {
        return adres;
    }

    public IletisimBilgileri adres(String adres) {
        this.adres = adres;
        return this;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getCepTelefonu() {
        return cepTelefonu;
    }

    public IletisimBilgileri cepTelefonu(String cepTelefonu) {
        this.cepTelefonu = cepTelefonu;
        return this;
    }

    public void setCepTelefonu(String cepTelefonu) {
        this.cepTelefonu = cepTelefonu;
    }

    public String getCepTelefonuAlt() {
        return cepTelefonuAlt;
    }

    public IletisimBilgileri cepTelefonuAlt(String cepTelefonuAlt) {
        this.cepTelefonuAlt = cepTelefonuAlt;
        return this;
    }

    public void setCepTelefonuAlt(String cepTelefonuAlt) {
        this.cepTelefonuAlt = cepTelefonuAlt;
    }

    public String getEvTelefonu() {
        return evTelefonu;
    }

    public IletisimBilgileri evTelefonu(String evTelefonu) {
        this.evTelefonu = evTelefonu;
        return this;
    }

    public void setEvTelefonu(String evTelefonu) {
        this.evTelefonu = evTelefonu;
    }

    public String getDahiliTelefon() {
        return dahiliTelefon;
    }

    public IletisimBilgileri dahiliTelefon(String dahiliTelefon) {
        this.dahiliTelefon = dahiliTelefon;
        return this;
    }

    public void setDahiliTelefon(String dahiliTelefon) {
        this.dahiliTelefon = dahiliTelefon;
    }

    public String getEposta() {
        return eposta;
    }

    public IletisimBilgileri eposta(String eposta) {
        this.eposta = eposta;
        return this;
    }

    public void setEposta(String eposta) {
        this.eposta = eposta;
    }

    public String getAcilAdSoyad() {
        return acilAdSoyad;
    }

    public IletisimBilgileri acilAdSoyad(String acilAdSoyad) {
        this.acilAdSoyad = acilAdSoyad;
        return this;
    }

    public void setAcilAdSoyad(String acilAdSoyad) {
        this.acilAdSoyad = acilAdSoyad;
    }

    public Yakinlik getYakinlik() {
        return yakinlik;
    }

    public IletisimBilgileri yakinlik(Yakinlik yakinlik) {
        this.yakinlik = yakinlik;
        return this;
    }

    public void setYakinlik(Yakinlik yakinlik) {
        this.yakinlik = yakinlik;
    }

    public String getAcilTelefon() {
        return acilTelefon;
    }

    public IletisimBilgileri acilTelefon(String acilTelefon) {
        this.acilTelefon = acilTelefon;
        return this;
    }

    public void setAcilTelefon(String acilTelefon) {
        this.acilTelefon = acilTelefon;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IletisimBilgileri iletisimBilgileri = (IletisimBilgileri) o;
        if (iletisimBilgileri.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), iletisimBilgileri.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IletisimBilgileri{" +
            "id=" + getId() +
            ", adres='" + getAdres() + "'" +
            ", cepTelefonu='" + getCepTelefonu() + "'" +
            ", cepTelefonuAlt='" + getCepTelefonuAlt() + "'" +
            ", evTelefonu='" + getEvTelefonu() + "'" +
            ", dahiliTelefon='" + getDahiliTelefon() + "'" +
            ", eposta='" + getEposta() + "'" +
            ", acilAdSoyad='" + getAcilAdSoyad() + "'" +
            ", yakinlik='" + getYakinlik() + "'" +
            ", acilTelefon='" + getAcilTelefon() + "'" +
            "}";
    }
}
