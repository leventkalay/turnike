package projectimage.domain.enumeration;

/**
 * The BelgeTuru enumeration.
 */
public enum BelgeTuru {
    CAE, CPE, FCE, IELTS, KPDS, TOEFL, UDS, YDS
}
