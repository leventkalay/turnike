package projectimage.domain.enumeration;

/**
 * The CalismaAlani enumeration.
 */
public enum CalismaAlani {
    KAMU, OZEL, ULUSLARARASI
}
