package projectimage.domain.enumeration;

/**
 * The OgrenimDurumu enumeration.
 */
public enum OgrenimDurumu {
    ILKOKUL, ORTAOKUL, LISE, ONLISANS, LISANS, LISANSUSTU, DOKTORA
}
