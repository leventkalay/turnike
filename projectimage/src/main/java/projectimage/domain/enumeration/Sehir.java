package projectimage.domain.enumeration;

/**
 * The Sehir enumeration.
 */
public enum Sehir {
    ADANA, ADIYAMAN, AFYON, AGRI, AMASYA, ANKARA, ANTALYA, ARTVIN, AYDIN, BALIKESIR, ISTANBUL, IZMIR
}
