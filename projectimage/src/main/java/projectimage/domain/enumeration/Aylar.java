package projectimage.domain.enumeration;

/**
 * The Aylar enumeration.
 */
public enum Aylar {
    OCAK, SUBAT, MART, NISAN, MAYIS, HAZIRAN, TEMMUZ, AGUSTOS, EYLUL, EKIM, KASIM, ARALIK
}
