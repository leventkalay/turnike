package projectimage.domain.enumeration;

/**
 * The KurumTipi enumeration.
 */
public enum KurumTipi {
    UNIVERSITE, DIGER
}
