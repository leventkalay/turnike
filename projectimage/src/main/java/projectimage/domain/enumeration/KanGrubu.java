package projectimage.domain.enumeration;

/**
 * The KanGrubu enumeration.
 */
public enum KanGrubu {
    BILINMIYOR, APozitif, ANegatif, ABPozitif, ABNegatif, BPozitif, BNegatif, SifirPozitif, SifirNegatif
}
