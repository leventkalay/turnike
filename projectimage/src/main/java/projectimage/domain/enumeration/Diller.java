package projectimage.domain.enumeration;

/**
 * The Diller enumeration.
 */
public enum Diller {
    TURKCE, INGILIZCE, FRANSIZCA, ALMANCA, JAPONCA
}
