package projectimage.domain.enumeration;

/**
 * The Ulke enumeration.
 */
public enum Ulke {
    TURKIYE, INGILTERE, ALMANYA, JAPONYA
}
