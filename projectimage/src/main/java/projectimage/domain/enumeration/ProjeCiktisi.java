package projectimage.domain.enumeration;

/**
 * The ProjeCiktisi enumeration.
 */
public enum ProjeCiktisi {
    PROJE_OZETI, PROJE_RAPORU, DIGER
}
