package projectimage.domain.enumeration;

/**
 * The Askerlik enumeration.
 */
public enum Askerlik {
    YAPTI, YAPMADI, MUAF, TECILLI
}
