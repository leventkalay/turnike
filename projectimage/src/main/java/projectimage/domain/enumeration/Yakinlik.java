package projectimage.domain.enumeration;

/**
 * The Yakinlik enumeration.
 */
public enum Yakinlik {
    BABA, ANNE, KARDES, KIZI, OGLU, ESI, DIGER
}
