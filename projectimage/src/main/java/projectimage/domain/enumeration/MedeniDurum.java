package projectimage.domain.enumeration;

/**
 * The MedeniDurum enumeration.
 */
public enum MedeniDurum {
    EVLI, BEKAR
}
