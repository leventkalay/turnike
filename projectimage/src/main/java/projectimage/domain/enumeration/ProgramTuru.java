package projectimage.domain.enumeration;

/**
 * The ProgramTuru enumeration.
 */
public enum ProgramTuru {
    SEMINER, KURS, TOPLANTI, CALISTAY, KONFERANS, KONGRE, PANEL, OZEL_IHTISAS_KOMISYONU, DIGER
}
