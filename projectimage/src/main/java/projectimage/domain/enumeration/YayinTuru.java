package projectimage.domain.enumeration;

/**
 * The YayinTuru enumeration.
 */
public enum YayinTuru {
    OZEL, KAMU, DIGER
}
