package projectimage.domain.enumeration;

/**
 * The IstihdamSekli enumeration.
 */
public enum IstihdamSekli {
    SK_657, KHK_696
}
