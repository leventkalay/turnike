package projectimage.domain.enumeration;

/**
 * The DetayBilgiler enumeration.
 */
public enum DetayBilgiler {
    ALTYAPI_YATIRIMLARI, ARGE, BILGI_VE_ILETISIM_TEKONOLOJILERI, BUTCE_HAZIRLIK_VE_UYGULAMA_SURECLERI, CEVRE, EGITIM, ENERJI, FON_YONETIMI, GENCLIKVESPOR
}
