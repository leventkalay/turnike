package projectimage.domain.enumeration;

/**
 * The CalismaSekli enumeration.
 */
public enum CalismaSekli {
    TAM_ZAMANLI, YARI_ZAMANLI, STAJ
}
