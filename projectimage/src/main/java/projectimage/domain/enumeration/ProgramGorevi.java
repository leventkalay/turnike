package projectimage.domain.enumeration;

/**
 * The ProgramGorevi enumeration.
 */
public enum ProgramGorevi {
    KATILIMCI, KURSIYER, KONUSMACI, EGITICI, MODERATOR, PANELIST, SUNUCU, ORGANIZASYON_VE_PLANLAMA, DIGER
}
