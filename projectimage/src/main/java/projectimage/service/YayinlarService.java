package projectimage.service;

import projectimage.domain.Yayinlar;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Yayinlar.
 */
public interface YayinlarService {

    /**
     * Save a yayinlar.
     *
     * @param yayinlar the entity to save
     * @return the persisted entity
     */
    Yayinlar save(Yayinlar yayinlar);

    /**
     * Get all the yayinlars.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Yayinlar> findAll(Pageable pageable);


    /**
     * Get the "id" yayinlar.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Yayinlar> findOne(Long id);

    /**
     * Delete the "id" yayinlar.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
