package projectimage.service;

import projectimage.domain.OgrenimBilgileri;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing OgrenimBilgileri.
 */
public interface OgrenimBilgileriService {

    /**
     * Save a ogrenimBilgileri.
     *
     * @param ogrenimBilgileri the entity to save
     * @return the persisted entity
     */
    OgrenimBilgileri save(OgrenimBilgileri ogrenimBilgileri);

    /**
     * Get all the ogrenimBilgileris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OgrenimBilgileri> findAll(Pageable pageable);


    /**
     * Get the "id" ogrenimBilgileri.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<OgrenimBilgileri> findOne(Long id);

    /**
     * Delete the "id" ogrenimBilgileri.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
