package projectimage.service;

import projectimage.domain.Istectrubeleri;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Istectrubeleri.
 */
public interface IstectrubeleriService {

    /**
     * Save a istectrubeleri.
     *
     * @param istectrubeleri the entity to save
     * @return the persisted entity
     */
    Istectrubeleri save(Istectrubeleri istectrubeleri);

    /**
     * Get all the istectrubeleris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Istectrubeleri> findAll(Pageable pageable);


    /**
     * Get the "id" istectrubeleri.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Istectrubeleri> findOne(Long id);

    /**
     * Delete the "id" istectrubeleri.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
