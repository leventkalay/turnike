package projectimage.service;

import projectimage.domain.KisiselBilgiler;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing KisiselBilgiler.
 */
public interface KisiselBilgilerService {

    /**
     * Save a kisiselBilgiler.
     *
     * @param kisiselBilgiler the entity to save
     * @return the persisted entity
     */
    KisiselBilgiler save(KisiselBilgiler kisiselBilgiler);

    /**
     * Get all the kisiselBilgilers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<KisiselBilgiler> findAll(Pageable pageable);


    /**
     * Get the "id" kisiselBilgiler.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<KisiselBilgiler> findOne(Long id);

    /**
     * Delete the "id" kisiselBilgiler.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
