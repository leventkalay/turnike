package projectimage.service;

import projectimage.domain.IletisimBilgileri;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing IletisimBilgileri.
 */
public interface IletisimBilgileriService {

    /**
     * Save a iletisimBilgileri.
     *
     * @param iletisimBilgileri the entity to save
     * @return the persisted entity
     */
    IletisimBilgileri save(IletisimBilgileri iletisimBilgileri);

    /**
     * Get all the iletisimBilgileris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<IletisimBilgileri> findAll(Pageable pageable);


    /**
     * Get the "id" iletisimBilgileri.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<IletisimBilgileri> findOne(Long id);

    /**
     * Delete the "id" iletisimBilgileri.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
