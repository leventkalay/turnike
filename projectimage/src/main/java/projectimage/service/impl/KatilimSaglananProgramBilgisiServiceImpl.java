package projectimage.service.impl;

import projectimage.service.KatilimSaglananProgramBilgisiService;
import projectimage.domain.KatilimSaglananProgramBilgisi;
import projectimage.repository.KatilimSaglananProgramBilgisiRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing KatilimSaglananProgramBilgisi.
 */
@Service
@Transactional
public class KatilimSaglananProgramBilgisiServiceImpl implements KatilimSaglananProgramBilgisiService {

    private final Logger log = LoggerFactory.getLogger(KatilimSaglananProgramBilgisiServiceImpl.class);

    private final KatilimSaglananProgramBilgisiRepository katilimSaglananProgramBilgisiRepository;

    public KatilimSaglananProgramBilgisiServiceImpl(KatilimSaglananProgramBilgisiRepository katilimSaglananProgramBilgisiRepository) {
        this.katilimSaglananProgramBilgisiRepository = katilimSaglananProgramBilgisiRepository;
    }

    /**
     * Save a katilimSaglananProgramBilgisi.
     *
     * @param katilimSaglananProgramBilgisi the entity to save
     * @return the persisted entity
     */
    @Override
    public KatilimSaglananProgramBilgisi save(KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi) {
        log.debug("Request to save KatilimSaglananProgramBilgisi : {}", katilimSaglananProgramBilgisi);
        return katilimSaglananProgramBilgisiRepository.save(katilimSaglananProgramBilgisi);
    }

    /**
     * Get all the katilimSaglananProgramBilgisis.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KatilimSaglananProgramBilgisi> findAll(Pageable pageable) {
        log.debug("Request to get all KatilimSaglananProgramBilgisis");
        return katilimSaglananProgramBilgisiRepository.findAll(pageable);
    }


    /**
     * Get one katilimSaglananProgramBilgisi by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KatilimSaglananProgramBilgisi> findOne(Long id) {
        log.debug("Request to get KatilimSaglananProgramBilgisi : {}", id);
        return katilimSaglananProgramBilgisiRepository.findById(id);
    }

    /**
     * Delete the katilimSaglananProgramBilgisi by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete KatilimSaglananProgramBilgisi : {}", id);
        katilimSaglananProgramBilgisiRepository.deleteById(id);
    }
}
