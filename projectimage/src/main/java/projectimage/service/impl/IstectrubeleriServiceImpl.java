package projectimage.service.impl;

import projectimage.service.IstectrubeleriService;
import projectimage.domain.Istectrubeleri;
import projectimage.repository.IstectrubeleriRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Istectrubeleri.
 */
@Service
@Transactional
public class IstectrubeleriServiceImpl implements IstectrubeleriService {

    private final Logger log = LoggerFactory.getLogger(IstectrubeleriServiceImpl.class);

    private final IstectrubeleriRepository istectrubeleriRepository;

    public IstectrubeleriServiceImpl(IstectrubeleriRepository istectrubeleriRepository) {
        this.istectrubeleriRepository = istectrubeleriRepository;
    }

    /**
     * Save a istectrubeleri.
     *
     * @param istectrubeleri the entity to save
     * @return the persisted entity
     */
    @Override
    public Istectrubeleri save(Istectrubeleri istectrubeleri) {
        log.debug("Request to save Istectrubeleri : {}", istectrubeleri);
        return istectrubeleriRepository.save(istectrubeleri);
    }

    /**
     * Get all the istectrubeleris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Istectrubeleri> findAll(Pageable pageable) {
        log.debug("Request to get all Istectrubeleris");
        return istectrubeleriRepository.findAll(pageable);
    }


    /**
     * Get one istectrubeleri by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Istectrubeleri> findOne(Long id) {
        log.debug("Request to get Istectrubeleri : {}", id);
        return istectrubeleriRepository.findById(id);
    }

    /**
     * Delete the istectrubeleri by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Istectrubeleri : {}", id);
        istectrubeleriRepository.deleteById(id);
    }
}
