package projectimage.service.impl;

import projectimage.service.UzmanlikAlanlariService;
import projectimage.domain.UzmanlikAlanlari;
import projectimage.repository.UzmanlikAlanlariRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing UzmanlikAlanlari.
 */
@Service
@Transactional
public class UzmanlikAlanlariServiceImpl implements UzmanlikAlanlariService {

    private final Logger log = LoggerFactory.getLogger(UzmanlikAlanlariServiceImpl.class);

    private final UzmanlikAlanlariRepository uzmanlikAlanlariRepository;

    public UzmanlikAlanlariServiceImpl(UzmanlikAlanlariRepository uzmanlikAlanlariRepository) {
        this.uzmanlikAlanlariRepository = uzmanlikAlanlariRepository;
    }

    /**
     * Save a uzmanlikAlanlari.
     *
     * @param uzmanlikAlanlari the entity to save
     * @return the persisted entity
     */
    @Override
    public UzmanlikAlanlari save(UzmanlikAlanlari uzmanlikAlanlari) {
        log.debug("Request to save UzmanlikAlanlari : {}", uzmanlikAlanlari);
        return uzmanlikAlanlariRepository.save(uzmanlikAlanlari);
    }

    /**
     * Get all the uzmanlikAlanlaris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UzmanlikAlanlari> findAll(Pageable pageable) {
        log.debug("Request to get all UzmanlikAlanlaris");
        return uzmanlikAlanlariRepository.findAll(pageable);
    }


    /**
     * Get one uzmanlikAlanlari by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UzmanlikAlanlari> findOne(Long id) {
        log.debug("Request to get UzmanlikAlanlari : {}", id);
        return uzmanlikAlanlariRepository.findById(id);
    }

    /**
     * Delete the uzmanlikAlanlari by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UzmanlikAlanlari : {}", id);
        uzmanlikAlanlariRepository.deleteById(id);
    }
}
