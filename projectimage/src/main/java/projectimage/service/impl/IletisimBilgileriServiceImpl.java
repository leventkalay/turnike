package projectimage.service.impl;

import projectimage.service.IletisimBilgileriService;
import projectimage.domain.IletisimBilgileri;
import projectimage.repository.IletisimBilgileriRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing IletisimBilgileri.
 */
@Service
@Transactional
public class IletisimBilgileriServiceImpl implements IletisimBilgileriService {

    private final Logger log = LoggerFactory.getLogger(IletisimBilgileriServiceImpl.class);

    private final IletisimBilgileriRepository iletisimBilgileriRepository;

    public IletisimBilgileriServiceImpl(IletisimBilgileriRepository iletisimBilgileriRepository) {
        this.iletisimBilgileriRepository = iletisimBilgileriRepository;
    }

    /**
     * Save a iletisimBilgileri.
     *
     * @param iletisimBilgileri the entity to save
     * @return the persisted entity
     */
    @Override
    public IletisimBilgileri save(IletisimBilgileri iletisimBilgileri) {
        log.debug("Request to save IletisimBilgileri : {}", iletisimBilgileri);
        return iletisimBilgileriRepository.save(iletisimBilgileri);
    }

    /**
     * Get all the iletisimBilgileris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<IletisimBilgileri> findAll(Pageable pageable) {
        log.debug("Request to get all IletisimBilgileris");
        return iletisimBilgileriRepository.findAll(pageable);
    }


    /**
     * Get one iletisimBilgileri by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<IletisimBilgileri> findOne(Long id) {
        log.debug("Request to get IletisimBilgileri : {}", id);
        return iletisimBilgileriRepository.findById(id);
    }

    /**
     * Delete the iletisimBilgileri by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete IletisimBilgileri : {}", id);
        iletisimBilgileriRepository.deleteById(id);
    }
}
