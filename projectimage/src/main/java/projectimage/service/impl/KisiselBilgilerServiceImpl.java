package projectimage.service.impl;

import projectimage.service.KisiselBilgilerService;
import projectimage.domain.KisiselBilgiler;
import projectimage.repository.KisiselBilgilerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing KisiselBilgiler.
 */
@Service
@Transactional
public class KisiselBilgilerServiceImpl implements KisiselBilgilerService {

    private final Logger log = LoggerFactory.getLogger(KisiselBilgilerServiceImpl.class);

    private final KisiselBilgilerRepository kisiselBilgilerRepository;

    public KisiselBilgilerServiceImpl(KisiselBilgilerRepository kisiselBilgilerRepository) {
        this.kisiselBilgilerRepository = kisiselBilgilerRepository;
    }

    /**
     * Save a kisiselBilgiler.
     *
     * @param kisiselBilgiler the entity to save
     * @return the persisted entity
     */
    @Override
    public KisiselBilgiler save(KisiselBilgiler kisiselBilgiler) {
        log.debug("Request to save KisiselBilgiler : {}", kisiselBilgiler);
        return kisiselBilgilerRepository.save(kisiselBilgiler);
    }

    /**
     * Get all the kisiselBilgilers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KisiselBilgiler> findAll(Pageable pageable) {
        log.debug("Request to get all KisiselBilgilers");
        return kisiselBilgilerRepository.findAll(pageable);
    }


    /**
     * Get one kisiselBilgiler by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KisiselBilgiler> findOne(Long id) {
        log.debug("Request to get KisiselBilgiler : {}", id);
        return kisiselBilgilerRepository.findById(id);
    }

    /**
     * Delete the kisiselBilgiler by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete KisiselBilgiler : {}", id);
        kisiselBilgilerRepository.deleteById(id);
    }
}
