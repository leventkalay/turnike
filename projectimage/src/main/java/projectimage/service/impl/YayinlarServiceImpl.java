package projectimage.service.impl;

import projectimage.service.YayinlarService;
import projectimage.domain.Yayinlar;
import projectimage.repository.YayinlarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Yayinlar.
 */
@Service
@Transactional
public class YayinlarServiceImpl implements YayinlarService {

    private final Logger log = LoggerFactory.getLogger(YayinlarServiceImpl.class);

    private final YayinlarRepository yayinlarRepository;

    public YayinlarServiceImpl(YayinlarRepository yayinlarRepository) {
        this.yayinlarRepository = yayinlarRepository;
    }

    /**
     * Save a yayinlar.
     *
     * @param yayinlar the entity to save
     * @return the persisted entity
     */
    @Override
    public Yayinlar save(Yayinlar yayinlar) {
        log.debug("Request to save Yayinlar : {}", yayinlar);
        return yayinlarRepository.save(yayinlar);
    }

    /**
     * Get all the yayinlars.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Yayinlar> findAll(Pageable pageable) {
        log.debug("Request to get all Yayinlars");
        return yayinlarRepository.findAll(pageable);
    }


    /**
     * Get one yayinlar by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Yayinlar> findOne(Long id) {
        log.debug("Request to get Yayinlar : {}", id);
        return yayinlarRepository.findById(id);
    }

    /**
     * Delete the yayinlar by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Yayinlar : {}", id);
        yayinlarRepository.deleteById(id);
    }
}
