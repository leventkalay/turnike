package projectimage.service.impl;

import projectimage.service.OzelIlgiAlanlariService;
import projectimage.domain.OzelIlgiAlanlari;
import projectimage.repository.OzelIlgiAlanlariRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing OzelIlgiAlanlari.
 */
@Service
@Transactional
public class OzelIlgiAlanlariServiceImpl implements OzelIlgiAlanlariService {

    private final Logger log = LoggerFactory.getLogger(OzelIlgiAlanlariServiceImpl.class);

    private final OzelIlgiAlanlariRepository ozelIlgiAlanlariRepository;

    public OzelIlgiAlanlariServiceImpl(OzelIlgiAlanlariRepository ozelIlgiAlanlariRepository) {
        this.ozelIlgiAlanlariRepository = ozelIlgiAlanlariRepository;
    }

    /**
     * Save a ozelIlgiAlanlari.
     *
     * @param ozelIlgiAlanlari the entity to save
     * @return the persisted entity
     */
    @Override
    public OzelIlgiAlanlari save(OzelIlgiAlanlari ozelIlgiAlanlari) {
        log.debug("Request to save OzelIlgiAlanlari : {}", ozelIlgiAlanlari);
        return ozelIlgiAlanlariRepository.save(ozelIlgiAlanlari);
    }

    /**
     * Get all the ozelIlgiAlanlaris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OzelIlgiAlanlari> findAll(Pageable pageable) {
        log.debug("Request to get all OzelIlgiAlanlaris");
        return ozelIlgiAlanlariRepository.findAll(pageable);
    }


    /**
     * Get one ozelIlgiAlanlari by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OzelIlgiAlanlari> findOne(Long id) {
        log.debug("Request to get OzelIlgiAlanlari : {}", id);
        return ozelIlgiAlanlariRepository.findById(id);
    }

    /**
     * Delete the ozelIlgiAlanlari by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OzelIlgiAlanlari : {}", id);
        ozelIlgiAlanlariRepository.deleteById(id);
    }
}
