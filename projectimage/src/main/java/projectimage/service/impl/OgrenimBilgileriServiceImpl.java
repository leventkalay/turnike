package projectimage.service.impl;

import projectimage.service.OgrenimBilgileriService;
import projectimage.domain.OgrenimBilgileri;
import projectimage.repository.OgrenimBilgileriRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing OgrenimBilgileri.
 */
@Service
@Transactional
public class OgrenimBilgileriServiceImpl implements OgrenimBilgileriService {

    private final Logger log = LoggerFactory.getLogger(OgrenimBilgileriServiceImpl.class);

    private final OgrenimBilgileriRepository ogrenimBilgileriRepository;

    public OgrenimBilgileriServiceImpl(OgrenimBilgileriRepository ogrenimBilgileriRepository) {
        this.ogrenimBilgileriRepository = ogrenimBilgileriRepository;
    }

    /**
     * Save a ogrenimBilgileri.
     *
     * @param ogrenimBilgileri the entity to save
     * @return the persisted entity
     */
    @Override
    public OgrenimBilgileri save(OgrenimBilgileri ogrenimBilgileri) {
        log.debug("Request to save OgrenimBilgileri : {}", ogrenimBilgileri);
        return ogrenimBilgileriRepository.save(ogrenimBilgileri);
    }

    /**
     * Get all the ogrenimBilgileris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OgrenimBilgileri> findAll(Pageable pageable) {
        log.debug("Request to get all OgrenimBilgileris");
        return ogrenimBilgileriRepository.findAll(pageable);
    }


    /**
     * Get one ogrenimBilgileri by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OgrenimBilgileri> findOne(Long id) {
        log.debug("Request to get OgrenimBilgileri : {}", id);
        return ogrenimBilgileriRepository.findById(id);
    }

    /**
     * Delete the ogrenimBilgileri by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OgrenimBilgileri : {}", id);
        ogrenimBilgileriRepository.deleteById(id);
    }
}
