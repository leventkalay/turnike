package projectimage.service.impl;

import projectimage.service.YabanciDilBilgileriService;
import projectimage.domain.YabanciDilBilgileri;
import projectimage.repository.YabanciDilBilgileriRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing YabanciDilBilgileri.
 */
@Service
@Transactional
public class YabanciDilBilgileriServiceImpl implements YabanciDilBilgileriService {

    private final Logger log = LoggerFactory.getLogger(YabanciDilBilgileriServiceImpl.class);

    private final YabanciDilBilgileriRepository yabanciDilBilgileriRepository;

    public YabanciDilBilgileriServiceImpl(YabanciDilBilgileriRepository yabanciDilBilgileriRepository) {
        this.yabanciDilBilgileriRepository = yabanciDilBilgileriRepository;
    }

    /**
     * Save a yabanciDilBilgileri.
     *
     * @param yabanciDilBilgileri the entity to save
     * @return the persisted entity
     */
    @Override
    public YabanciDilBilgileri save(YabanciDilBilgileri yabanciDilBilgileri) {
        log.debug("Request to save YabanciDilBilgileri : {}", yabanciDilBilgileri);
        return yabanciDilBilgileriRepository.save(yabanciDilBilgileri);
    }

    /**
     * Get all the yabanciDilBilgileris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<YabanciDilBilgileri> findAll(Pageable pageable) {
        log.debug("Request to get all YabanciDilBilgileris");
        return yabanciDilBilgileriRepository.findAll(pageable);
    }


    /**
     * Get one yabanciDilBilgileri by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<YabanciDilBilgileri> findOne(Long id) {
        log.debug("Request to get YabanciDilBilgileri : {}", id);
        return yabanciDilBilgileriRepository.findById(id);
    }

    /**
     * Delete the yabanciDilBilgileri by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete YabanciDilBilgileri : {}", id);
        yabanciDilBilgileriRepository.deleteById(id);
    }
}
