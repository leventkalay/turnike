package projectimage.service.impl;

import projectimage.service.EgiticiYetkinlikleriService;
import projectimage.domain.EgiticiYetkinlikleri;
import projectimage.repository.EgiticiYetkinlikleriRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing EgiticiYetkinlikleri.
 */
@Service
@Transactional
public class EgiticiYetkinlikleriServiceImpl implements EgiticiYetkinlikleriService {

    private final Logger log = LoggerFactory.getLogger(EgiticiYetkinlikleriServiceImpl.class);

    private final EgiticiYetkinlikleriRepository egiticiYetkinlikleriRepository;

    public EgiticiYetkinlikleriServiceImpl(EgiticiYetkinlikleriRepository egiticiYetkinlikleriRepository) {
        this.egiticiYetkinlikleriRepository = egiticiYetkinlikleriRepository;
    }

    /**
     * Save a egiticiYetkinlikleri.
     *
     * @param egiticiYetkinlikleri the entity to save
     * @return the persisted entity
     */
    @Override
    public EgiticiYetkinlikleri save(EgiticiYetkinlikleri egiticiYetkinlikleri) {
        log.debug("Request to save EgiticiYetkinlikleri : {}", egiticiYetkinlikleri);
        return egiticiYetkinlikleriRepository.save(egiticiYetkinlikleri);
    }

    /**
     * Get all the egiticiYetkinlikleris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EgiticiYetkinlikleri> findAll(Pageable pageable) {
        log.debug("Request to get all EgiticiYetkinlikleris");
        return egiticiYetkinlikleriRepository.findAll(pageable);
    }


    /**
     * Get one egiticiYetkinlikleri by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EgiticiYetkinlikleri> findOne(Long id) {
        log.debug("Request to get EgiticiYetkinlikleri : {}", id);
        return egiticiYetkinlikleriRepository.findById(id);
    }

    /**
     * Delete the egiticiYetkinlikleri by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EgiticiYetkinlikleri : {}", id);
        egiticiYetkinlikleriRepository.deleteById(id);
    }
}
