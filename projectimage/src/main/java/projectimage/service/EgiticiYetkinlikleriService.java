package projectimage.service;

import projectimage.domain.EgiticiYetkinlikleri;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing EgiticiYetkinlikleri.
 */
public interface EgiticiYetkinlikleriService {

    /**
     * Save a egiticiYetkinlikleri.
     *
     * @param egiticiYetkinlikleri the entity to save
     * @return the persisted entity
     */
    EgiticiYetkinlikleri save(EgiticiYetkinlikleri egiticiYetkinlikleri);

    /**
     * Get all the egiticiYetkinlikleris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EgiticiYetkinlikleri> findAll(Pageable pageable);


    /**
     * Get the "id" egiticiYetkinlikleri.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<EgiticiYetkinlikleri> findOne(Long id);

    /**
     * Delete the "id" egiticiYetkinlikleri.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
