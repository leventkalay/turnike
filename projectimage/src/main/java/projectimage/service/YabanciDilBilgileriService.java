package projectimage.service;

import projectimage.domain.YabanciDilBilgileri;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing YabanciDilBilgileri.
 */
public interface YabanciDilBilgileriService {

    /**
     * Save a yabanciDilBilgileri.
     *
     * @param yabanciDilBilgileri the entity to save
     * @return the persisted entity
     */
    YabanciDilBilgileri save(YabanciDilBilgileri yabanciDilBilgileri);

    /**
     * Get all the yabanciDilBilgileris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<YabanciDilBilgileri> findAll(Pageable pageable);


    /**
     * Get the "id" yabanciDilBilgileri.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<YabanciDilBilgileri> findOne(Long id);

    /**
     * Delete the "id" yabanciDilBilgileri.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
