package projectimage.service;

import projectimage.domain.KatilimSaglananProgramBilgisi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing KatilimSaglananProgramBilgisi.
 */
public interface KatilimSaglananProgramBilgisiService {

    /**
     * Save a katilimSaglananProgramBilgisi.
     *
     * @param katilimSaglananProgramBilgisi the entity to save
     * @return the persisted entity
     */
    KatilimSaglananProgramBilgisi save(KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi);

    /**
     * Get all the katilimSaglananProgramBilgisis.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<KatilimSaglananProgramBilgisi> findAll(Pageable pageable);


    /**
     * Get the "id" katilimSaglananProgramBilgisi.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<KatilimSaglananProgramBilgisi> findOne(Long id);

    /**
     * Delete the "id" katilimSaglananProgramBilgisi.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
