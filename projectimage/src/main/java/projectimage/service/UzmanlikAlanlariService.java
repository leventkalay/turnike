package projectimage.service;

import projectimage.domain.UzmanlikAlanlari;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing UzmanlikAlanlari.
 */
public interface UzmanlikAlanlariService {

    /**
     * Save a uzmanlikAlanlari.
     *
     * @param uzmanlikAlanlari the entity to save
     * @return the persisted entity
     */
    UzmanlikAlanlari save(UzmanlikAlanlari uzmanlikAlanlari);

    /**
     * Get all the uzmanlikAlanlaris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<UzmanlikAlanlari> findAll(Pageable pageable);


    /**
     * Get the "id" uzmanlikAlanlari.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<UzmanlikAlanlari> findOne(Long id);

    /**
     * Delete the "id" uzmanlikAlanlari.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
