package projectimage.service;

import projectimage.domain.OzelIlgiAlanlari;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing OzelIlgiAlanlari.
 */
public interface OzelIlgiAlanlariService {

    /**
     * Save a ozelIlgiAlanlari.
     *
     * @param ozelIlgiAlanlari the entity to save
     * @return the persisted entity
     */
    OzelIlgiAlanlari save(OzelIlgiAlanlari ozelIlgiAlanlari);

    /**
     * Get all the ozelIlgiAlanlaris.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OzelIlgiAlanlari> findAll(Pageable pageable);


    /**
     * Get the "id" ozelIlgiAlanlari.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<OzelIlgiAlanlari> findOne(Long id);

    /**
     * Delete the "id" ozelIlgiAlanlari.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
