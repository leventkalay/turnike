package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.EgiticiYetkinlikleri;
import projectimage.service.EgiticiYetkinlikleriService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EgiticiYetkinlikleri.
 */
@RestController
@RequestMapping("/api")
public class EgiticiYetkinlikleriResource {

    private final Logger log = LoggerFactory.getLogger(EgiticiYetkinlikleriResource.class);

    private static final String ENTITY_NAME = "egiticiYetkinlikleri";

    private final EgiticiYetkinlikleriService egiticiYetkinlikleriService;

    public EgiticiYetkinlikleriResource(EgiticiYetkinlikleriService egiticiYetkinlikleriService) {
        this.egiticiYetkinlikleriService = egiticiYetkinlikleriService;
    }

    /**
     * POST  /egitici-yetkinlikleris : Create a new egiticiYetkinlikleri.
     *
     * @param egiticiYetkinlikleri the egiticiYetkinlikleri to create
     * @return the ResponseEntity with status 201 (Created) and with body the new egiticiYetkinlikleri, or with status 400 (Bad Request) if the egiticiYetkinlikleri has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/egitici-yetkinlikleris")
    @Timed
    public ResponseEntity<EgiticiYetkinlikleri> createEgiticiYetkinlikleri(@RequestBody EgiticiYetkinlikleri egiticiYetkinlikleri) throws URISyntaxException {
        log.debug("REST request to save EgiticiYetkinlikleri : {}", egiticiYetkinlikleri);
        if (egiticiYetkinlikleri.getId() != null) {
            throw new BadRequestAlertException("A new egiticiYetkinlikleri cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EgiticiYetkinlikleri result = egiticiYetkinlikleriService.save(egiticiYetkinlikleri);
        return ResponseEntity.created(new URI("/api/egitici-yetkinlikleris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /egitici-yetkinlikleris : Updates an existing egiticiYetkinlikleri.
     *
     * @param egiticiYetkinlikleri the egiticiYetkinlikleri to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated egiticiYetkinlikleri,
     * or with status 400 (Bad Request) if the egiticiYetkinlikleri is not valid,
     * or with status 500 (Internal Server Error) if the egiticiYetkinlikleri couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/egitici-yetkinlikleris")
    @Timed
    public ResponseEntity<EgiticiYetkinlikleri> updateEgiticiYetkinlikleri(@RequestBody EgiticiYetkinlikleri egiticiYetkinlikleri) throws URISyntaxException {
        log.debug("REST request to update EgiticiYetkinlikleri : {}", egiticiYetkinlikleri);
        if (egiticiYetkinlikleri.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EgiticiYetkinlikleri result = egiticiYetkinlikleriService.save(egiticiYetkinlikleri);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, egiticiYetkinlikleri.getId().toString()))
            .body(result);
    }

    /**
     * GET  /egitici-yetkinlikleris : get all the egiticiYetkinlikleris.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of egiticiYetkinlikleris in body
     */
    @GetMapping("/egitici-yetkinlikleris")
    @Timed
    public ResponseEntity<List<EgiticiYetkinlikleri>> getAllEgiticiYetkinlikleris(Pageable pageable) {
        log.debug("REST request to get a page of EgiticiYetkinlikleris");
        Page<EgiticiYetkinlikleri> page = egiticiYetkinlikleriService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/egitici-yetkinlikleris");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /egitici-yetkinlikleris/:id : get the "id" egiticiYetkinlikleri.
     *
     * @param id the id of the egiticiYetkinlikleri to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the egiticiYetkinlikleri, or with status 404 (Not Found)
     */
    @GetMapping("/egitici-yetkinlikleris/{id}")
    @Timed
    public ResponseEntity<EgiticiYetkinlikleri> getEgiticiYetkinlikleri(@PathVariable Long id) {
        log.debug("REST request to get EgiticiYetkinlikleri : {}", id);
        Optional<EgiticiYetkinlikleri> egiticiYetkinlikleri = egiticiYetkinlikleriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(egiticiYetkinlikleri);
    }

    /**
     * DELETE  /egitici-yetkinlikleris/:id : delete the "id" egiticiYetkinlikleri.
     *
     * @param id the id of the egiticiYetkinlikleri to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/egitici-yetkinlikleris/{id}")
    @Timed
    public ResponseEntity<Void> deleteEgiticiYetkinlikleri(@PathVariable Long id) {
        log.debug("REST request to delete EgiticiYetkinlikleri : {}", id);
        egiticiYetkinlikleriService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
