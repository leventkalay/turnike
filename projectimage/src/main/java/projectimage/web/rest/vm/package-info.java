/**
 * View Models used by Spring MVC REST controllers.
 */
package projectimage.web.rest.vm;
