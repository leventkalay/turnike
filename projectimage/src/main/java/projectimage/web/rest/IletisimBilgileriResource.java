package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.IletisimBilgileri;
import projectimage.service.IletisimBilgileriService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing IletisimBilgileri.
 */
@RestController
@RequestMapping("/api")
public class IletisimBilgileriResource {

    private final Logger log = LoggerFactory.getLogger(IletisimBilgileriResource.class);

    private static final String ENTITY_NAME = "iletisimBilgileri";

    private final IletisimBilgileriService iletisimBilgileriService;

    public IletisimBilgileriResource(IletisimBilgileriService iletisimBilgileriService) {
        this.iletisimBilgileriService = iletisimBilgileriService;
    }

    /**
     * POST  /iletisim-bilgileris : Create a new iletisimBilgileri.
     *
     * @param iletisimBilgileri the iletisimBilgileri to create
     * @return the ResponseEntity with status 201 (Created) and with body the new iletisimBilgileri, or with status 400 (Bad Request) if the iletisimBilgileri has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/iletisim-bilgileris")
    @Timed
    public ResponseEntity<IletisimBilgileri> createIletisimBilgileri(@Valid @RequestBody IletisimBilgileri iletisimBilgileri) throws URISyntaxException {
        log.debug("REST request to save IletisimBilgileri : {}", iletisimBilgileri);
        if (iletisimBilgileri.getId() != null) {
            throw new BadRequestAlertException("A new iletisimBilgileri cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IletisimBilgileri result = iletisimBilgileriService.save(iletisimBilgileri);
        return ResponseEntity.created(new URI("/api/iletisim-bilgileris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /iletisim-bilgileris : Updates an existing iletisimBilgileri.
     *
     * @param iletisimBilgileri the iletisimBilgileri to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated iletisimBilgileri,
     * or with status 400 (Bad Request) if the iletisimBilgileri is not valid,
     * or with status 500 (Internal Server Error) if the iletisimBilgileri couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/iletisim-bilgileris")
    @Timed
    public ResponseEntity<IletisimBilgileri> updateIletisimBilgileri(@Valid @RequestBody IletisimBilgileri iletisimBilgileri) throws URISyntaxException {
        log.debug("REST request to update IletisimBilgileri : {}", iletisimBilgileri);
        if (iletisimBilgileri.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IletisimBilgileri result = iletisimBilgileriService.save(iletisimBilgileri);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, iletisimBilgileri.getId().toString()))
            .body(result);
    }

    /**
     * GET  /iletisim-bilgileris : get all the iletisimBilgileris.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of iletisimBilgileris in body
     */
    @GetMapping("/iletisim-bilgileris")
    @Timed
    public ResponseEntity<List<IletisimBilgileri>> getAllIletisimBilgileris(Pageable pageable) {
        log.debug("REST request to get a page of IletisimBilgileris");
        Page<IletisimBilgileri> page = iletisimBilgileriService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/iletisim-bilgileris");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /iletisim-bilgileris/:id : get the "id" iletisimBilgileri.
     *
     * @param id the id of the iletisimBilgileri to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the iletisimBilgileri, or with status 404 (Not Found)
     */
    @GetMapping("/iletisim-bilgileris/{id}")
    @Timed
    public ResponseEntity<IletisimBilgileri> getIletisimBilgileri(@PathVariable Long id) {
        log.debug("REST request to get IletisimBilgileri : {}", id);
        Optional<IletisimBilgileri> iletisimBilgileri = iletisimBilgileriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(iletisimBilgileri);
    }

    /**
     * DELETE  /iletisim-bilgileris/:id : delete the "id" iletisimBilgileri.
     *
     * @param id the id of the iletisimBilgileri to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/iletisim-bilgileris/{id}")
    @Timed
    public ResponseEntity<Void> deleteIletisimBilgileri(@PathVariable Long id) {
        log.debug("REST request to delete IletisimBilgileri : {}", id);
        iletisimBilgileriService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
