package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.UzmanlikAlanlari;
import projectimage.service.UzmanlikAlanlariService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UzmanlikAlanlari.
 */
@RestController
@RequestMapping("/api")
public class UzmanlikAlanlariResource {

    private final Logger log = LoggerFactory.getLogger(UzmanlikAlanlariResource.class);

    private static final String ENTITY_NAME = "uzmanlikAlanlari";

    private final UzmanlikAlanlariService uzmanlikAlanlariService;

    public UzmanlikAlanlariResource(UzmanlikAlanlariService uzmanlikAlanlariService) {
        this.uzmanlikAlanlariService = uzmanlikAlanlariService;
    }

    /**
     * POST  /uzmanlik-alanlaris : Create a new uzmanlikAlanlari.
     *
     * @param uzmanlikAlanlari the uzmanlikAlanlari to create
     * @return the ResponseEntity with status 201 (Created) and with body the new uzmanlikAlanlari, or with status 400 (Bad Request) if the uzmanlikAlanlari has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/uzmanlik-alanlaris")
    @Timed
    public ResponseEntity<UzmanlikAlanlari> createUzmanlikAlanlari(@Valid @RequestBody UzmanlikAlanlari uzmanlikAlanlari) throws URISyntaxException {
        log.debug("REST request to save UzmanlikAlanlari : {}", uzmanlikAlanlari);
        if (uzmanlikAlanlari.getId() != null) {
            throw new BadRequestAlertException("A new uzmanlikAlanlari cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UzmanlikAlanlari result = uzmanlikAlanlariService.save(uzmanlikAlanlari);
        return ResponseEntity.created(new URI("/api/uzmanlik-alanlaris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /uzmanlik-alanlaris : Updates an existing uzmanlikAlanlari.
     *
     * @param uzmanlikAlanlari the uzmanlikAlanlari to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated uzmanlikAlanlari,
     * or with status 400 (Bad Request) if the uzmanlikAlanlari is not valid,
     * or with status 500 (Internal Server Error) if the uzmanlikAlanlari couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/uzmanlik-alanlaris")
    @Timed
    public ResponseEntity<UzmanlikAlanlari> updateUzmanlikAlanlari(@Valid @RequestBody UzmanlikAlanlari uzmanlikAlanlari) throws URISyntaxException {
        log.debug("REST request to update UzmanlikAlanlari : {}", uzmanlikAlanlari);
        if (uzmanlikAlanlari.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UzmanlikAlanlari result = uzmanlikAlanlariService.save(uzmanlikAlanlari);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, uzmanlikAlanlari.getId().toString()))
            .body(result);
    }

    /**
     * GET  /uzmanlik-alanlaris : get all the uzmanlikAlanlaris.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of uzmanlikAlanlaris in body
     */
    @GetMapping("/uzmanlik-alanlaris")
    @Timed
    public ResponseEntity<List<UzmanlikAlanlari>> getAllUzmanlikAlanlaris(Pageable pageable) {
        log.debug("REST request to get a page of UzmanlikAlanlaris");
        Page<UzmanlikAlanlari> page = uzmanlikAlanlariService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/uzmanlik-alanlaris");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /uzmanlik-alanlaris/:id : get the "id" uzmanlikAlanlari.
     *
     * @param id the id of the uzmanlikAlanlari to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the uzmanlikAlanlari, or with status 404 (Not Found)
     */
    @GetMapping("/uzmanlik-alanlaris/{id}")
    @Timed
    public ResponseEntity<UzmanlikAlanlari> getUzmanlikAlanlari(@PathVariable Long id) {
        log.debug("REST request to get UzmanlikAlanlari : {}", id);
        Optional<UzmanlikAlanlari> uzmanlikAlanlari = uzmanlikAlanlariService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uzmanlikAlanlari);
    }

    /**
     * DELETE  /uzmanlik-alanlaris/:id : delete the "id" uzmanlikAlanlari.
     *
     * @param id the id of the uzmanlikAlanlari to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/uzmanlik-alanlaris/{id}")
    @Timed
    public ResponseEntity<Void> deleteUzmanlikAlanlari(@PathVariable Long id) {
        log.debug("REST request to delete UzmanlikAlanlari : {}", id);
        uzmanlikAlanlariService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
