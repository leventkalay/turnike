package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.YabanciDilBilgileri;
import projectimage.service.YabanciDilBilgileriService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing YabanciDilBilgileri.
 */
@RestController
@RequestMapping("/api")
public class YabanciDilBilgileriResource {

    private final Logger log = LoggerFactory.getLogger(YabanciDilBilgileriResource.class);

    private static final String ENTITY_NAME = "yabanciDilBilgileri";

    private final YabanciDilBilgileriService yabanciDilBilgileriService;

    public YabanciDilBilgileriResource(YabanciDilBilgileriService yabanciDilBilgileriService) {
        this.yabanciDilBilgileriService = yabanciDilBilgileriService;
    }

    /**
     * POST  /yabanci-dil-bilgileris : Create a new yabanciDilBilgileri.
     *
     * @param yabanciDilBilgileri the yabanciDilBilgileri to create
     * @return the ResponseEntity with status 201 (Created) and with body the new yabanciDilBilgileri, or with status 400 (Bad Request) if the yabanciDilBilgileri has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/yabanci-dil-bilgileris")
    @Timed
    public ResponseEntity<YabanciDilBilgileri> createYabanciDilBilgileri(@Valid @RequestBody YabanciDilBilgileri yabanciDilBilgileri) throws URISyntaxException {
        log.debug("REST request to save YabanciDilBilgileri : {}", yabanciDilBilgileri);
        if (yabanciDilBilgileri.getId() != null) {
            throw new BadRequestAlertException("A new yabanciDilBilgileri cannot already have an ID", ENTITY_NAME, "idexists");
        }
        YabanciDilBilgileri result = yabanciDilBilgileriService.save(yabanciDilBilgileri);
        return ResponseEntity.created(new URI("/api/yabanci-dil-bilgileris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /yabanci-dil-bilgileris : Updates an existing yabanciDilBilgileri.
     *
     * @param yabanciDilBilgileri the yabanciDilBilgileri to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated yabanciDilBilgileri,
     * or with status 400 (Bad Request) if the yabanciDilBilgileri is not valid,
     * or with status 500 (Internal Server Error) if the yabanciDilBilgileri couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/yabanci-dil-bilgileris")
    @Timed
    public ResponseEntity<YabanciDilBilgileri> updateYabanciDilBilgileri(@Valid @RequestBody YabanciDilBilgileri yabanciDilBilgileri) throws URISyntaxException {
        log.debug("REST request to update YabanciDilBilgileri : {}", yabanciDilBilgileri);
        if (yabanciDilBilgileri.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        YabanciDilBilgileri result = yabanciDilBilgileriService.save(yabanciDilBilgileri);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, yabanciDilBilgileri.getId().toString()))
            .body(result);
    }

    /**
     * GET  /yabanci-dil-bilgileris : get all the yabanciDilBilgileris.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of yabanciDilBilgileris in body
     */
    @GetMapping("/yabanci-dil-bilgileris")
    @Timed
    public ResponseEntity<List<YabanciDilBilgileri>> getAllYabanciDilBilgileris(Pageable pageable) {
        log.debug("REST request to get a page of YabanciDilBilgileris");
        Page<YabanciDilBilgileri> page = yabanciDilBilgileriService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/yabanci-dil-bilgileris");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /yabanci-dil-bilgileris/:id : get the "id" yabanciDilBilgileri.
     *
     * @param id the id of the yabanciDilBilgileri to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the yabanciDilBilgileri, or with status 404 (Not Found)
     */
    @GetMapping("/yabanci-dil-bilgileris/{id}")
    @Timed
    public ResponseEntity<YabanciDilBilgileri> getYabanciDilBilgileri(@PathVariable Long id) {
        log.debug("REST request to get YabanciDilBilgileri : {}", id);
        Optional<YabanciDilBilgileri> yabanciDilBilgileri = yabanciDilBilgileriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(yabanciDilBilgileri);
    }

    /**
     * DELETE  /yabanci-dil-bilgileris/:id : delete the "id" yabanciDilBilgileri.
     *
     * @param id the id of the yabanciDilBilgileri to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/yabanci-dil-bilgileris/{id}")
    @Timed
    public ResponseEntity<Void> deleteYabanciDilBilgileri(@PathVariable Long id) {
        log.debug("REST request to delete YabanciDilBilgileri : {}", id);
        yabanciDilBilgileriService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
