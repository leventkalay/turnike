package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.Istectrubeleri;
import projectimage.service.IstectrubeleriService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Istectrubeleri.
 */
@RestController
@RequestMapping("/api")
public class IstectrubeleriResource {

    private final Logger log = LoggerFactory.getLogger(IstectrubeleriResource.class);

    private static final String ENTITY_NAME = "istectrubeleri";

    private final IstectrubeleriService istectrubeleriService;

    public IstectrubeleriResource(IstectrubeleriService istectrubeleriService) {
        this.istectrubeleriService = istectrubeleriService;
    }

    /**
     * POST  /istectrubeleris : Create a new istectrubeleri.
     *
     * @param istectrubeleri the istectrubeleri to create
     * @return the ResponseEntity with status 201 (Created) and with body the new istectrubeleri, or with status 400 (Bad Request) if the istectrubeleri has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/istectrubeleris")
    @Timed
    public ResponseEntity<Istectrubeleri> createIstectrubeleri(@Valid @RequestBody Istectrubeleri istectrubeleri) throws URISyntaxException {
        log.debug("REST request to save Istectrubeleri : {}", istectrubeleri);
        if (istectrubeleri.getId() != null) {
            throw new BadRequestAlertException("A new istectrubeleri cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Istectrubeleri result = istectrubeleriService.save(istectrubeleri);
        return ResponseEntity.created(new URI("/api/istectrubeleris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /istectrubeleris : Updates an existing istectrubeleri.
     *
     * @param istectrubeleri the istectrubeleri to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated istectrubeleri,
     * or with status 400 (Bad Request) if the istectrubeleri is not valid,
     * or with status 500 (Internal Server Error) if the istectrubeleri couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/istectrubeleris")
    @Timed
    public ResponseEntity<Istectrubeleri> updateIstectrubeleri(@Valid @RequestBody Istectrubeleri istectrubeleri) throws URISyntaxException {
        log.debug("REST request to update Istectrubeleri : {}", istectrubeleri);
        if (istectrubeleri.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Istectrubeleri result = istectrubeleriService.save(istectrubeleri);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, istectrubeleri.getId().toString()))
            .body(result);
    }

    /**
     * GET  /istectrubeleris : get all the istectrubeleris.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of istectrubeleris in body
     */
    @GetMapping("/istectrubeleris")
    @Timed
    public ResponseEntity<List<Istectrubeleri>> getAllIstectrubeleris(Pageable pageable) {
        log.debug("REST request to get a page of Istectrubeleris");
        Page<Istectrubeleri> page = istectrubeleriService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/istectrubeleris");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /istectrubeleris/:id : get the "id" istectrubeleri.
     *
     * @param id the id of the istectrubeleri to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the istectrubeleri, or with status 404 (Not Found)
     */
    @GetMapping("/istectrubeleris/{id}")
    @Timed
    public ResponseEntity<Istectrubeleri> getIstectrubeleri(@PathVariable Long id) {
        log.debug("REST request to get Istectrubeleri : {}", id);
        Optional<Istectrubeleri> istectrubeleri = istectrubeleriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(istectrubeleri);
    }

    /**
     * DELETE  /istectrubeleris/:id : delete the "id" istectrubeleri.
     *
     * @param id the id of the istectrubeleri to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/istectrubeleris/{id}")
    @Timed
    public ResponseEntity<Void> deleteIstectrubeleri(@PathVariable Long id) {
        log.debug("REST request to delete Istectrubeleri : {}", id);
        istectrubeleriService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
