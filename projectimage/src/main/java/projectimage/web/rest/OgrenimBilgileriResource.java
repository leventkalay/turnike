package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.OgrenimBilgileri;
import projectimage.service.OgrenimBilgileriService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OgrenimBilgileri.
 */
@RestController
@RequestMapping("/api")
public class OgrenimBilgileriResource {

    private final Logger log = LoggerFactory.getLogger(OgrenimBilgileriResource.class);

    private static final String ENTITY_NAME = "ogrenimBilgileri";

    private final OgrenimBilgileriService ogrenimBilgileriService;

    public OgrenimBilgileriResource(OgrenimBilgileriService ogrenimBilgileriService) {
        this.ogrenimBilgileriService = ogrenimBilgileriService;
    }

    /**
     * POST  /ogrenim-bilgileris : Create a new ogrenimBilgileri.
     *
     * @param ogrenimBilgileri the ogrenimBilgileri to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ogrenimBilgileri, or with status 400 (Bad Request) if the ogrenimBilgileri has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ogrenim-bilgileris")
    @Timed
    public ResponseEntity<OgrenimBilgileri> createOgrenimBilgileri(@Valid @RequestBody OgrenimBilgileri ogrenimBilgileri) throws URISyntaxException {
        log.debug("REST request to save OgrenimBilgileri : {}", ogrenimBilgileri);
        if (ogrenimBilgileri.getId() != null) {
            throw new BadRequestAlertException("A new ogrenimBilgileri cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OgrenimBilgileri result = ogrenimBilgileriService.save(ogrenimBilgileri);
        return ResponseEntity.created(new URI("/api/ogrenim-bilgileris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ogrenim-bilgileris : Updates an existing ogrenimBilgileri.
     *
     * @param ogrenimBilgileri the ogrenimBilgileri to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ogrenimBilgileri,
     * or with status 400 (Bad Request) if the ogrenimBilgileri is not valid,
     * or with status 500 (Internal Server Error) if the ogrenimBilgileri couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ogrenim-bilgileris")
    @Timed
    public ResponseEntity<OgrenimBilgileri> updateOgrenimBilgileri(@Valid @RequestBody OgrenimBilgileri ogrenimBilgileri) throws URISyntaxException {
        log.debug("REST request to update OgrenimBilgileri : {}", ogrenimBilgileri);
        if (ogrenimBilgileri.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OgrenimBilgileri result = ogrenimBilgileriService.save(ogrenimBilgileri);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ogrenimBilgileri.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ogrenim-bilgileris : get all the ogrenimBilgileris.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ogrenimBilgileris in body
     */
    @GetMapping("/ogrenim-bilgileris")
    @Timed
    public ResponseEntity<List<OgrenimBilgileri>> getAllOgrenimBilgileris(Pageable pageable) {
        log.debug("REST request to get a page of OgrenimBilgileris");
        Page<OgrenimBilgileri> page = ogrenimBilgileriService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ogrenim-bilgileris");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /ogrenim-bilgileris/:id : get the "id" ogrenimBilgileri.
     *
     * @param id the id of the ogrenimBilgileri to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ogrenimBilgileri, or with status 404 (Not Found)
     */
    @GetMapping("/ogrenim-bilgileris/{id}")
    @Timed
    public ResponseEntity<OgrenimBilgileri> getOgrenimBilgileri(@PathVariable Long id) {
        log.debug("REST request to get OgrenimBilgileri : {}", id);
        Optional<OgrenimBilgileri> ogrenimBilgileri = ogrenimBilgileriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ogrenimBilgileri);
    }

    /**
     * DELETE  /ogrenim-bilgileris/:id : delete the "id" ogrenimBilgileri.
     *
     * @param id the id of the ogrenimBilgileri to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ogrenim-bilgileris/{id}")
    @Timed
    public ResponseEntity<Void> deleteOgrenimBilgileri(@PathVariable Long id) {
        log.debug("REST request to delete OgrenimBilgileri : {}", id);
        ogrenimBilgileriService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
