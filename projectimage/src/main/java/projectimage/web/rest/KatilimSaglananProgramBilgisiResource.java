package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.KatilimSaglananProgramBilgisi;
import projectimage.service.KatilimSaglananProgramBilgisiService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing KatilimSaglananProgramBilgisi.
 */
@RestController
@RequestMapping("/api")
public class KatilimSaglananProgramBilgisiResource {

    private final Logger log = LoggerFactory.getLogger(KatilimSaglananProgramBilgisiResource.class);

    private static final String ENTITY_NAME = "katilimSaglananProgramBilgisi";

    private final KatilimSaglananProgramBilgisiService katilimSaglananProgramBilgisiService;

    public KatilimSaglananProgramBilgisiResource(KatilimSaglananProgramBilgisiService katilimSaglananProgramBilgisiService) {
        this.katilimSaglananProgramBilgisiService = katilimSaglananProgramBilgisiService;
    }

    /**
     * POST  /katilim-saglanan-program-bilgisis : Create a new katilimSaglananProgramBilgisi.
     *
     * @param katilimSaglananProgramBilgisi the katilimSaglananProgramBilgisi to create
     * @return the ResponseEntity with status 201 (Created) and with body the new katilimSaglananProgramBilgisi, or with status 400 (Bad Request) if the katilimSaglananProgramBilgisi has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/katilim-saglanan-program-bilgisis")
    @Timed
    public ResponseEntity<KatilimSaglananProgramBilgisi> createKatilimSaglananProgramBilgisi(@Valid @RequestBody KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi) throws URISyntaxException {
        log.debug("REST request to save KatilimSaglananProgramBilgisi : {}", katilimSaglananProgramBilgisi);
        if (katilimSaglananProgramBilgisi.getId() != null) {
            throw new BadRequestAlertException("A new katilimSaglananProgramBilgisi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KatilimSaglananProgramBilgisi result = katilimSaglananProgramBilgisiService.save(katilimSaglananProgramBilgisi);
        return ResponseEntity.created(new URI("/api/katilim-saglanan-program-bilgisis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /katilim-saglanan-program-bilgisis : Updates an existing katilimSaglananProgramBilgisi.
     *
     * @param katilimSaglananProgramBilgisi the katilimSaglananProgramBilgisi to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated katilimSaglananProgramBilgisi,
     * or with status 400 (Bad Request) if the katilimSaglananProgramBilgisi is not valid,
     * or with status 500 (Internal Server Error) if the katilimSaglananProgramBilgisi couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/katilim-saglanan-program-bilgisis")
    @Timed
    public ResponseEntity<KatilimSaglananProgramBilgisi> updateKatilimSaglananProgramBilgisi(@Valid @RequestBody KatilimSaglananProgramBilgisi katilimSaglananProgramBilgisi) throws URISyntaxException {
        log.debug("REST request to update KatilimSaglananProgramBilgisi : {}", katilimSaglananProgramBilgisi);
        if (katilimSaglananProgramBilgisi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KatilimSaglananProgramBilgisi result = katilimSaglananProgramBilgisiService.save(katilimSaglananProgramBilgisi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, katilimSaglananProgramBilgisi.getId().toString()))
            .body(result);
    }

    /**
     * GET  /katilim-saglanan-program-bilgisis : get all the katilimSaglananProgramBilgisis.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of katilimSaglananProgramBilgisis in body
     */
    @GetMapping("/katilim-saglanan-program-bilgisis")
    @Timed
    public ResponseEntity<List<KatilimSaglananProgramBilgisi>> getAllKatilimSaglananProgramBilgisis(Pageable pageable) {
        log.debug("REST request to get a page of KatilimSaglananProgramBilgisis");
        Page<KatilimSaglananProgramBilgisi> page = katilimSaglananProgramBilgisiService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/katilim-saglanan-program-bilgisis");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /katilim-saglanan-program-bilgisis/:id : get the "id" katilimSaglananProgramBilgisi.
     *
     * @param id the id of the katilimSaglananProgramBilgisi to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the katilimSaglananProgramBilgisi, or with status 404 (Not Found)
     */
    @GetMapping("/katilim-saglanan-program-bilgisis/{id}")
    @Timed
    public ResponseEntity<KatilimSaglananProgramBilgisi> getKatilimSaglananProgramBilgisi(@PathVariable Long id) {
        log.debug("REST request to get KatilimSaglananProgramBilgisi : {}", id);
        Optional<KatilimSaglananProgramBilgisi> katilimSaglananProgramBilgisi = katilimSaglananProgramBilgisiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(katilimSaglananProgramBilgisi);
    }

    /**
     * DELETE  /katilim-saglanan-program-bilgisis/:id : delete the "id" katilimSaglananProgramBilgisi.
     *
     * @param id the id of the katilimSaglananProgramBilgisi to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/katilim-saglanan-program-bilgisis/{id}")
    @Timed
    public ResponseEntity<Void> deleteKatilimSaglananProgramBilgisi(@PathVariable Long id) {
        log.debug("REST request to delete KatilimSaglananProgramBilgisi : {}", id);
        katilimSaglananProgramBilgisiService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
