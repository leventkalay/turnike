package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.Yayinlar;
import projectimage.service.YayinlarService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Yayinlar.
 */
@RestController
@RequestMapping("/api")
public class YayinlarResource {

    private final Logger log = LoggerFactory.getLogger(YayinlarResource.class);

    private static final String ENTITY_NAME = "yayinlar";

    private final YayinlarService yayinlarService;

    public YayinlarResource(YayinlarService yayinlarService) {
        this.yayinlarService = yayinlarService;
    }

    /**
     * POST  /yayinlars : Create a new yayinlar.
     *
     * @param yayinlar the yayinlar to create
     * @return the ResponseEntity with status 201 (Created) and with body the new yayinlar, or with status 400 (Bad Request) if the yayinlar has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/yayinlars")
    @Timed
    public ResponseEntity<Yayinlar> createYayinlar(@Valid @RequestBody Yayinlar yayinlar) throws URISyntaxException {
        log.debug("REST request to save Yayinlar : {}", yayinlar);
        if (yayinlar.getId() != null) {
            throw new BadRequestAlertException("A new yayinlar cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Yayinlar result = yayinlarService.save(yayinlar);
        return ResponseEntity.created(new URI("/api/yayinlars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /yayinlars : Updates an existing yayinlar.
     *
     * @param yayinlar the yayinlar to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated yayinlar,
     * or with status 400 (Bad Request) if the yayinlar is not valid,
     * or with status 500 (Internal Server Error) if the yayinlar couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/yayinlars")
    @Timed
    public ResponseEntity<Yayinlar> updateYayinlar(@Valid @RequestBody Yayinlar yayinlar) throws URISyntaxException {
        log.debug("REST request to update Yayinlar : {}", yayinlar);
        if (yayinlar.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Yayinlar result = yayinlarService.save(yayinlar);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, yayinlar.getId().toString()))
            .body(result);
    }

    /**
     * GET  /yayinlars : get all the yayinlars.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of yayinlars in body
     */
    @GetMapping("/yayinlars")
    @Timed
    public ResponseEntity<List<Yayinlar>> getAllYayinlars(Pageable pageable) {
        log.debug("REST request to get a page of Yayinlars");
        Page<Yayinlar> page = yayinlarService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/yayinlars");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /yayinlars/:id : get the "id" yayinlar.
     *
     * @param id the id of the yayinlar to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the yayinlar, or with status 404 (Not Found)
     */
    @GetMapping("/yayinlars/{id}")
    @Timed
    public ResponseEntity<Yayinlar> getYayinlar(@PathVariable Long id) {
        log.debug("REST request to get Yayinlar : {}", id);
        Optional<Yayinlar> yayinlar = yayinlarService.findOne(id);
        return ResponseUtil.wrapOrNotFound(yayinlar);
    }

    /**
     * DELETE  /yayinlars/:id : delete the "id" yayinlar.
     *
     * @param id the id of the yayinlar to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/yayinlars/{id}")
    @Timed
    public ResponseEntity<Void> deleteYayinlar(@PathVariable Long id) {
        log.debug("REST request to delete Yayinlar : {}", id);
        yayinlarService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
