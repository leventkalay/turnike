package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.OzelIlgiAlanlari;
import projectimage.service.OzelIlgiAlanlariService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OzelIlgiAlanlari.
 */
@RestController
@RequestMapping("/api")
public class OzelIlgiAlanlariResource {

    private final Logger log = LoggerFactory.getLogger(OzelIlgiAlanlariResource.class);

    private static final String ENTITY_NAME = "ozelIlgiAlanlari";

    private final OzelIlgiAlanlariService ozelIlgiAlanlariService;

    public OzelIlgiAlanlariResource(OzelIlgiAlanlariService ozelIlgiAlanlariService) {
        this.ozelIlgiAlanlariService = ozelIlgiAlanlariService;
    }

    /**
     * POST  /ozel-ilgi-alanlaris : Create a new ozelIlgiAlanlari.
     *
     * @param ozelIlgiAlanlari the ozelIlgiAlanlari to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ozelIlgiAlanlari, or with status 400 (Bad Request) if the ozelIlgiAlanlari has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ozel-ilgi-alanlaris")
    @Timed
    public ResponseEntity<OzelIlgiAlanlari> createOzelIlgiAlanlari(@RequestBody OzelIlgiAlanlari ozelIlgiAlanlari) throws URISyntaxException {
        log.debug("REST request to save OzelIlgiAlanlari : {}", ozelIlgiAlanlari);
        if (ozelIlgiAlanlari.getId() != null) {
            throw new BadRequestAlertException("A new ozelIlgiAlanlari cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OzelIlgiAlanlari result = ozelIlgiAlanlariService.save(ozelIlgiAlanlari);
        return ResponseEntity.created(new URI("/api/ozel-ilgi-alanlaris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ozel-ilgi-alanlaris : Updates an existing ozelIlgiAlanlari.
     *
     * @param ozelIlgiAlanlari the ozelIlgiAlanlari to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ozelIlgiAlanlari,
     * or with status 400 (Bad Request) if the ozelIlgiAlanlari is not valid,
     * or with status 500 (Internal Server Error) if the ozelIlgiAlanlari couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ozel-ilgi-alanlaris")
    @Timed
    public ResponseEntity<OzelIlgiAlanlari> updateOzelIlgiAlanlari(@RequestBody OzelIlgiAlanlari ozelIlgiAlanlari) throws URISyntaxException {
        log.debug("REST request to update OzelIlgiAlanlari : {}", ozelIlgiAlanlari);
        if (ozelIlgiAlanlari.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OzelIlgiAlanlari result = ozelIlgiAlanlariService.save(ozelIlgiAlanlari);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ozelIlgiAlanlari.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ozel-ilgi-alanlaris : get all the ozelIlgiAlanlaris.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ozelIlgiAlanlaris in body
     */
    @GetMapping("/ozel-ilgi-alanlaris")
    @Timed
    public ResponseEntity<List<OzelIlgiAlanlari>> getAllOzelIlgiAlanlaris(Pageable pageable) {
        log.debug("REST request to get a page of OzelIlgiAlanlaris");
        Page<OzelIlgiAlanlari> page = ozelIlgiAlanlariService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ozel-ilgi-alanlaris");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /ozel-ilgi-alanlaris/:id : get the "id" ozelIlgiAlanlari.
     *
     * @param id the id of the ozelIlgiAlanlari to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ozelIlgiAlanlari, or with status 404 (Not Found)
     */
    @GetMapping("/ozel-ilgi-alanlaris/{id}")
    @Timed
    public ResponseEntity<OzelIlgiAlanlari> getOzelIlgiAlanlari(@PathVariable Long id) {
        log.debug("REST request to get OzelIlgiAlanlari : {}", id);
        Optional<OzelIlgiAlanlari> ozelIlgiAlanlari = ozelIlgiAlanlariService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ozelIlgiAlanlari);
    }

    /**
     * DELETE  /ozel-ilgi-alanlaris/:id : delete the "id" ozelIlgiAlanlari.
     *
     * @param id the id of the ozelIlgiAlanlari to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ozel-ilgi-alanlaris/{id}")
    @Timed
    public ResponseEntity<Void> deleteOzelIlgiAlanlari(@PathVariable Long id) {
        log.debug("REST request to delete OzelIlgiAlanlari : {}", id);
        ozelIlgiAlanlariService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
