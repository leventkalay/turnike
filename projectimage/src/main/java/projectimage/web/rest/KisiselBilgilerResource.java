package projectimage.web.rest;

import com.codahale.metrics.annotation.Timed;
import projectimage.domain.KisiselBilgiler;
import projectimage.service.KisiselBilgilerService;
import projectimage.web.rest.errors.BadRequestAlertException;
import projectimage.web.rest.util.HeaderUtil;
import projectimage.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing KisiselBilgiler.
 */
@RestController
@RequestMapping("/api")
public class KisiselBilgilerResource {

    private final Logger log = LoggerFactory.getLogger(KisiselBilgilerResource.class);

    private static final String ENTITY_NAME = "kisiselBilgiler";

    private final KisiselBilgilerService kisiselBilgilerService;

    public KisiselBilgilerResource(KisiselBilgilerService kisiselBilgilerService) {
        this.kisiselBilgilerService = kisiselBilgilerService;
    }

    /**
     * POST  /kisisel-bilgilers : Create a new kisiselBilgiler.
     *
     * @param kisiselBilgiler the kisiselBilgiler to create
     * @return the ResponseEntity with status 201 (Created) and with body the new kisiselBilgiler, or with status 400 (Bad Request) if the kisiselBilgiler has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/kisisel-bilgilers")
    @Timed
    public ResponseEntity<KisiselBilgiler> createKisiselBilgiler(@Valid @RequestBody KisiselBilgiler kisiselBilgiler) throws URISyntaxException {
        log.debug("REST request to save KisiselBilgiler : {}", kisiselBilgiler);
        if (kisiselBilgiler.getId() != null) {
            throw new BadRequestAlertException("A new kisiselBilgiler cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KisiselBilgiler result = kisiselBilgilerService.save(kisiselBilgiler);
        return ResponseEntity.created(new URI("/api/kisisel-bilgilers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /kisisel-bilgilers : Updates an existing kisiselBilgiler.
     *
     * @param kisiselBilgiler the kisiselBilgiler to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated kisiselBilgiler,
     * or with status 400 (Bad Request) if the kisiselBilgiler is not valid,
     * or with status 500 (Internal Server Error) if the kisiselBilgiler couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/kisisel-bilgilers")
    @Timed
    public ResponseEntity<KisiselBilgiler> updateKisiselBilgiler(@Valid @RequestBody KisiselBilgiler kisiselBilgiler) throws URISyntaxException {
        log.debug("REST request to update KisiselBilgiler : {}", kisiselBilgiler);
        if (kisiselBilgiler.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KisiselBilgiler result = kisiselBilgilerService.save(kisiselBilgiler);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, kisiselBilgiler.getId().toString()))
            .body(result);
    }

    /**
     * GET  /kisisel-bilgilers : get all the kisiselBilgilers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of kisiselBilgilers in body
     */
    @GetMapping("/kisisel-bilgilers")
    @Timed
    public ResponseEntity<List<KisiselBilgiler>> getAllKisiselBilgilers(Pageable pageable) {
        log.debug("REST request to get a page of KisiselBilgilers");
        Page<KisiselBilgiler> page = kisiselBilgilerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/kisisel-bilgilers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /kisisel-bilgilers/:id : get the "id" kisiselBilgiler.
     *
     * @param id the id of the kisiselBilgiler to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the kisiselBilgiler, or with status 404 (Not Found)
     */
    @GetMapping("/kisisel-bilgilers/{id}")
    @Timed
    public ResponseEntity<KisiselBilgiler> getKisiselBilgiler(@PathVariable Long id) {
        log.debug("REST request to get KisiselBilgiler : {}", id);
        Optional<KisiselBilgiler> kisiselBilgiler = kisiselBilgilerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kisiselBilgiler);
    }

    /**
     * DELETE  /kisisel-bilgilers/:id : delete the "id" kisiselBilgiler.
     *
     * @param id the id of the kisiselBilgiler to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/kisisel-bilgilers/{id}")
    @Timed
    public ResponseEntity<Void> deleteKisiselBilgiler(@PathVariable Long id) {
        log.debug("REST request to delete KisiselBilgiler : {}", id);
        kisiselBilgilerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
