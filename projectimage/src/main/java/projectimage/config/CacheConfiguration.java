package projectimage.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(projectimage.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(projectimage.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(projectimage.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(projectimage.domain.PersistentToken.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.User.class.getName() + ".persistentTokens", jcacheConfiguration);
            cm.createCache(projectimage.domain.KisiselBilgiler.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.IletisimBilgileri.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.OgrenimBilgileri.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.YabanciDilBilgileri.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.Istectrubeleri.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.UzmanlikAlanlari.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.EgiticiYetkinlikleri.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.Yayinlar.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.Proje.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.KatilimSaglananProgramBilgisi.class.getName(), jcacheConfiguration);
            cm.createCache(projectimage.domain.OzelIlgiAlanlari.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
