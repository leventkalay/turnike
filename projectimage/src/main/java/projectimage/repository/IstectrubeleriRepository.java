package projectimage.repository;

import projectimage.domain.Istectrubeleri;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Istectrubeleri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IstectrubeleriRepository extends JpaRepository<Istectrubeleri, Long> {

}
