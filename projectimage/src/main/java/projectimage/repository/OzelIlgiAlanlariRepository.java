package projectimage.repository;

import projectimage.domain.OzelIlgiAlanlari;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OzelIlgiAlanlari entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OzelIlgiAlanlariRepository extends JpaRepository<OzelIlgiAlanlari, Long> {

}
