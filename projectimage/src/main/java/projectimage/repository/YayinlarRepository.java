package projectimage.repository;

import projectimage.domain.Yayinlar;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Yayinlar entity.
 */
@SuppressWarnings("unused")
@Repository
public interface YayinlarRepository extends JpaRepository<Yayinlar, Long> {

}
