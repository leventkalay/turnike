package projectimage.repository;

import projectimage.domain.KisiselBilgiler;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the KisiselBilgiler entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KisiselBilgilerRepository extends JpaRepository<KisiselBilgiler, Long> {

    @Query("select kisisel_bilgiler from KisiselBilgiler kisisel_bilgiler where kisisel_bilgiler.user.login = ?#{principal.username}")
    List<KisiselBilgiler> findByUserIsCurrentUser();

}
