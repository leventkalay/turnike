package projectimage.repository;

import projectimage.domain.YabanciDilBilgileri;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the YabanciDilBilgileri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface YabanciDilBilgileriRepository extends JpaRepository<YabanciDilBilgileri, Long> {

}
