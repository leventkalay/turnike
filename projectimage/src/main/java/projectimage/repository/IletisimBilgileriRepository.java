package projectimage.repository;

import projectimage.domain.IletisimBilgileri;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IletisimBilgileri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IletisimBilgileriRepository extends JpaRepository<IletisimBilgileri, Long> {

}
