package projectimage.repository;

import projectimage.domain.EgiticiYetkinlikleri;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EgiticiYetkinlikleri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EgiticiYetkinlikleriRepository extends JpaRepository<EgiticiYetkinlikleri, Long> {

}
