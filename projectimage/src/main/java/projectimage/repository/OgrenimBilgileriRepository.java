package projectimage.repository;

import projectimage.domain.OgrenimBilgileri;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OgrenimBilgileri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OgrenimBilgileriRepository extends JpaRepository<OgrenimBilgileri, Long> {

}
