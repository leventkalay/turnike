package projectimage.repository;

import projectimage.domain.KatilimSaglananProgramBilgisi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the KatilimSaglananProgramBilgisi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KatilimSaglananProgramBilgisiRepository extends JpaRepository<KatilimSaglananProgramBilgisi, Long> {

}
