package projectimage.repository;

import projectimage.domain.UzmanlikAlanlari;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UzmanlikAlanlari entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UzmanlikAlanlariRepository extends JpaRepository<UzmanlikAlanlari, Long> {

}
